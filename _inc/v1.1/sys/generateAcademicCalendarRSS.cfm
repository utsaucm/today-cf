<!---
	*	academicEvents.cfm
	*	A simple coldfusion app that utilizes the coldfusion webservices and xml tags
	*	to pull events from the R25 Webservices component.
	*
	* @author john.garza@utsa.edu
	* @see also:  R25 Searches and Authentication
	*
--->
<cftry>
<cfscript>
//following should probably belond in Application.cfm
rssroot = "http://www.utsa.edu/calendar/rss.cfm?rssShow=academic";
</cfscript>
<cfhttp url = "#rssroot#" port="80" method="get"></cfhttp>
<cfset sXMLFileName = "acadCal" />

<cfset relativeFileName = "/today/rss/#sXMLFileName#.xml" />
<cfset fullPathName = ExpandPath("#relativeFileName#") />
<cffile action="write" file="#fullPathName#" output="#CFHTTP.FileContent#" />
<!--- Name the File --->
<cfoutput>
	<div>
    Saved Academic Calendar to file: <a href="#relativeFileName#">#fullPathName#</a>
  </div>
</cfoutput>
<!--- Clear out the Array prevents rewriting of the data in other RSS files --->
<cfcatch>
  <!--- surrounded everything in a cfcatch incase webservices is unreachable. In this case, return nothing --->
  <cfoutput>
    <h1>CFCATCH CAUGHT</h1>
    #cfcatch.type#  <br/>
    #cfcatch.message#  <br/>
    #cfcatch.detail#  <br/>
    <cfdump var="#cfcatch.tagcontext#" />
  </cfoutput>
</cfcatch>
</cftry>
<!---<cfdump var="#resultStruct#"/>--->
