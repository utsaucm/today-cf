<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
	<cfquery name="SelectLeadPhotos" datasource="#Application.PrimaryDSN#" timeout="500">
		SELECT * FROM Articles WHERE
		leadphoto = 't'
		AND dateStart < now()
		ORDER BY dateStart DESC LIMIT 5
	</cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
	<cfquery name="SelectLeadPhotos" datasource="#Application.PrimaryDSN#" timeout="500">
		<!---
			This SELECT query was developed by Ruben Ortiz on 6-24-2008 to fix an anomaly:
			inconsistent results between the today rss feed and utsa home page top stories list.
			The WHERE clause prevents future stores from being pushed to rss lead Photos feed.
			the DateDiff function returns a negative value if the article startdate is greater than the current date.
		--->

		SELECT TOP 5 Articles.*
		FROM Articles
		WHERE leadphoto = 1 AND DateDiff(d, Articles.DateStart, getdate()) >= 0
		Order by datestart desc

		<!--- original query developed by Joe McBride
		SELECT TOP 5 Articles.*
		FROM Articles
		Where leadphoto=1
		Order by datestart desc
		--->
	</cfquery>
</cfif>
