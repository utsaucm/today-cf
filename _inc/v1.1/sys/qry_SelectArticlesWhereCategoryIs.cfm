<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
	<cfquery name="SelectArticlesWhereCategoryIs" datasource="#Application.PrimaryDSN#" timeout="500">
		SELECT a.*, r.categoryid
    FROM articles a, rsscategoryarticlerelations r WHERE
     a.articleid = r.articleid and r.categoryid = #categoryID#
     AND a.dateStart < now()
     ORDER BY a.articleid DESC LIMIT 20;
	</cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
	<cfquery name="SelectArticlesWhereCategoryIs" datasource="#Application.PrimaryDSN#" timeout="500">
    <!---
    	This SELECT query was developed by Ruben Ortiz on 6-25-2008 to fix an anomaly - prevent future (unpublished stories)
    	from appearing in the rss feeds along the current stories.
    	The WHERE clause prevents future stores from being pushed to all rss feeds.
    	the DateDiff function returns a negative value if the article startdate is greater than the current date.
    --->
    SELECT     TOP 10 dbo.Articles.*
    FROM         dbo.Articles INNER JOIN
                          dbo.RSSCategoryArticleRelations ON dbo.Articles.ArticleID = dbo.RSSCategoryArticleRelations.ArticleID
    WHERE     (dbo.RSSCategoryArticleRelations.CategoryID = #categoryID# AND DateDiff(d, dbo.Articles.DateStart, getdate()) >= 0)
    ORDER BY datestart desc

    <!--- Joe McBride's code:
    SELECT     TOP 5 dbo.Articles.*
    FROM         dbo.Articles INNER JOIN
                          dbo.RSSCategoryArticleRelations ON dbo.Articles.ArticleID = dbo.RSSCategoryArticleRelations.ArticleID
    WHERE     (dbo.RSSCategoryArticleRelations.CategoryID = #categoryID# AND DateDiff(d, dbo.Articles.DateStart, getdate()) >= 0)
    Order by datestart desc
    --->
	</cfquery>
</cfif>
