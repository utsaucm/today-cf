<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
	<cfquery name="SelectTopArticles" datasource="#Application.PrimaryDSN#" timeout="500">
		SELECT * FROM Articles WHERE
		dateExpire IS NULL
		AND dateStart < now()
		ORDER BY dateStart DESC LIMIT 10;
	</cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
	<cfquery name="SelectTopArticles" datasource="#Application.PrimaryDSN#" timeout="500">
		<!---
			This SELECT query was revised by Ruben Ortiz on 03-27-2009 to filter out expired stores from being listed on RSS feeds and today stories.
		--->
		SELECT TOP 10 Articles.*
		FROM Articles
		WHERE DateDiff(d, Articles.DateStart, getdate()) >= 0 AND DateExpire IS NULL
		ORDER BY datestart DESC;


		<!---
			This SELECT query was developed by Ruben Ortiz on 6-24-2008 to fix an anomaly:
			inconsistent results between the today rss feed and utsa home page top stories list.
			The WHERE clause prevents future stores from being pushed to rss top stories feed.
			the DateDiff function returns a negative value if the article startdate is greater than the current date.
		--->

		<!---
		SELECT TOP 5 Articles.*
		FROM Articles
		WHERE DateDiff(d, Articles.DateStart, getdate()) >= 0
		ORDER BY datestart DESC;
		--->


		<!--- original query developed by Joe McBride
		SELECT TOP 10 Articles.*
		FROM Articles
		Where leadPhoto = 1
		Order by datestart desc;
		--->
	</cfquery>
</cfif>
