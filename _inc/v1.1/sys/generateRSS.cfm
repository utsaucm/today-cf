<cfset aStories=ArrayNew(2) />
<cfset iLoop = 1 />
<cfset categoryname="Top Stories" />

<cfinclude template="qry_SelectTopArticles.cfm"/>
<cfif iLoop gt 1>
	<cfset iLoop = 1/>
</cfif>

<!--- Loop through individual articles from query of articles in one category --->
<cfloop query="SelectTopArticles">
	<cfset aStories[iLoop][1] = SelectTopArticles.URL[SelectTopArticles.currentRow]> <!--- Link --->
	<cfset aStories[iLoop][2] = SelectTopArticles.title> <!--- Title --->
	<cfset aStories[iLoop][3] = SelectTopArticles.teaser> <!--- Teaser --->
	<cfset aStories[iLoop][4] = SelectTopArticles.content> <!--- Description --->
 	<cfset aStories[iLoop][5] = SelectTopArticles.leadphotoaddress> <!--- Photo --->
	<cfset aStories[iLoop][6] = SelectTopArticles.author>    <!--- Author --->
  <cfset aStories[iLoop][7] = SelectTopArticles.datestart> <!--- DateStart --->
	<cfset iLoop = iLoop + 1>
</cfloop>

<!--- Write individual XML file --->

<!--- RSS filename --->
<cfset sXMLFileName = "top" />
<cfinclude template="act_WriteXMLFile.cfm">
<cfoutput><div><a href="/today/rss/top.xml">top.xml</a> - Top Stories</div></cfoutput>

<cfflush/>
<!--- Clear out the Array prevents rewriting of the data in other RSS files --->
<cfset Temp = ArrayClear(aStories) />

<cfset aStories=ArrayNew(2) />
<cfset iLoop = 1 />
<cfset categoryname="Lead Photos" />

<cfinclude template="qry_SelectLeadPhotos.cfm" />
<cfif iLoop gt 1><cfset iLoop = 1 /></cfif>
<!--- Loop through individual articles from query of articles in one category --->
<cfloop query="SelectLeadPhotos">
	<cfset aStories[iLoop][1] = SelectLeadPhotos.URL[SelectLeadPhotos.currentRow] /> <!--- Link --->
	<cfset aStories[iLoop][2] = SelectLeadPhotos.title /> <!--- Title --->
	<cfset aStories[iLoop][3] = SelectLeadPhotos.teaser /> <!--- Teaser --->
	<cfset aStories[iLoop][4] = SelectLeadPhotos.content /> <!--- Description --->
 	<cfset aStories[iLoop][5] = SelectLeadPhotos.leadphotoaddress /> <!--- Photo --->
	<cfset aStories[iLoop][6] = SelectLeadPhotos.author />    <!--- Author --->
	<cfset aStories[iLoop][7] = SelectLeadPhotos.datestart /> <!--- DateStart --->
  <cfset iLoop = iLoop + 1 />
</cfloop>

<!--- Write individual XML file --->
<cfset sXMLFileName = "photos" /> <!--- Name the File --->
<cfinclude template="act_WriteXMLFile.cfm" />
<cfoutput><div><a href="/today/rss/photos.xml">photos.xml</a> - Lead Photos</div></cfoutput>

<!--- Clear out the Array prevents rewriting of the data in other RSS files --->
<cfset Temp = ArrayClear(aStories) />


<cfinclude template="qry_SelectAllCategories.cfm" />

<cfset aStories=ArrayNew(2) />
<cfset iLoop = 1 />

<!--- Loop through categories from DB --->
<cfoutput query="SelectAllCategories">
	<cfflush/>
	<cfset CategoryID=SelectAllCategories.CategoryID />
	<cfinclude template="qry_SelectArticlesWhereCategoryIs.cfm" />
	<cfif iLoop gt 1><cfset iLoop = 1 /></cfif>

	<!--- Loop through individual articles from query of articles in one category --->
	<cfloop query="SelectArticlesWhereCategoryIs">
		<cfset aStories[iLoop][1] = SelectArticlesWhereCategoryIs.URL[SelectArticlesWhereCategoryIs.currentRow] /> <!--- Link --->
		<cfset aStories[iLoop][2] = SelectArticlesWhereCategoryIs.title /> <!--- Title --->
		<cfset aStories[iLoop][3] = SelectArticlesWhereCategoryIs.teaser /> <!--- Teaser --->
		<cfset aStories[iLoop][4] = SelectArticlesWhereCategoryIs.content /> <!--- Description --->
 		<cfset aStories[iLoop][5] = SelectArticlesWhereCategoryIs.leadphotoaddress /> <!--- Photo --->
		<cfset aStories[iLoop][6] = SelectArticlesWhereCategoryIs.author />    <!--- Author --->
  	<cfset aStories[iLoop][7] = SelectArticlesWhereCategoryIs.datestart /> <!--- DateStart --->
		<cfset iLoop = iLoop + 1 />
	</cfloop>

	<!--- Write individual XML file --->
	<cfset sXMLFileName = SelectAllCategories.CategoryID /> <!--- Name the file --->
	<cfinclude template="act_WriteXMLFile.cfm" />
	<div><a href="/today/rss/#categoryid#.xml">#categoryid#.xml</a> - #categoryName#</div>

	<!--- Clear out the Array prevents rewriting of the data in other RSS files --->
	<cfset Temp = ArrayClear(aStories) />
</cfoutput>
