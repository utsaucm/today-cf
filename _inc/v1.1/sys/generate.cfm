<h1>UTSA Today v1.1 Includes Generated</h1>
	<cf_UTSASiteCache
		url="/today/_inc/v1.1/cfm/today-cta-01.cfm"
		path="/today/_inc/v1.1/generated/today-cta-01.html" />
	<cf_UTSASiteCache
		url="/today/_inc/v1.1/cfm/today-cta-02.cfm"
		path="/today/_inc/v1.1/generated/today-cta-02.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-announcements.cfm"
		path="/today/_inc/v1.1/generated/today-announcements.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-header-after-title.cfm"
		path="/today/_inc/v1.1/generated/today-header-after-title.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-header.cfm"
		path="/today/_inc/v1.1/generated/today-header.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-footer.cfm"
		path="/today/_inc/v1.1/generated/today-footer.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-main-stories.cfm"
		path="/today/_inc/v1.1/generated/today-main-stories.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-marr.cfm"
		path="/today/_inc/v1.1/generated/today-marr.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-more-news.cfm"
		path="/today/_inc/v1.1/generated/today-more-news.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-nav.cfm"
		path="/today/_inc/v1.1/generated/today-nav.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-newswire.cfm"
		path="/today/_inc/v1.1/generated/today-newswire.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-itn.cfm"
		path="/today/_inc/v1.1/generated/today-itn.html" />
		<!---
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-pod.cfm"
		path="/today/_inc/v1.1/generated/today-pod.html" />
		--->
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-spotlights.cfm"
		path="/today/_inc/v1.1/generated/today-spotlights.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/todays-date.cfm"
		path="/today/_inc/v1.1/generated/todays-date.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/today-more-news-page.cfm"
		path="/today/_inc/v1.1/generated/today-more-news-page.html" />

		<!--- new additions as of v2.0, will need to modularize these a bit more
		 - as we move modules over to cascade, this will get more granular and
		 - be migrated away from coldfusion --->
	 <!---
	 	- can't do this yet as all the generated links have a GET action on index.cfm for archive content
	<cf_UTSASiteCache url="/today/_inc/v2.0/cfm/content-archives-LCol.cfm"
		path="/today/_inc/v2.0/generated/content-archives-LCol.html" />
	<cf_UTSASiteCache url="/today/_inc/v2.0/cfm/content-archives-RCol.cfm"
		path="/today/_inc/v2.0/generated/content-archives-RCol.html" />
		--->

	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/news-include.cfm"
		path="/today/_inc/v1.1/generated/news-include.html" />
	<cf_UTSASiteCache url="/today/_inc/v1.1/cfm/marr-include.cfm"
		path="/today/_inc/v1.1/generated/marr-include.html" />

	<cfflush/>

	<cfinclude template="/today/_inc/v1.1/sys/generateRSS.cfm" />
	<cfinclude template="/today/_inc/v1.1/sys/generateAcademicCalendarRSS.cfm" />
	<cf_UTSASiteCache url="/today/rss/index.cfm"
		path="/today/rss/index.html" />
