<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
	<cfquery name="SelectAllCategories" datasource="#Application.PrimaryDSN#" timeout="500">
		SELECT * FROM RSSCategories
		WHERE CategoryStatus = 't'
		ORDER BY RSSCategories.CategoryName;
	</cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
	<cfquery name="SelectAllCategories" datasource="#Application.PrimaryDSN#" timeout="500">
		SELECT * FROM RSSCategories
		WHERE CategoryStatus = 1
		ORDER BY RSSCategories.CategoryName;
	</cfquery>
</cfif>
