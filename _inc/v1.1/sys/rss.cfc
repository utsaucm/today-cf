<cfcomponent>
	<cffunction name="RenderEnclosure" access="public" output="true" returntype="string">
		<cfargument name="imageURI" required="YES" />
		<cfscript>
		/* make sure URL is absolute */
		imageURL = "";
		imagePath = "";
		imageSize = 0;
		mimeType = "image/jpeg";

		if (len(imageURI) gt 0) {
			if ( (find("http://",imageURI) lt 1) AND (find("https://", imageURI) lt 1) ) {
				imageURL = "http://www.utsa.edu" & imageURI;
			} else {
				imageURL = imageURI;
			}
		}
		/* need local path for this image */
		imagePath = REReplaceNoCase(imageURI, "http://www.utsa.edu/", "/", "ONE");
		imagePath = REReplaceNoCase(imagePath, "https://www.utsa.edu/", "/", "ONE");
		imagePath = REReplaceNoCase(imagePath, "http://utsa.edu/", "/", "ONE");
		imagePath = REReplaceNoCase(imagePath, "https://utsa.edu/", "/", "ONE");
		localPath = ExpandPath(imagePath);
		</cfscript>

		<cfif (Len(imagePath) gt 0)>
			<cfif (FileExists(localPath))>
				<cfset mimeType = fileGetMimeType(localPath, FALSE) />
  			<cfset imgFileInfo = GetFileInfo(localPath) />
				<cfset imageSize = imgFileInfo.Size />
			<cfelse>
				<cfset imageURL = "http://www.utsa.edu/today/images/monument340.jpg" />
				<cfset localPath = ExpandPath("/today/images/monument340.jpg") />
				<cfset mimeType = "image/jpeg" />
				<cfset imgFileInfo = GetFileInfo(localPath) />
				<cfset imageSize = imgFileInfo.Size />
			</cfif>
			<cfoutput>
				<enclosure url="#imageURL#" type="#mimeType#" length="#imageSize#"/>
			</cfoutput>
		</cfif>
	</cffunction>

	<cffunction name="RenderRSSItem" access="public" output="true" returntype="string">
		<cfargument name="item" required="YES" />
		<cfscript>
		itemURI = item[1];

		if (len(itemURI) gt 0) {
			if ( (find("http://",itemURI) lt 1) AND (find("https://", itemURI) lt 1) ) {
			itemURI = "http://www.utsa.edu" & itemURI;
			}
		} else {
			itemURI = "http://www.utsa.edu";
		}
		/* clean up the extraneous chars */
		encodedTitle = item[2];
		encodedTitle = XMLFormat(encodedTitle);
		encodedTitle = URL.DeMoronize(encodedTitle);
		encodedTitle = CleanHighASCII(encodedTitle);
		/* clean up the extraneous chars */
		encodedTeaser = item[3];
		encodedTeaser = XMLFormat(encodedTeaser);
		encodedTeaser = DeMoronize(encodedTeaser);
		encodedTeaser = CleanHighASCII(encodedTeaser);
		/* clean up the extraneous chars */
		photoURI = "";
		leadPhotoAddress = item[5];
		if (len(leadPhotoAddress) eq 0) {
			leadPhotoAddress = "/today/images/monument340.jpg";
		}
		
		if (len(leadPhotoAddress) gt 0) {
			if ( (find("http://",leadPhotoAddress) lt 1) AND (find("https://", leadPhotoAddress) lt 1) ) {
			leadPhotoAddress = "http://www.utsa.edu" & leadPhotoAddress;
			}
		}
		if (Len(leadPhotoAddress) gt 0) {
			photoURI = '<img src="#leadPhotoAddress#" alt="#encodedTitle#" />';
		}
		encodedTeaser = item[3];
		encodedTeaser = XMLFormat(encodedTeaser);
		encodedTeaser = DeMoronize(encodedTeaser);
		encodedTeaser = CleanHighASCII(encodedTeaser);
		if (Len(encodedTeaser) eq 0) {
			encodedTeaser = encodedTitle;
		}
		/* store Articles "Content" field text data into a var for further processing */
		pubDate = item[7];
		pubDate = DateAdd( "s", GETTimeZoneInfo().UTCTotalOffset, pubDate );
		pubDate = "#dateformat(pubDate, "ddd, dd mmm yyyy")# #timeformat(pubDate, "HH:mm:ss")# GMT";
		</cfscript>
<cfoutput>
	<item>
		<title>#encodedTitle#</title>
		<link>#itemURI#</link>
		<description>
			<![CDATA[
			#photoURI#
			#encodedTeaser#
			]]>
		</description>
		#RenderEnclosure(leadPhotoAddress)#
		<pubDate>#pubDate#</pubDate>
	</item>
</cfoutput>
	</cffunction>
</cfcomponent>
