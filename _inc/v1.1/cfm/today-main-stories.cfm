
<cfinclude template="/today/_inc/admin/articleManager/act_PrepHomePageArray.cfm" />
<div class="row">
  <cfloop index="iLooper" from="1" to="1" step="1">
    <div class="col-sm-12">
      <div class="card">
      <cfoutput>
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-main-stories-01" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#xmlFormat(aStories[iLooper][5])#">
          <img width="100%" class="card-img-top hidden-sm-down" src="#XMLFORMAT(aStories[1][11])#" alt="#CleanHighAscii(aStories[iLooper][2])#"/>
          <img width="100%" class="card-img-top hidden-md-up" src="#XMLFORMAT(aStories[1][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#"/>
          <div class="card-block mainheadlineblue">
            #CleanHighAscii(aStories[iLooper][2])#
          </div>
        </a>
      </cfoutput>
      </div>
    </div>
  </cfloop>
</div>

<div class="row">
    <div class="col-sm-6">
    <cfloop index="iLooper" from="2" to="2" step="1">
      <div class="card">
        <cfoutput>
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-main-stories-02" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#XMLFORMAT(aStories[iLooper][5])#">
          <img width="100%" class="card-img-top" src="#XMLFORMAT(aStories[2][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#"/>
          <div class="card-block secondaryheadlineblue"><span class="card-title">
            #CleanHighAscii(aStories[iLooper][2])#
          </span></div>
        </a>
        </cfoutput>
      </div>
    </cfloop>
    </div>

    <div class="col-sm-6">
    <cfloop index="iLooper" from="3" to="3" step="1">
      <div class="card">
        <cfoutput>
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-main-stories-03" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#XMLFORMAT(aStories[iLooper][5])#">
          <img width="100%" class="card-img-top" src="#XMLFORMAT(aStories[3][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#" />
          <div class="card-block secondaryheadlineblue"><span class="card-title">
            #CleanHighAscii(aStories[iLooper][2])#
          </span></div>
        </a>
        </cfoutput>
      </div>
    </cfloop>
    </div>
</div>
<hr>
<!-- end main stories -->
