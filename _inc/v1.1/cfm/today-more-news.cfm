<!---
Today - More News
today-more-news.cfm
===

Responsible for outputting the more news items in the main index today page sidebar

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Requires admin/articleManager/act_PrepHomePageArray.cfm to be cfincluded before
this file is included, @see /today/index.cfm

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
--->
<cfinclude template="/today/_inc/admin/articleManager/act_PrepHomePageArray.cfm" />

<!-- begin more news -->
  <div class="row">
    <div class="col-sm-12">
      <span class="utsaToday-SectionTitle">News</span>
      <a class="os" href="/today/rss/"><i class="fa fa-rss-square" aria-hidden="true"></i></a>
    </div>
  </div>

<cfloop index="iLooper" from="#moreNewsStart#" to="#moreNews_i - 1#" step="1">
  <cfset currentTitle = "#aStories[iLooper][2]#" />
  <cfset currentTitle = REReplaceNoCase(currentTitle, "Meet a Roadrunner: ", "") />

  <cfif (currentTitle GT 0) >
    <cfoutput>
      <div class="row">
        <div class="col-sm-12">
          <h3 class="rightnew">
            <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-more-news" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#XmlFormat(aStories[iLooper][5])#" data-id="#aStories[iLooper][1]#">
              #CleanHighAscii(currentTitle)#</a>
          </h3>
        </div>
      </div>
      <hr>
    </cfoutput>
  </cfif>
</cfloop>
<cfif moreNewsStart EQ moreNews_i >
    <p>There are no More News stories at this time.</p>
<cfelse>
  <div class="row">
    <div class="col-sm-12">
      <p class="morenews">
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-more-news" ga-event-action="/today/morenews.html" href="/today/morenews.html">
          <i class="fa fa-angle-double-right" aria-hidden="true"></i> More news
        </a>
      </p>
    </div>
  </div>
</cfif>
<!-- end more news -->
