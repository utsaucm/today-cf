<cfset todaysDate = now() />
<cfset dateFormatString = "dddd, mmmm dd, yyyy" />
<section id="utsatoday-top" style="margin-top: 2em;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12" style="padding-bottom: .5rem;">
        <div class="col-md-7 utsaToday-Logo">
          <div class="row">
            <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-logo" ga-event-action="/today" href="/today">
              <img src="/today/img/utsaTodayLogoBlue.png" alt="UTSA Today Home" />
            </a>
          </div>
        </div>
        <div class="col-md-5 col-sm-12">
          <div class="row">
            <form class="form-inline" id="cse-search-box" action="/today/searchresults.html">
              <input name="cx" type="hidden" value="000238266656426684962:z1bmoync9ei" />
              <input id="q" name="q"  class="col-sm-11" class="form-control" type="text" placeholder="Search" />
              <button class="col-sm-1 btn" type="submit" style="padding:0 5px 4px 5px;border-radius:0;"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </div>
          <div class="row">
            <span class="utsaToday-date"><cfoutput>#DateFormat(todaysDate, "#dateFormatString#")#</cfoutput></span>
            <nav id="departmentalSocialNav" class="float-xs-right">
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-social" ga-event-action="https://www.facebook.com/utsa" href="https://www.facebook.com/utsa"><i class="fa fa-facebook utsa-social-circle-top" aria-hidden="true"></i></a>
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-social" ga-event-action="https://twitter.com/utsa" href="https://twitter.com/utsa"><i class="fa fa-twitter utsa-social-circle-top" aria-hidden="true"></i></a>
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-social" ga-event-action="https://instagram.com/utsa" href="https://instagram.com/utsa"><i class="fa fa-instagram utsa-social-circle-top" aria-hidden="true"></i></a>
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-social" ga-event-action="http://www.linkedin.com/edu/the-university-of-texas-at-san-antonio-19521" href="http://www.linkedin.com/edu/the-university-of-texas-at-san-antonio-19521"><i class="fa fa-linkedin-square utsa-social-circle-top" aria-hidden="true"></i></a>
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-social" ga-event-action="https://www.youtube.com/user/utsa" href="https://www.youtube.com/user/utsa"><i class="fa fa-youtube-play utsa-social-circle-top" aria-hidden="true"></i></a>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="utsatoday-nav">
  <div class="container" style="margin-bottom: 1.5rem;">
    <nav class="navbar " style="border-radius:0;">
      <button class="navbar-toggler hidden-lg-up float-xs-left" data-target="#todayNavbar" data-toggle="collapse" style="margin-top: -4px !important;" type="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="collapse navbar-toggleable-md" id="todayNavbar">
        <ul class="nav navbar-nav float-xs-left">
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/today/categories/roadrunners.html" href="/today/categories/roadrunners.html">Meet a Roadrunner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/experts" href="/experts">Experts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/ucm" href="/ucm">Communications &amp; Marketing</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/ucm/communications/media-relations/media-contacts.html" href="/ucm/communications/media-relations/media-contacts.html">For the Media</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/sombrilla/" href="/sombrilla/">Sombrilla Magazine</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" ga-on="click" ga-event-category="todayClick" ga-event-label="today-nav-bar" ga-event-action="/today/archives/" href="/today/archives/">News Archive</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</section>
