<!---
Today - Announcements
today-announcements.cfm
===

Responsible for outputting announcment blurbs on the main index page for today

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Requires admin/articleManager/act_PrepHomePageArray.cfm to be cfincluded before
this file is included, @see /today/index.cfm

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
--->
<cfinclude template="/today/_inc/admin/articleManager/act_PrepHomePageArray.cfm" />
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="announcementQuery" datasource="#Application.PrimaryDSN#">
  SELECT * FROM Articles
  WHERE
    aroundCampus = 1
    AND (
      DateExpire > now()
      OR DateExpire IS NULL
    )
    AND DateStart < now()
  ORDER BY DateAdded, DateStart DESC
  LIMIT 6
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="announcementQuery" datasource="#Application.PrimaryDSN#">
      SELECT TOP 6 *
      FROM [utsaweb].[dbo].[Articles]
      WHERE aroundCampus = 1
      AND (DateExpire > GETDATE() OR DateExpire is NULL)
      AND DateStart <= GETDATE()
      ORDER BY DateStart DESC;
  </cfquery>
</cfif>


<div class="col-sm-6">
  <div class="row">
    <div class="col-sm-12">
      <span class="utsaToday-SectionTitle">Announcements</span>
      <!---
      <a class="os" href="/today/rss/"><i class="fa fa-rss-square" aria-hidden="true"></i></a>
      --->
    </div>
  </div>

<!-- begin announcements -->
<cfoutput query="announcementQuery">
  <div class="row">
    <div class="col-sm-12">
      <p class="fiveTB">
        <a class="announcements" ga-on="click" ga-event-category="todayClick" ga-event-label="home-announcements" ga-event-action="#XmlFormat(announcementQuery.URL)#" href="#XmlFormat(announcementQuery.URL)#">
          #CleanHighAscii(announcementQuery.title)#
        </a>
      </p>
      <hr>
    </div>
  </div>
</cfoutput>
<!-- end announcments -->

<p class="submitAnnouncements"><a ga-on="click" ga-event-category="todayClick" ga-event-label="home-announcements-intake" ga-event-action="/today/intake/view.cfm?page=intake-around" href="/today/intake/view.cfm?page=intake-around">
  <i class="fa fa-angle-double-right" aria-hidden="true"></i> Submit an Announcement
</a></p>
</div>
