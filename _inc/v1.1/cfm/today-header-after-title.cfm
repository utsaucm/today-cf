<!---
Today - Header After Title
today-header-after-title.cfm
===

Responsible for outputting standard stylesheets for use on all UTSA Today pages

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

This file should be cfincluded after the <title> tag of any UTSA Today template.

- global.min.css is the UCM distributed global minified bootstrap UTSA css file created
  by building [https://bitbucket.org/utsaucm/utsa-bootstrap]

- today.css is the local project's styles, any style changes required for UTSA
  Today should be made in the today.css stylesheet

- any local compoments (bower) should be declared after today.css iff they are used site wide

Changelog:
 - 8/2016: initial version
--->
<!-- begin announcements -->
<link rel="stylesheet" type="text/css" href="/_files/css/global.min.css">
<link rel="stylesheet" type="text/css" href="/today/css/today.css">
<!-- localy imported bower components (CSS) -->
<link href="/today/components/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<link href="/today/components/featherlight/release/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lte IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <script src="/_files/js/selectivizr-min.js"></script>
  <link rel="stylesheet" type="text/css" href="/_files/css/ie8-fixes.css" />
<![endif]-->
