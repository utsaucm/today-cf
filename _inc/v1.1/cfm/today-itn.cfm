<!---
Today - Photo of the Day
today-pod.cfm
===

Responsible for outputting the latest article with a title containing
"Photo of the Day"

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
 - 10/2016: standardizing output with today-marr.cfm
--->
<!-- begin In The News -->
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="inTheNewsQuery" datasource="#Application.PrimaryDSN#">
  SELECT * FROM Articles
  WHERE
    inTheNews = 1
    AND (
      DateExpire > now()
      OR DateExpire IS NULL
    )
    AND DateStart < now()
  ORDER BY DateAdded, DateStart DESC
  LIMIT 1
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="inTheNewsQuery" datasource="#Application.PrimaryDSN#">
      SELECT TOP 1 *
      FROM [utsaweb].[dbo].[Articles]
      WHERE inTheNews = 1
      AND (DateExpire > GETDATE() OR DateExpire is NULL)
      AND DateStart <= GETDATE()
      ORDER BY DateStart DESC;
  </cfquery>
</cfif>

<div class="col-sm-6">
  <span class="utsaToday-SectionTitle">UTSA In The News</span>

  <cfoutput query="inTheNewsQuery">
    <cfset idx = Find(":",  inTheNewsQuery.title ,0) />
    <cfset modTitle = Trim(Mid(inTheNewsQuery.title, idx + 1, Len(inTheNewsQuery.title))) />
    <div class="section group img2">
      <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-itn" ga-event-action="#XmlFormat(inTheNewsQuery.URL)#" href="#XmlFormat(inTheNewsQuery.URL)#">
        <img width="100%" src="#inTheNewsQuery.leadPhotoAddress2#" alt="#modTitle#"  />
        <h3 class="PODheadlineblue">
          #encodeForHTML(modTitle)#
        </h3>
      </a>
    </div>
  </cfoutput>
  <hr>
</div>
<!-- end In The News -->
