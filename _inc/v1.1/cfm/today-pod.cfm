<!---
Today - Photo of the Day
today-pod.cfm
===

Responsible for outputting the latest article with a title containing
"Photo of the Day"

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
 - 10/2016: standardizing output with today-marr.cfm
--->
<!-- begin photo of the day -->
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="photoOfTheDay" datasource="#Application.PrimaryDSN#">
  SELECT * FROM Articles
  WHERE
    Title LIKE '%Photo of the Day%'
    AND (
      DateExpire > now()
      OR DateExpire IS NULL
    )
    AND DateStart < now()
  ORDER BY DateAdded DESC
  LIMIT 1
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="photoOfTheDay" datasource="#Application.PrimaryDSN#">
      SELECT TOP 1 *
      FROM [utsaweb].[dbo].[Articles]
      WHERE Title LIKE '%Photo of the day%'
      AND (DateExpire > GETDATE() OR DateExpire is NULL)
      AND DateStart <= GETDATE()
      ORDER BY DateStart DESC;
  </cfquery>
</cfif>

<div class="col-sm-6">
  <span class="utsaToday-SectionTitle">Photo of the Day</span>

  <cfoutput query="photoOfTheDay">
    <cfset idx = Find(":",  photoOfTheDay.title ,0) />
    <cfset modTitle = Trim(Mid(photoOfTheDay.title, idx + 1, Len(photoOfTheDay.title))) />
    <div class="section group img2">
      <a href="#XmlFormat(photoOfTheDay.URL)#">
        <img width="100%" src="#photoOfTheDay.leadPhotoAddress2#" alt="#modTitle#"  />
        <h3 class="PODheadlineblue">
          #encodeForHTML(modTitle)#
        </h3>
      </a>
    </div>
  </cfoutput>
  <hr>
</div>
<!-- end photo of the day -->
