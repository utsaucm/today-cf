<!---
Today - Newswire
today-newswire.cfm
===

Responsible for outputting newswire blurbs on the main index page for today

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Requires admin/articleManager/act_PrepHomePageArray.cfm to be cfincluded before
this file is included, @see /today/index.cfm

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
--->
<cfsilent>
  <cfinclude template="/today/_inc/admin/articleManager/act_PrepHomePageArray.cfm" />
  <cfif Application.ENV EQ "TEST">
    <!--- TEST: dialect is postgresql --->
    <cfquery name="newswireQuery" datasource="#Application.PrimaryDSN#">
    SELECT * FROM Articles
    WHERE
      internalStory = 1
      AND (
        DateExpire > now()
        OR DateExpire IS NULL
      )
      AND DateStart < now()
    ORDER BY DateAdded, DateStart DESC
    LIMIT #Application.maxNewswire#
    </cfquery>
  <cfelse>
    <!--- ELSE: assume MSSQL dialect --->
    <cfquery name="newswireQuery" datasource="#Application.PrimaryDSN#">
        SELECT TOP #Application.maxNewswire# *
        FROM [utsaweb].[dbo].[Articles]
        WHERE internalStory = 1
        AND (DateExpire > GETDATE() OR DateExpire is NULL)
        AND DateStart <= GETDATE()
        ORDER BY DateStart DESC;
    </cfquery>
  </cfif>
</cfsilent>
<div class="col-sm-6">
  <div class="row">
    <div class="col-sm-12">
      <span class="utsaToday-SectionTitle">UTSA Newswire</span>
      <!---
      <a class="os" href="/today/rss/"><i class="fa fa-rss-square" aria-hidden="true"></i></a>
      --->
    </div>
  </div>

<!-- begin newswire -->
<cfoutput query="newswireQuery">
  <div class="row">
    <div class="col-sm-12">
      <h3 class="newswireheader">
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-newswire" ga-event-action="#XmlFormat(newswireQuery.URL)#" href="#XmlFormat(newswireQuery.URL)#">#CleanHighAscii(newswireQuery.title)#</a>
      </h3>
      <p>#CleanHighAscii(newswireQuery.Submitter)#</p>
      <hr>
    </div>
  </div>
</cfoutput>
<!-- end newswire -->

  <p class="submitCampusNewswire"><a ga-on="click" ga-event-category="todayClick" ga-event-label="home-newswire-intake" ga-event-action="/today/intake/view.cfm?page=intake-around" href="/today/intake/view.cfm?page=intake-around">
    <i class="fa fa-angle-double-right" aria-hidden="true"></i> Submit a Newswire story
  </a></p>
</div>
