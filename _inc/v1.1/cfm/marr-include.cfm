<cfprocessingdirective pageencoding="utf-8"/>
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="meetarr" datasource="#Application.PrimaryDSN#">
  SELECT * FROM Articles
  WHERE (
    dateStart IS NOT NULL
    AND Title LIKE '%Meet a Roadrunner:%'
    OR Title LIKE '%Commencement Spotlight:%'
    AND DateStart < now() )
  ORDER BY DateStart DESC
  LIMIT 3
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="meetarr" datasource="#Application.PrimaryDSN#">
  SELECT        TOP (3) *
  FROM            dbo.Articles
  WHERE (Title LIKE '%Meet a Roadrunner:%' OR Title LIKE '%Commencement Spotlight:%') AND DateStart < GETDATE()
  ORDER BY DateStart DESC
  </cfquery>
</cfif>
<cfset i = 1 />
<div class="row"><div class="col-sm-12">
<cfloop query="meetarr">
  <cfif  i EQ 1>
    <cfset idx = Find(":",  meetarr.title[i] ,0) />
    <cfset modTitle = Trim(Mid(meetarr.title[i], idx + 1, Len(meetarr.title[i]))) />
    <cfoutput>
      <div class="section group img2">
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="include-marr" ga-event-action="http://www.utsa.edu#XmlFormat(meetarr.url[i])#" href="http://www.utsa.edu#XmlFormat(meetarr.url[i])#">
          <img width="100%" src="http://www.utsa.edu#XMLFormat(meetarr.leadPhotoAddress[i])#" alt="#modTitle#" max-height="95%"/>
          <h3 class="PODheadlineblue">
              #encodeForHTML(modTitle)#
          </h3>
        </a>
      </div>
    </cfoutput>
  </cfif>
 	<cfset i++>
</cfloop>
</div></div>
