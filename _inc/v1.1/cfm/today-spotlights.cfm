<!---
Today - Spotlights
today-spotlights.cfm
===

Responsible for outputting spotlight blurbs (image, headline, link) on the main
index page for today, most likely will be links to sombrilla stories/features

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Changelog:
 - 8/2016: initial version, no previous version to migrate from, starting with mock html
 - 8/15/2016: adding initial output from spotlight table in utsaweb db
--->
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="spotlight" datasource="#Application.PrimaryDSN#">
    SELECT * FROM spotlight WHERE
    ACTIVE = 'YES'
    AND START_DATE_TIME < now()
    AND (
      EXPIRE_DATE_TIME > now()
      OR EXPIRE_DATE_TIME IS NULL
    )
    ORDER BY START_DATE_TIME DESC
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="spotlight" datasource="#Application.PrimaryDSN#">
    SELECT * FROM [utsaweb].[dbo].[Spotlight] WHERE
    ACTIVE = 'YES'
    AND (EXPIRE_DATE_TIME > GETDATE() OR EXPIRE_DATE_TIME is NULL)
    AND START_DATE_TIME <= GETDATE()
    ORDER BY START_DATE_TIME DESC;
  </cfquery>
</cfif>
<cfif spotlight.recordcount GT 0>
<div class="utsaToday-MoreNews">
  <div class="row">
    <div class="col-sm-4">
      <span class="utsaToday-SectionTitle">Spotlight</span>
    </div>
  </div>
  <cfoutput query="spotlight">
    <div class="row">
      <div class="col-md-4">
        <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-spotlights" ga-event-action="#TEASER_URL#" href="#TEASER_URL#">
          <img width="100%" src="#RESOURCE_URL#"  alt="#CleanHighAscii(TITLE)#"/>
        </a>
      </div>
      <div class="col-md-8">
        <span class="spotlightblue">
          <strong>
            <a ga-on="click" ga-event-category="todayClick" ga-event-label="home-spotlights" ga-event-action="#TEASER_URL#" href="#TEASER_URL#">
            #CleanHighAscii(TITLE)#
            </a>
          </strong>
          <br/>
          #CleanHighAscii(TEASER)#
          <p class="spotlightfrom">From #TYPE#</p>
        </span>
      </div>
    </div>
    <hr>
  </cfoutput>
</div>
</cfif>
