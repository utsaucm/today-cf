<!---
Today - More News Page
today-more-news-page.cfm
===

Responsible for outputting content for /today/morenews.html

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>

Requires admin/articleManager/act_PrepMoreNewsArray.cfm to be cfincluded before
this file is included, @see /today/index.cfm

Changelog:
 - 8/2016: initial version, adopted from old announcements.cfm code
--->
<cfinclude template="/today/_inc/admin/articleManager/act_PrepMoreNewsArray.cfm">

<div class="utsaToday-MoreNews">
  <div class="row">
    <div class="col-sm-12">
      <span class="pageTitle">More News</span>
    </div>
  </div>
  <hr>
  <cfloop index="iLooper" from="1" to="#moreNewsCount#" step="1">
    <cfif Len(moreStories[iLooper][2]) GT 0 AND Len(moreStories[iLooper][3]) GT 0>
    <cfoutput>
      <div class="row">
          <div class="col-sm-3">
            <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-more-news-page" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#XmlFormat(moreStories[iLooper][5])#" data-id="#moreStories[iLooper][1]#">
              <img src="#XmlFormat(moreStories[iLooper][6])#"  alt="#CleanHighAscii(moreStories[iLooper][2])#"/>
            </a>
          </div>
          <div class="col-sm-9">
            <span class="more2">
              <a ga-on="click" ga-event-category="todayClick" ga-event-label="today-more-news-page" ga-event-action="#xmlFormat(aStories[iLooper][5])#" href="#XmlFormat(moreStories[iLooper][5])#">
                #CleanHighAscii(moreStories[iLooper][2])#
              </a>
            </span>
          </div>
      </div>
      <hr>
    </cfoutput>
    </cfif>
  </cfloop>
  <cfif moreNewsCount EQ moreNews_i>
      <p style="clear:both;">There is currently no more news at this time.</p>
  </cfif>
</div>

<p class="morenews"><a ga-on="click" ga-event-category="todayClick" ga-event-label="today-archive" ga-event-action="archives/" href="archives/">
  <i class="fa fa-angle-double-right" aria-hidden="true"></i> News Archives
</a></p>
