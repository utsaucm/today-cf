<br/>
<div class="row">
  <div class="col-sm-6">
    <cfinclude template="/today/archives/qry_SelectAllCategories.cfm">
    <h3>Browse by Category</h3>
    <form action="index.cfm" method="get">
    	<input type="hidden" name="fuseaction" value="category" />
    	<select class="col-sm-10" name="categoryid">
        <cfoutput query="SelectAllCategories">
            <option value="#Categoryid#">#CategoryName#</option>
        </cfoutput>
    	</select>
    	<input type="submit" value="Go!"/>
    </form>
  </div>
  <div class="col-sm-6">
    <h3 >Browse by Date</h3>
    <cfset sCurrentYear = year(now()) />
    <!--- Change this to include more years from the DB --->
    <cfset sStartYear = 2005 />
    <cfset iCounter = 0 />

    <form action="index.cfm" method="get">
    	<input type="hidden" name="fuseaction" value="date" />
    	<select name="year">
    		<cfloop from="#sStartYear#" to="#sCurrentYear#" index="idx" step="1">
    			<cfoutput>
    				<option value="#sCurrentYear-iCounter#">#sCurrentYear - iCounter#</option>
    			</cfoutput>
    			<cfset iCounter = iCounter + 1 />
    		</cfloop>
    	</select>
    	<select name="month">
    		<option value="1">January</option>
    		<option value="2">February</option>
    		<option value="3">March</option>
    		<option value="4">April</option>
    		<option value="5">May</option>
    		<option value="6">June</option>
    		<option value="7">July</option>
    		<option value="8">August</option>
    		<option value="9">September</option>
    		<option value="10">October</option>
    		<option value="11">November</option>
    		<option value="12">December</option>
    	</select>
    	<input type="submit" value="Go!"/>
    </form>
  </div>
</div>
<br/>
