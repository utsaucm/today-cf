<!--- Note: aroundCampus represents Campus Newswire stories --->
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfQuery name="SelectControlPanelArticles" datasource="#Application.PrimaryDSN#">
    SELECT * FROM Articles
    WHERE dateStart IS NOT NULL
    ORDER BY dateStart DESC
    LIMIT 150
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="SelectControlPanelArticles" datasource="#Application.PrimaryDSN#">
  SELECT TOP 175 * FROM Articles ORDER BY Datestart DESC
  </cfquery>
</cfif>
