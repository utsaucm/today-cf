<!---
Today - Action - Prep Home Page Array
act_PrepHomePageArray.cfm
===

This is the workhorse cfusion include which structures the stories for the home page

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>
@author - Ruben Ortiz <ruben.ortiz@utsa.edu>
@author - Shashi Pinheiro <shashi.pinheiro@utsa.edu>
@author - legacy, I don't know how old this code is

Changelog:
 - 8/2016: initial version, adopted from original code maintained by R. Ortiz
 		- Major refactor, pulled major array offsets to variables and aStories definition to the top of the page
		- Notes:
				- TODO: this page requires <cfinclud'ing other CFM pages and sharing scope, really gnarly stuff
					The multi-dimensional array "aStories" should really be refactored to a CF Struct...
		- Renamed count and index variables to <section>Count, <section>Start and <section>_i
				- *Start and *_i are used in children pages, be careful!
				- *Count variables are used by act_CategorizeStories, be even more careful!
 		- Population of 2nd dimension of the array is **screaming** refactor me to a single cfc function...
		- Unfortuantely, we got bigger fish to fry, so these TODO s will have to wait
--->
<cfsilent>
<cfset articleElementSize = 12 /> <!-- 12 items per entry -->
<cfset topStoryCount = 3 />
<cfset marrCount = 1 />
<cfset moreNewsCount = 10 />
<cfset newsWireCount = 10 />
<cfset announcementCount = 10 />
<cfset itnCount = 1 />
<cfset aStoriesMax = topStoryCount + marrCount + moreNewsCount + newsWireCount + announcementCount />
<!--- This is our result array where all the magic is stored (why aren't we using seperate db queries?) --->
<cfset aStories = ArrayNew(2)>
<cfloop index="i" from="1" to="#aStoriesMax#">
	<cfloop index="j" from="1" to="#articleElementSize#">
		<cfset aStories[i][j] = "">
	</cfloop>
</cfloop>
<!--- array idx pointers (offsets into aStories, why not use a struct?)--->
<cfset topStory_i = 1 />
<cfset marr_i = topStory_i + topStoryCount />
<cfset moreNews_i = marr_i + marrCount />
<cfset newsWire_i = moreNews_i + moreNewsCount />
<cfset announcement_i = newsWire_i + announcementCount />
<cfset itn_i = announcement_i + itnCount />

<!--- starting indexes --->
<cfset topStoryStart = topStory_i />
<cfset marrStart = marr_i />
<cfset moreNewsStart = moreNews_i />
<cfset newsWireStart = newsWire_i />
<cfset announcementStart = announcement_i />
<cfset itnStart = itn_i />

<!--- these are flags --->
<cfset iLeadphoto = 0>
<cfset iLeadphoto2 = 0>
<cfset iLeadphoto3 = 0 />
<cfset meetARoadrunner = 0 />
<!--- accumulators, these are incremented by act_CategorizeStories
	(I know, passing scope is BAD! Why aren't we using seperate queries again?)
	--->
<cfset futureStories = 0 />
<cfset moreNewsStories = 0 />
<cfset newswireStories = 0 />
<cfset expiredStories = 0 />
<cfset archivedStories = 0 />
<cfset announcementStories = 0 />
<cfset itnStories = 0 />
<cfset totalStories = 0 />


<!--- include SQL query: SELECT Top 80 * FROM Articles table ORDER DESC --->
<cfinclude template="sql_SelectControlPanelArticles.cfm" />
<!--- loops thru out data to build aStories --->
<cfloop query="SelectControlPanelArticles">
	<!--- Story Status codes used in act_CategorizeStories.cfm:
		Future Stories (unlimited) 			- 0
		First Lead Photo 	 			- 1
		Second Lead Photo 				- 20
		Third Lead Photo  				- 30
		Meet A Roadrunner (latest) - 40
		More News stories (6 count) 	- 2
		UTSA Newswire  (6 count) 		- 3
		Expired (unlimited) 			- 4
		Archives (unlimited) 			- 5
		Announcement Stories (6 count) 	- 6
		In The News 				- 7
	--->
	<!--- include the category code --->
	<cfinclude template="act_CategorizeStories.cfm">
	<!--- Make updates to the Array --->
	<!---<p>#articleid# - #title# - #teaser# - #datestart# - #url#</p>	 --->
	<cfif (storyStatus is 4) or (storystatus is 5) or (storystatus is 0)>
		<!--- skip expired, archived or future stories --->
		<!--- skip: do not add to the aStories array for display) --->
	<cfelseif (storyStatus is 1)>
		<!--- Set Lead photo to the first slot--->
		<cfset aStories[1][1] = ArticleID>
		<cfset aStories[1][2] = title>
		<cfset aStories[1][3] = teaser>
		<cfset aStories[1][4] = datestart>
		<cfset aStories[1][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[1][6] = leadphotoAddress>
		<cfset aStories[1][7] = leadPhoto>
		<cfset aStories[1][8] = internalStory>
		<cfset aStories[1][9] = aroundCampus>
		<cfset aStories[1][10] = "Lead Photo Story">
		<cfset aStories[1][11] = leadphotoAddress2>
		<!--- Let the rest of the code know that the lead photo has run --->
		<cfset iLeadPhoto = 1>
	<cfelseif (storyStatus is 20)>
		<!--- Set Lead photo to the second slot--->
		<cfset aStories[2][1] = ArticleID>
		<cfset aStories[2][2] = title>
		<cfset aStories[2][3] = teaser>
		<cfset aStories[2][4] = datestart>
		<cfset aStories[2][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[2][6] = leadphotoAddress>
		<cfset aStories[2][7] = leadPhoto>
		<cfset aStories[2][8] = internalStory>
		<cfset aStories[2][9] = aroundCampus>
		<cfset aStories[2][10] = "Lead Photo Story 2">
		<cfset aStories[2][11] = leadphotoAddress2>
		<!--- Let the rest of the code know that the lead photo has run --->
		<cfset iLeadPhoto2 = 1>
	<cfelseif (storyStatus is 30)>
		<!--- Set Lead photo to the third slot--->
		<cfset aStories[3][1] = ArticleID>
		<cfset aStories[3][2] = title>
		<cfset aStories[3][3] = teaser>
		<cfset aStories[3][4] = datestart>
		<cfset aStories[3][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[3][6] = leadphotoAddress>
		<cfset aStories[3][7] = leadPhoto>
		<cfset aStories[3][8] = internalStory>
		<cfset aStories[3][9] = aroundCampus>
		<cfset aStories[3][10] = "Lead Photo Story 3">
		<cfset aStories[3][11] = leadphotoAddress2>
		<!--- Let the rest of the code know that the lead photo has run --->
		<cfset iLeadPhoto3 = 1>
	<cfelseif (storyStatus is 40)>
		<cfset aStories[4][1] = ArticleID>
		<cfset aStories[4][2] = title>
		<cfset aStories[4][3] = teaser>
		<cfset aStories[4][4] = datestart>
		<cfset aStories[4][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[4][6] = leadphotoAddress>
		<cfset aStories[4][7] = leadPhoto>
		<cfset aStories[4][8] = internalStory>
		<cfset aStories[4][9] = aroundCampus>
		<cfset aStories[4][10] = "Meet a Roadrunner">
		<cfset meetARoadrunner = 1>
		<!--- More News: storyStatus is 2 --->
	<cfelseif (storyStatus is 2) and moreNewsStories LTE moreNewsCount and moreNews_i LTE newsWire_i>
		<!--- <cfoutput>StoryStatus is 2 <Br /></cfoutput>	 --->
		<cfset aStories[moreNews_i][1] = ArticleID>
		<cfset aStories[moreNews_i][2] = REReplaceNoCase(title, "Meet a Roadrunner: ", "") >
		<cfset aStories[moreNews_i][3] = teaser>
		<cfset aStories[moreNews_i][4] = datestart>
		<cfset aStories[moreNews_i][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[moreNews_i][6] = leadphotoAddress>
		<cfset aStories[moreNews_i][7] = leadPhoto>
		<cfset aStories[moreNews_i][8] = internalStory>
		<cfset aStories[moreNews_i][8] = aroundCampus>
		<cfset aStories[moreNews_i][10] = "More News story">
		<cfset aStories[moreNews_i][11] = leadphotoAddress2>
		<cfset moreNews_i++>
		<!--- UTSA NewsWire: storyStatus is 3 --->
	<cfelseif (storyStatus is 3) AND (newswireStories LTE newsWireCount AND newsWire_i LTE announcement_i)>
		<!---<cfoutput>StoryStatus is 3 <Br /></cfoutput>--->
		<cfset aStories[newsWire_i][1] = ArticleID>
		<cfset aStories[newsWire_i][2] = title>
		<cfset aStories[newsWire_i][3] = teaser>
		<cfset aStories[newsWire_i][4] = datestart>
		<cfset aStories[newsWire_i][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[newsWire_i][6] = leadphotoAddress>
		<cfset aStories[newsWire_i][7] = leadPhoto>
		<cfset aStories[newsWire_i][8] = internalStory>
		<cfset aStories[newsWire_i][9] = aroundCampus>
		<cfset aStories[newsWire_i][10] = Submitter>
		<cfset aStories[newsWire_i][11] = "UTSA NewsWire story">
		<cfset aStories[newsWire_i][12] = leadphotoAddress2>
		<cfset newsWire_i++ />
		<!--- Announcements: storyStatus is 6 --->
	<cfelseif (storyStatus is 6) AND (announcementStories LTE announcementCount and announcement_i LTE aStoriesMax)>
		<cfset aStories[announcement_i][1] = ArticleID>
		<cfset aStories[announcement_i][2] = title>
		<cfset aStories[announcement_i][3] = teaser>
		<cfset aStories[announcement_i][4] = datestart>
		<cfset aStories[announcement_i][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[announcement_i][6] = leadphotoAddress>
		<cfset aStories[announcement_i][7] = leadPhoto>
		<cfset aStories[announcement_i][8] = internalStory>
		<cfset aStories[announcement_i][9] = aroundCampus>
		<cfset aStories[announcement_i][10] = Submitter>
		<cfset aStories[announcement_i][11] = "Announcement">
		<cfset aStories[announcement_i][12] = leadphotoAddress2>
		<cfset announcement_i++ />
	<cfelseif (storyStatus is 7) AND (itnStories LTE itnCount and itn_i LTE aStoriesMax)>
		<cfset aStories[itn_i][1] = ArticleID>
		<cfset aStories[itn_i][2] = title>
		<cfset aStories[itn_i][3] = teaser>
		<cfset aStories[itn_i][4] = datestart>
		<cfset aStories[itn_i][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset aStories[itn_i][6] = leadphotoAddress>
		<cfset aStories[itn_i][7] = leadPhoto>
		<cfset aStories[itn_i][8] = internalStory>
		<cfset aStories[itn_i][9] = aroundCampus>
		<cfset aStories[itn_i][10] = Submitter>
		<cfset aStories[itn_i][11] = "Announcement">
		<cfset aStories[itn_i][12] = leadphotoAddress2>
		<cfset itn_i++ />
	</cfif>
</cfloop>

<!--- Counter vars for the home page --->
<cfset featuredNewsEmptyCounter = 0 />
<cfset featuredNewsEmptyNum = 4 />

<cfset moreNewsEmptyCounter = 0 />
<cfset moreNewsEmptyNum =  6 />

<cfset aroundCampusEmptyCounter = 0 />
<cfset aroundCampusEmptyNum = 6 />

<cfset announceEmptyCounter = 0 />
<cfset announceEmptyNum = 6 />

<cfset itnEmptyCounter = 0 />
<cfset itnEmptyNum = 1 />
</cfsilent>
