<!--- 	Categorize our stories...
		This script will return a storyStatus value for each story based on the key below.
		It must be called in conjunction with a query. --->

<!--- Story Status codes:
Future Stories (unlimited) 			- 0
Lead Photo (1) 					- 1
Second Lead Photo 				- 20
Third Lead Photo  				- 30
More News stories (6 count) 	- 2
UTSA Newswire  (6 count) 		- 3
Expired (unlimited) 			- 4
Archives (unlimited) 			- 5
Announcement Stories (6 count) 	- 6
Photo of the day 				- 7
Meet A Roadrunner				- 8
--->

<!--- updated by Ruben O. on 9-11-15 --->
<!--- flag var, gets set to 1 when lead photo is also a 'photo of the day' story --->
<cfset meetARoadrunner = 0 />
<cfset pod = 0 />

<!--- 09-11-15 Creating a new flag for photo of the day --->
<cfif FindNoCase("UTSA Photo of the day:", #SelectControlPanelArticles.Title#)>
	<cfset pod = 1 />
</cfif>

<cfif FindNoCase("Meet a Roadrunner:", #SelectControlPanelArticles.Title#)>
	<cfset meetARoadrunner = 1 />
</cfif>

<!--- 0: Future stories --->
<cfif datestart gt now()>
	<cfset storyStatus = 0 />

<!--- 4: Expired --->
<cfelseif (dateexpire is not "") and (dateexpire lt now())>
	<cfset storyStatus = 4 />

<!--- 1: Lead photo --->
<cfelseif leadphoto is 1 and leadPhotoAddress NEQ "" and ileadphoto is 0 and inTheNews is 0 and pod is 0 and meetARoadrunner is 0>
	<cfset iLeadPhoto = 1 />
	<cfset storyStatus = 1 />

<!--- updated by Ruben O. on 9-9-09 --->
<cfelseif leadPhoto is 1 and leadPhotoAddress NEQ "" and ileadphoto2 is 0 and inTheNews is 0 and pod is 0 and meetARoadrunner is 0>
	<cfset iLeadPhoto2 = 1 />
  <cfset storyStatus = 20 />

<!--- updated by Ruben O. on 9-9-09 --->
<cfelseif leadPhoto is 1 and leadPhotoAddress NEQ "" and ileadphoto3 is 0 and inTheNews is 0 and pod is 0 and meetARoadrunner is 0>
  <cfset iLeadPhoto3 = 1 />
  <cfset storyStatus = 30 />

<!--- adding MARR section to home page array --->
<cfelseif meetARoadrunner is 1>
	<cfset storyStatus = 40 />

<!--- Make all other leadPhoto stories after the first 3 - More News stories: --->
<cfelseif leadPhoto is 1 and leadPhotoAddress NEQ "" and ileadphoto2 is 1 and ileadphoto3 is 1 and inTheNews is 0 and pod is 0 and meetARoadrunner is 0>
	<cfset storyStatus = 2 />
	<cfset moreNewsStories++ />
	<cfset totalStories++ />

<cfelseif (moreNewsStory is true) AND (inTheNews is 0) and (meetARoadrunner is 0) and (pod is 0)>
	<cfset storyStatus = 2 />
	<cfset moreNewsStories++ />
	<cfset totalStories++ />

<!--- UTSA NewsWire (internal) stories (6) --->
<cfelseif internalStory is true>
	<cfset storyStatus = 3 />
  <cfset newswireStories++ />
	<cfset totalStories++ />

<cfelseif aroundCampus is true> <!--- Announcements --->
	<cfset storyStatus = 6 />
 	<cfset announcementStories++ />
	<cfset totalStories++ />

<cfelseif (inTheNews is true) AND (meetARoadrunner is 0)>
	<cfset storyStatus = 7 />
	<cfset itnStories++ />
	<cfset totalStories++ />
<!--- 5: Archive --->
<cfelse>
	<cfset storyStatus = 5 />
  <cfset archivedStories++ />
</cfif>
