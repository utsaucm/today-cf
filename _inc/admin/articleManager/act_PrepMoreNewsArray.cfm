<!---
Today - Action - Prep More News Page Array
act_PrepMoreNewsArray.cfm
===

This is a slimmed down version of act_PrepHomePageArray designed to only prepare stories
for the more news page on utsa.edu/today/morenews.html

@version - Today 1.1 (August 2016)
@author - Eric Ramirez <eric.ramirez3@utsa.edu>
@author - John David Garza <john.garza@utsa.edu>
@author - Ruben Ortiz <ruben.ortiz@utsa.edu>
@author - Shashi Pinheiro <shashi.pinheiro@utsa.edu>
@author - legacy, I don't know how old this code is

@see - act_PrepHomePageArray.cfm for more information

Changelog:
 - 8/2016 (Today v1.1): initial version, adopted from original code maintained by R. Ortiz

--->
<cfset articleElementSize = 12 /> <!-- 12 items per entry -->
<cfset moreNewsCount = 25 />
<!--- This is our result array where all the magic is stored (why aren't we using seperate db queries?) --->
<cfset moreStories = ArrayNew(2)>
<cfloop index="i" from="1" to="#moreNewsCount#">
	<cfloop index="j" from="1" to="#articleElementSize#">
		<cfset aStories[i][j] = "">
	</cfloop>
</cfloop>
<!--- array idx pointers (offsets into aStories, why not use a struct?)--->
<cfset moreNews_i = 1 />
<!--- starting indexes --->
<cfset moreNewsStart = 1 />
<!--- accumulators, these are incremented by act_CategorizeStories
	(I know, passing scope is BAD! Why aren't we using seperate queries again?)
	--->
<!--- these are flags --->
<cfset iLeadphoto = 0>
<cfset iLeadphoto2 = 0>
<cfset iLeadphoto3 = 0 />
<cfset meetARoadrunner = 0 />
<!--- accumulators, these are incremented by act_CategorizeStories
	(I know, passing scope is BAD! Why aren't we using seperate queries again?)
	--->
<cfset futureStories = 0 />
<cfset moreNewsStories = 0 />
<cfset newswireStories = 0 />
<cfset expiredStories = 0 />
<cfset archivedStories = 0 />
<cfset announcementStories = 0 />
<cfset itnStories = 0 />
<cfset totalStories = 0 />



<!--- include SQL query: SELECT Top 80 * FROM Articles table ORDER DESC --->
<cfinclude template="sql_SelectControlPanelArticles.cfm">
<!--- loops thru out data to build aStories --->
<cfloop query="SelectControlPanelArticles">
<!--- Story Status codes used in act_CategorizeStories.cfm:
	More News stories (25 count) 	- 2
	Archives (unlimited) 			- 5
--->
	<!--- include the category code --->
	<cfinclude template="act_CategorizeStories.cfm">
	<!--- Make updates to the Array --->
	<!---<p>#articleid# - #title# - #teaser# - #datestart# - #url#</p>	 --->
	<cfif (storyStatus is 4) or (storystatus is 5) or (storystatus is 0)>
		<!--- skip expired, archived or future stories --->
		<!--- skip: do not add to the aStories array for display) --->
	<cfelseif ((storyStatus is 2) or (storystatus is 5)) and moreNewsStories LTE moreNewsCount>
		<!--- <cfoutput>StoryStatus is 2 <Br /></cfoutput>	 --->
		<cfset moreStories[moreNews_i][1] = ArticleID>
		<cfset moreStories[moreNews_i][2] = title>
		<cfset moreStories[moreNews_i][3] = teaser>
		<cfset moreStories[moreNews_i][4] = datestart>
		<cfset moreStories[moreNews_i][5] = SelectControlPanelArticles.URL[SelectControlPanelArticles.currentRow]>
		<cfset moreStories[moreNews_i][6] = leadphotoAddress>
		<cfset moreStories[moreNews_i][7] = leadPhoto>
		<cfset moreStories[moreNews_i][8] = internalStory>
		<cfset moreStories[moreNews_i][8] = aroundCampus>
		<cfset moreStories[moreNews_i][10] = "More News story">
		<cfset moreStories[moreNews_i][11] = leadphotoAddress2>
		<cfset moreNews_i++>
		<!--- UTSA NewsWire: storyStatus is 3 --->
	</cfif>
</cfloop>
