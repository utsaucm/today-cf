<cfquery name="photoOfTheDay">
    SELECT *     
    FROM [utsaweb].[dbo].[Articles]
    WHERE Title LIKE '%Photo of the day%' AND (DateExpire > GETDATE() OR DateExpire is NULL) AND DateStart <= GETDATE()
    ORDER BY DateStart DESC;
</cfquery>          

<!--- Photo of the day --->	
<div class="section group" id="todayPhotoOfTheDay">
      <!---<cfif photoOfTheDay.recordcount EQ 1>--->
          <cfoutput query="photoOfTheDay">
              <img src="#photoOfTheDay.leadPhotoAddress2#" alt=""  width="100%"  height="348px" />
              <a href="#XmlFormat(photoOfTheDay.URL)#">#photoOfTheDay.Title#</a>
              <p>#photoOfTheDay.Teaser#</p>
              <hr />
          </cfoutput>
  <!---    </cfif>--->
</div>