<!---
<cfquery name="photoOfTheDay">
    SELECT TOP 1 *
    FROM [utsaweb].[dbo].[Articles]
    WHERE Title LIKE '%Photo of the day%' AND (DateExpire > GETDATE() OR DateExpire is NULL) AND DateStart <= GETDATE()
    ORDER BY DateStart DESC;
</cfquery>
--->
<div class="section group" id="todayPhotoOfTheDay">
  <!--- Meet a Roadrunner --->
	<!--#include file="/today/inc/meetarr.html"-->
  <div class="col span_6_of_12">
        <span class="utsaToday-SectionTitle">Meet a Roadrunner</span>
        <div class="section group img2">
            <img src="http://www.utsa.edu/today/images/davidtueme.jpg" alt="" width="297">
            <h3  class="photobottom"><a href="http://www.utsa.edu/today/2016/05/davidtueme.html">Meet David Tueme '14</a></h3>
        </div>
  </div>
  <div class="col span_6_of_12">
      <span class="utsaToday-SectionTitle">Photo of the Day</span>
      <div class="section group  img2">
        <div class="featherlightGallery">
          <a  alt="Photo of the day" href="http://www.utsa.edu/today/2016/06/pod-robot.html" data-featherlight="http://haldev.utsa.edu/today/img/pod-robotwcaption.jpg"><img alt="Photo of the day" src="img/photoofday.jpg" width="297">
          <h3 class="photobottom">Click to reveal today's photo</a></h3>
        </div>
      </div>
  </div>
  <hr>
</div>



<!--- Photo of the day --->
<!---
<div class="section group" id="todayPhotoOfTheDay">
      <cfif photoOfTheDay.recordcount EQ 1>
          <cfoutput query="photoOfTheDay">
              <img src="#photoOfTheDay.leadPhotoAddress2#" alt=""  width="100%"  height="348px" />
              <a href="#XmlFormat(photoOfTheDay.URL)#">#photoOfTheDay.Title#</a>
              <p>#photoOfTheDay.Teaser#</p>
              <hr />
          </cfoutput>
      </cfif>
</div>
--->
