<cfinclude template="/today/_inc/v1.1/cfm/todays-date.cfm" />

<div class="col span_7_of_12 utsaToday-Logo">
  <a href="/today"><img src="/today/img/utsaTodayLogoBlue.png" width="45%" alt="UTSA Today"></a>
</div>
<div class="col span_5_of_12">
  <span class="utsaToday-date"><cfoutput>#today#</cfoutput></span>
  <cfinclude template="/today/inc/departmentalSocialNav.html">
</div>
