<div class="section group" id="mainStoryHighlights">
  <!-- STORY #1 -->
  <div class="col span_12_of_12 mainStoryHighlight">
      <cfloop index="iLooper" from="1" to="1" step="1">
          <cfoutput>
              <a href="#xmlFormat(aStories[iLooper][5])#">
              <img src="#XMLFORMAT(aStories[1][11])#" alt="#CleanHighAscii(aStories[iLooper][2])#">
                <div class="mainheadlineblue">
                #CleanHighAscii(aStories[iLooper][2])#
                </div>
              </a>
          </cfoutput>
      </cfloop>
  </div>

  <div class="section group mainHighlightMobile">
      <cfloop index="iLooper" from="1" to="1" step="1">
          <cfoutput>
              <a href="#XMLFORMAT(aStories[iLooper][5])#">
              <img src="#XMLFORMAT(aStories[1][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#">
              <div class="mainheadlineblue">
                #CleanHighAscii(aStories[iLooper][2])#
              </div>
              </a>
          </cfoutput>
      </cfloop>
  </div>

  <div class="section group">
    <!-- STORY #2 -->
    <div class="col span_6_of_12 mainHighlightMobile2">
      <cfloop index="iLooper" from="2" to="2" step="1">
      <cfoutput>
      <a href="#XMLFORMAT(aStories[iLooper][5])#">
        <img src="#XMLFORMAT(aStories[2][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#"/>
        <div class="secondaryheadlineblue">
          #CleanHighAscii(aStories[iLooper][2])#
        </div>
      </a>
      </cfoutput>
      </cfloop>
    </div>

    <!-- STORY #3 -->
    <div class="col span_6_of_12 mainHighlightMobile2">
      <cfloop index="iLooper" from="3" to="3" step="1">
      <cfoutput>
      <a href="#XMLFORMAT(aStories[iLooper][5])#">
        <img src="#XMLFORMAT(aStories[3][6])#" alt="#CleanHighAscii(aStories[iLooper][2])#" />
        <div class="secondaryheadlineblue">
          #CleanHighAscii(aStories[iLooper][2])#
        </div>
      </a>
      </cfoutput>
      </cfloop>
    </div>
  </div>

</div>
