<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="/_files/css/global.min.css">
<link rel="stylesheet" type="text/css" href="/today/css/today.css">
<!--[if lt IE 9]>
<script src="/_inc/js/selectivizr-min.js"></script>
<link rel="stylesheet" type="text/css" href="/_inc/css/ie8-fixes.css" />
<![endif]-->
