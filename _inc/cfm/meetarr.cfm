<!---
<cfquery name="meetarr">
	SELECT        TOP (3) ArticleID, DateAdded, DateStart, DateExpire, Title, Teaser, [Content], URL, CategoryID, Author,  leadPhoto, leadPhotoAddress
	FROM            dbo.Articles
	<!--- WHERE Title LIKE '%Meet A Roadrunner:%' AND (DateAdded <= DATEADD(dd, 2, DATEADD(ww, DATEDIFF(ww, 0, DATEADD(dd, -2, GETDATE())) - 2, 0)))--->
    WHERE Title LIKE '%Meet A Roadrunner:%' AND (DateAdded <= DATEADD(dd, 1, DATEADD(ww, DATEDIFF(ww, 0, DATEADD(dd, -1, GETDATE())) - 1, 0)))
	ORDER BY DateAdded DESC
</cfquery>
--->

<cfquery name="meetarr">
	SELECT        TOP (3) ArticleID, DateAdded, DateStart, DateExpire, Title, Teaser, [Content], URL, CategoryID, Author,  leadPhoto, leadPhotoAddress
	FROM            dbo.Articles
    WHERE (Title LIKE '%Meet a Roadrunner:%' OR Title LIKE '%Commencement Spotlight:%') AND DateStart < GETDATE()
	ORDER BY DateAdded DESC
</cfquery>
<!--- Get the date of Wednesday from last week --->
<cfquery name="lastwed">
	SELECT LastWeekWednesday = DATEADD(dd, 2, DATEADD(ww, DATEDIFF(ww, 0, DATEADD(dd, -2, GETDATE())) - 3, 0))
</cfquery>

<!---<cfdump var="#meetarr#">--->
<!---<cftry>--->
    <!---<cfif meetarr.recordcount GTE 0 AND DateCompare(#DateFormat(meetarr.DateStart)#, #DateFormat(lastwed.LastWeekWednesday)#) LTE 0>--->
     <!---   <cfif meetarr.recordcount GT 0>--->

    <cfset i = 1>
        <cfloop query="meetarr">
			<cfif  i EQ 3>
                <!--- Strip out the "Meet a Roadrunner:" substring in the title --->
                <!---<cfset idx = Find(":", meetarr.title ,0)>
                <cfset modTitle = Trim(Mid(meetarr.title, idx + 1, Len(meetarr.title)))>
                <cfoutput>
                    <div class="section group">
                        <img src="#XMLFormat(meetarr.leadPhotoAddress)#" alt="" width="297" height="168" />
                        <h3 style="font-size: 0.95em;"><a href="#XmlFormat(meetarr.url)#">#encodeForHTML(modTitle)#</a></h3>
                        <p>#encodeForHTML(meetarr.Teaser)#</p>
                    </div>
                </cfoutput>--->

                <cfset idx = Find(":",  meetarr.title[i] ,0)>
                <cfset modTitle = Trim(Mid(meetarr.title[i], idx + 1, Len(meetarr.title[i])))>
                <cfoutput>
                    <div class="section group">
                        <img src="http://www.utsa.edu#XMLFormat(meetarr.leadPhotoAddress[i])#" alt="" width="297" />
                        <h3 style="font-size: 0.95em;"><a href="http://www.utsa.edu#XmlFormat(meetarr.url[i])#">#encodeForHTML(modTitle)#</a></h3>
                        <p>#encodeForHTML(meetarr.Teaser[i])#</p>
                    </div>
                </cfoutput>

            </cfif>
         	<cfset i++>
           </cfloop>
    <!---<cfelse>
        <!--- No output --->
    </cfif>
    <cfcatch type="any">
        <!--- do nothing --->
    </cfcatch>--->
<!---</cftry>--->
