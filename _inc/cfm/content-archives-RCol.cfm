<div class="col2">
    <div class="content-archives">
		<cfParam Name="attributes.fuseaction" Default="archive">
		<h1>Archived Stories</h1>
			<cfif structkeyexists(URL, "fuseaction")>
				<!---<cfif attributes.fuseaction contains "category">--->
                <cfif (#URL.Fuseaction# EQ "category")>
                    <cfinclude template="/today/archives/dsp_ArticlesByCategory.cfm">
                <!---<cfelseif attributes.fuseaction contains "date">--->
                <cfelseif (#URL.Fuseaction# EQ "date")>
                    <cfinclude template="/today/archives/dsp_ArticlesByDate.cfm">
                <cfelse>
                    <cfinclude template="/today/archives/dsp_ArchivePage.cfm">
                </cfif>
			<cfelse>
            	<cfinclude template="/today/archives/dsp_ArchivePage.cfm">
            </cfif>
    </div>
</div> <!-- end col2 -->
