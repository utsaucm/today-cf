<cfloop index="iLooper" from="1" to="4" step="1">             
    <cfif Len(aStories[iLooper][2]) GT 0 AND Len(aStories[iLooper][3]) GT 0>
        <cfoutput>  
            <div class="section group clearfix">
              <div class="col span_3_of_12">
                <img src="#encodeForHTML(aStories[iLooper][6])#" alt="" />
              </div>
              <div class="col span_9_of_12">
                <h3><a href="#xmlFormat(aStories[iLooper][5])#">#encodeForHTML(aStories[iLooper][2])#</a></h3>
                <p>#encodeForHTML(aStories[iLooper][3])#</p>
              </div>
            </div>
            <hr />
        </cfoutput>
    <cfelse>
        <cfset featuredNewsEmptyCounter++>   
    </cfif>
</cfloop>
 <cfif featuredNewsEmptyCounter EQ featuredNewsEmptyNum>
    <p>There are no Featured stories at this time.</p>
</cfif>    