<div class="col1" style="padding:5px;">
    <div class="content-top">
      <cfinclude template="/today/_inc/v1.1/sys/qry_SelectAllCategories.cfm">
    <h2>Browse by Category</h2>

    <ul class="archive-category">
        <cfoutput query="SelectAllCategories">
            <li><a href="/today/archives/index.cfm?fuseaction=category&iStartRow=1&categoryid=#Categoryid#">#CategoryName#</a></li>
        </cfoutput>
    </ul>
    <cfinclude template="/today/archives/dsp_ArticlesByDateNav.cfm">
    </div>
</div> <!-- end col1 -->
