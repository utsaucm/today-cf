<cfloop index="iLooper" from="4" to="9" step="1">
  <cfif Len(aStories[iLooper][2]) GT 0 AND Len(aStories[iLooper][3]) GT 0>
  <cfoutput>
    <div class="section group clearfix">
      <div class="col span_12_of_12">
          <h3 class="rightnew">
            <a href="#XmlFormat(aStories[iLooper][5])#">#XmlFormat(aStories[iLooper][2])#</a>
          </h3>
      </div>
    </div>
    <hr />
  </cfoutput>
  <cfelse>
    <cfset moreNewsEmptyCounter++>
  </cfif>
</cfloop>
<cfif moreNewsEmptyCounter EQ moreNewsEmptyNum>
    <p>There are no More News stories at this time.</p>
</cfif>
