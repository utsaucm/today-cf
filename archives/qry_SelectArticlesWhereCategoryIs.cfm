<cftry>
	<cfif Application.ENV EQ "TEST">
	  <!--- TEST: dialect is postgresql --->
		<cfquery name="SelectArticlesWhereCategoryIs" datasource="#Application.PrimaryDSN#" timeout="500">
			SELECT a.* FROM Articles a, RSSCategoryArticleRelations r WHERE
			a.ArticleID = r.ArticleID
			AND r.CategoryID = <cfqueryparam value="#categoryID#" cfsqltype="cf_sql_integer" maxlength="15" />
			AND a.dateStart < now()
		</cfquery>
	<cfelse>
	  <!--- ELSE: assume MSSQL dialect --->
		<cfquery name="SelectArticlesWhereCategoryIs" datasource="utsaweb-syntaxprod">
			SELECT     dbo.Articles.*
			FROM         dbo.Articles INNER JOIN
								  dbo.RSSCategoryArticleRelations ON dbo.Articles.ArticleID = dbo.RSSCategoryArticleRelations.ArticleID
			<!---WHERE     (dbo.RSSCategoryArticleRelations.CategoryID = #categoryID#) --->
			WHERE     (dbo.RSSCategoryArticleRelations.CategoryID = <cfqueryparam value="#categoryID#" cfsqltype="cf_sql_integer" maxlength="15">)
				AND dbo.Articles.DateStart <= GETDATE()
			Order by datestart desc;
		</cfquery>
	</cfif>
<cfcatch type="any">
	<cfoutput><p>#errorMsg# </p></cfoutput>
    </cfcatch>
</cftry>
