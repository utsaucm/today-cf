<!--- This query must be used in conjunction with the stored procedure that is listed below.
	Please note that a copy of this is running in the offices directory search, which is the front end.
	At some point this code should be turned into a tag that can be utilized on the offices directory search.
--->
<!---
<cfQuery name="SelectCompleteOfficeHierarchy" datasource="#request.PrimaryDSN#">
	--drop table ##holder
	CREATE TABLE ##Holder(SerialNo int,OfficeID int, Head varchar(300),levelss int, homepage int, phone varchar(50))
	EXEC ExpandHierarchy #startLevel#
	SELECT ##Holder.*,links.url 
		FROM ##Holder
		LEFT Join links 
		on Links.LinkID=##Holder.homepage

		ORDER BY SerialNo,Head
	drop table ##holder
</cfquery>
--->


<cfQuery name="SelectCompleteOfficeHierarchy" datasource="#request.PrimaryDSN#">
	<!--- --drop table ##holder --->
	CREATE TABLE ##Holder(SerialNo int,OfficeID int, Head varchar(300),levelss int, homepage int, phone varchar(50), prefix varchar(50))
	EXEC ExpandHierarchy #startLevel#
	SELECT ##Holder.*,links.url 
		FROM ##Holder
		LEFT Join links 
		on Links.LinkID=##Holder.homepage

		ORDER BY SerialNo,Head
	drop table ##holder
</cfquery>
<!---
/* ################################################
   # This procedure is used in conjunction with
   # the query "SelectCompleteOfficeHierarchy".
   # A working copy of the query can be found below
   # this proceduer.  Last updated 6/27/03.
   ################################################ */


CREATE      PROCEDURE ExpandHierarchy
@Start INT -- Where to Start
AS
/*Source Table
CREATE TABLE [offices] (
	[officeID] 	int ,
	officeParent int,
	[head] 	varchar(50),
) 
*/

SET NOCOUNT ON
DECLARE @nLevel INT, @Current INT
CREATE TABLE #Stack( nLevel INT, KeyNo INT)
CREATE TABLE #Hierarchy(SerialNo INT identity, nLevel INT ,KeyNo INT)

--

IF @start IS NULL
 SELECT TOP 1  @nLevel=1,@current=OfficeID FROM Offices WHERE  officeParent IS NULL ELSE
 SELECT TOP 1 @nLevel=1,@current=OfficeID fROM Offices WHERE OfficeID=@Start 

INSERT INTO #stack VALUES (@nLevel,@current)

WHILE (@nLevel >0)
 BEGIN
	IF EXISTS (SELECT * FROM #stack WHERE nLevel =@nLevel) 
	 -- Any thing in current Level

	 BEGIN
		-- Pop One Record
		SELECT TOP  1 @Current =KeyNo
			FROM #Stack WHERE @nLevel=nLevel
		DELETE FROM #Stack WHERE KeyNo=@Current AND @nLevel=nLevel
		--Pop One Record

		-- Insert the record into hierarchy Table
		INSERT INTO #Hierarchy( nLevel,KeyNo)
			VALUES (@nLevel,@Current)

		--Insert Leaf Records (One with no Children) Directly in 
		--#hierarchy Table

		INSERT INTO #Hierarchy( nLevel,KeyNo) 
			SELECT  @nLevel+1, a.OfficeID 
				FROM Offices a LEFT OUTER JOIN Offices b
					ON b.OfficeParent=a.OfficeID
				  	WHERE a.OfficeParent=@Current AND b.OfficeID IS NULL

		--Insert non-Leaf (One with have some children) in #Stack 
		--Table
		INSERT INTO #stack
			SELECT @nLevel+1, a.OfficeID 
				FROM Offices a INNER JOIN Offices b
				ON b.OfficeParent=a.OfficeID  
				WHERE a.officeParent=@Current

		--if any Leaf Found increment the @nLevel
		IF @@RowCount >0
			SELECT @nLevel=@nLevel +1
	  END
 	ELSE -- if (exists (Select  * from #stack where  nLevel =@nLevel)
		-- Nothing in current Level Go to Previous Level
		SELECT @nLevel= @nLevel -1
 End

INSERT INTO #HOlder
	SELECT a.SerialNo,OfficeID,Space((a.nLevel -1) * 3)+b.OfficeName,a.nLevel,b.homepage,b.phone
		FROM #Hierarchy a, offices b
		WHERE b.OfficeID =a.KeyNo
		ORDER BY a.SerialNo
SET NOCOUNT OFF







GO



--->

<!---
<cfQuery name="SelectCompleteOfficeHierarchy" datasource="#request.PrimaryDSN#">
	--drop table ##holder
	CREATE TABLE ##Holder(SerialNo int,OfficeID int, Head varchar(300),levelss int, homepage int, phone varchar(50))
	EXEC ExpandHierarchy #startLevel#
	SELECT ##Holder.*,links.url 
		FROM ##Holder
		LEFT Join links 
		on Links.LinkID=##Holder.homepage

		ORDER BY SerialNo,Head
	drop table ##holder
</cfquery>
--->