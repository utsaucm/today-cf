<cfquery datasource="#request.PrimaryDSN#" name="InsertRetrospectiveStart">
SET NOCOUNT ON
INSERT INTO Retrospectives (RetrospectiveDate)
VALUES ('#RetrospectiveDate#')
SELECT ThisID = @@Identity
SET NOCOUNT OFF
</cfquery>