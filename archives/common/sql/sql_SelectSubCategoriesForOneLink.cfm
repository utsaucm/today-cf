<cfQuery name="SelectSubCategoriesForOneLink" datasource="#request.PrimaryDSN#">
Select subCategories.subcategoryName, subCategories.subCategoryID
	FROM subCategories 
	WHERE subCategories.subCategoryID not in (#subCategoryIDs#)
	ORDER BY subCategories.subCategoryName
</cfquery>