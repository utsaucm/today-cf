		<!--- iStartRow displaying with record 1 if not specified via url --->
		<cfParam name="iStartRow" default="1">
		<!--- Number of records to iMaxRowlay on a page --->
		<cfParam name="iMaxRow" default="20">

		<!--- Set parameters for displaying data --->
		<cfset iLastRow=iStartRow + iMaxRow>
		<cfif iLastRow GREATER THAN sQueryName.RecordCount>
			<cfset iLastRow=999>
		<cfelse>
			<cfset iLastRow=iMaxRow>
		</cfif>		