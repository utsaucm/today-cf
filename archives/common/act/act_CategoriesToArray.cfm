<cfinclude template="../sql/sql_SelectCategoriesIDOrdered.cfm">
<cfset aCategories= ArrayNew(2)>

<!--- Next, let's populate the arrays with the actual data from the database. --->
<cfOutPut query="SelectCategoriesIDOrdered">
   <cfset aCategories[currentrow][1] = "#CategoryID#">
   <cfset aCategories[currentrow][2] = "#CategoryName#">
   <cfset aCategories[currentrow][3] = "#CategoryDescription#">
</cfoutput>
<!--- 
<cfOutPut>
<cfloop from="1" to="#arraylen(aCategories)#" index="i">
#aCategories[i][1]#. 
#aCategories[i][2]# - 
#aCategories[i][3]#
</cfloop>
</cfoutput> --->
