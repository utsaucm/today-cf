<cfScript>
//	Write sorting functionality into the tables.
// Tables must be modified as in the offices listings (views)

// Interacts with associated query pages
function WriteTableSortLink(sLabel,sField){
//request.sTextURL = cgi.script_name & "?" & cgi.Query_String & "&";
	var sLink = "";
	var sQueryString = cgi.Query_String;
	var sScriptName = "https://www.utsa.edu/today/admin/indext.cfm";
	//Remove our own specifications from the query string for sorting the page.
	If (isdefined("url.sAscDesc"))
		sQueryString = ReplaceNoCase(sQueryString,"&sAscDesc="&url.sAscDesc,"");
	If (isdefined("url.sqlSort"))
		sQueryString = ReplaceNoCase(sQueryString,"&sqlSort="&url.sqlSort,"");


/*	sQueryString = ReplaceNoCase(sQueryString,"&sAscDesc=asc","");
	sQueryString = ReplaceNoCase(sQueryString,"&sAscDesc=desc","");
	sQueryString = ReplaceNoCase(sQueryString,"&=asc","");
	sQueryString = ReplaceNoCase(sQueryString,"&sAscDesc=asc",""); */
	IF (isdefined("url.sAscDesc")) {
		If (url.sAscDesc contains "asc") {
			sAscDesc = "desc";
		}Else {
			sAscDesc = "asc";
		}
	}
	// Write the link
	sLink = "<a href=""" & sScriptName & "?" & sQueryString & "&sqlSort=" & sField & "&sAscDesc=" & sAscDesc & """>" & sLabel & "</a>";
	return sLink;
}
</cfscript>