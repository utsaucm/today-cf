<cfOutPUT>
		<div>
			<!--- iMaxRowlay prev link --->
			<CFIF iStartRow NOT EQUAL 1>
				<CFIF iStartRow GTE iMaxRow>
					<CFSET prev=iMaxRow>
					<CFSET prevrec=iStartRow - iMaxRow>
				<CFELSE>
					<CFSET prev=iStartRow - 1>
					<CFSET prevrec=1>
				</CFIF>
				<span><a class="pagingButton" href="http://www.utsa.edu/today/archives/index.cfm?fuseaction=#attributes.fuseaction#&iStartRow=#prevrec#<cfIF isdefined("url.sqlSort")>&sqlSort=#url.sqlSort#</cfif><cfIF isdefined("url.sAscDesc")>&sAscDesc=#url.sAscDesc#</cfif><cfIF isdefined("url.categoryid")>&categoryid=#url.categoryid#</cfif>"><-- Previous #prev#</a></span>
			</CFIF>

			<!--- iMaxRowlay next link --->
			<CFIF iLastRow LT sQueryName.RecordCount>
				<CFIF iStartRow + iMaxRow * 2 GTE sQueryName.RecordCount>
					<CFSET next=sQueryName.RecordCount - iStartRow - iMaxRow + 1>
				<CFELSE>
					<CFSET next=iMaxRow>
				</CFIF>
				<span class="h"> | </span>
				<span><a class="pagingButton" href="http://www.utsa.edu/today/archives/index.cfm?fuseaction=#attributes.fuseaction#&iStartRow=#Evaluate("iStartRow + iMaxRow")#<cfIF isdefined("url.sqlSort")>&sqlSort=#url.sqlSort#</cfif><cfIF isdefined("url.sAscDesc")>&sAscDesc=#url.sAscDesc#</cfif><cfIF isdefined("url.categoryid")>&categoryid=#url.categoryid#</cfif>">Next #next# --></a><span>
			</cfif>
		</div>
</cfoutput>