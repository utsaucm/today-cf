<cftry>
	<cfif Application.ENV EQ "TEST">
	  <!--- TEST: dialect is postgresql --->
		<cfquery name="SelectArticlesByDate" datasource="#Application.PrimaryDSN#" timeout="500">
			SELECT * FROM Articles WHERE
			dateStart >= '#sArchiveDate#'
			AND dateStart <= '#sArchiveDateFinal# 23:59:59'
			AND dateStart <= now()
			ORDER BY dateStart DESC
		</cfquery>
	<cfelse>
	  <!--- ELSE: assume MSSQL dialect --->
		<cfquery name="SelectArticlesByDate" datasource="#Application.PrimaryDSN#" timeout="500">
			SELECT     *
			FROM       Articles
			WHERE     (datestart >= '#sArchiveDate#') and (datestart <= '#sArchiveDateFinal# 23:59:59') and  Articles.DateStart <= GETDATE()
			Order by datestart desc
		</cfquery>
	</cfif>
<cfcatch type="any">
	<cfoutput><p>#errorMsg# </p></cfoutput>
    </cfcatch>
</cftry>
