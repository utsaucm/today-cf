<h2>Browse by Category</h2>
<cfinclude template="qry_SelectCategoryByID.cfm">
<cfoutput query="SelectCategoryByID">
    <h2>You are searching for articles in <span style="font-style:italic">#CategoryName#</span>.</h2>
</cfoutput>

<!--- open query --->
<cfset sAscDesc='asc'>
<cfinclude template="qry_SelectArticlesWhereCategoryIs.cfm">
<cfset sQueryName=SelectArticlesWhereCategoryIs>
<cfinclude template="dsp_viewArticles.cfm">