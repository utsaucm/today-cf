<!-- Search -->
<!---
<form method="get" action="http://www.google.com/u/UTSAEDU">
<p>
    <label for="q">Search UTSA Today</label>
    <input class="search" type="text" value="search" name="q" id="q" size="20" onfocus="clearText(this)"  />
    <input type="hidden" name="domains" value="utsa.edu" /><input type="hidden" name="domains" value="utsa.edu" />
    <input type="hidden" name="sitesearch" value="utsa.edu" />
    <input type="hidden" name="hq" value="inurl:www.utsa.edu/today" />
    <input type="image" alt="go" name="sa" value="go" src="/1/i/h/go.gif" onclick="submit();" onkeypress="submit();" width="26" height="18" />
</p>
</form>--->

<h2>Recent Stories</h2>
<!---
Listing of fields:

ArticleID
DateAdded
DateStart
DateExpire
DateEmphasis
DateLastModified
Title
Teaser
Content
URL
CategoryID
HistoricalRating
ArticleTypeID
MarketAudienceID
CustodianLastEditID
CustodianOwnerID
CustodianLastEditIP
--->
<cfif Application.ENV EQ "TEST">
  <!--- TEST: dialect is postgresql --->
  <cfquery name="SelectArticles" datasource="#Application.PrimaryDSN#">
    SELECT * FROM Articles WHERE
    dateStart < now()
    ORDER BY dateStart DESC LIMIT 10
  </cfquery>
<cfelse>
  <!--- ELSE: assume MSSQL dialect --->
  <cfquery name="SelectArticles" datasource="#Application.PrimaryDSN#">
    SELECT TOP 10 * from Articles
    WHERE Articles.DateStart <= GETDATE()
    ORDER BY datestart desc
  </cfquery>
</cfif>
<table>
<tr>
	<th>Title</th>
	<th>Date</th>
</tr>
<cfoutput query="SelectArticles">
<tr>
  <td>#dateformat(datestart, "mmm d, yyyy")#</td>
	<td><a href="#SelectArticles.URL[currentrow]#">#title#</a></td>
</tr>
</cfoutput>
</table>
