<cfif isdefined("url.month") and isdefined("url.year")>
	<cfset myMonth = trim(REReplace(url.month, "[^0-9]", "", "ALL"))>
	<cfset myYear = trim(ReReplace(url.year, "[^0-9]", "", "ALL"))>
	<!--- <cfset sArchiveDate = url.month & "/" & 1 & "/" & url.year>
	<cfset sArchiveDateFinal=url.month & "/" & daysinmonth(sArchiveDate) & "/" & url.year> --->
	<cfset sArchiveDate = myMonth & "/" & 1 & "/" & myYear>
	<cfset sArchiveDateFinal= myMonth & "/" & daysinmonth(sArchiveDate) & "/" & myYear>
	<!-- Account for different sized months --->

	<cfinclude template="qry_SelectArticlesByDate.cfm">
	<cfoutput><h2>#sArchiveDate# - #sArchiveDateFinal#</h2></cfoutput>
	<table cellpadding="2">
		<cfoutput query="SelectArticlesByDate">
            <tr>
                <td>#dateformat(datestart,"mm/dd/yy")#</td>
                <td><a href="#SelectArticlesByDate.URL[currentrow]#">#title#</a></td>
            </tr>
        </cfoutput>
	</table>


<cfelseif  isdefined("url.year")>
	<cfset myYear = trim(ReReplace(url.year, "[^0-9]", "", "ALL"))>
	<!--- Display Months --->
	<!--- <cfif year(now()) is url.year> --->
	<cfif year(now()) is myYear>
		<cfset sCurrentMonth = month(now())+1>
	<cfelse>
		<cfset sCurrentMonth = 13>
	</cfif>
	<cfset sStartMonth = 1>
	<cfset iCounter = 1>
	<ul>
	<cfloop condition="iCounter less than 13">
		<cfif (sCurrentMonth-iCounter) is 0>
		 <cfexit>
		 <cfelse><cfoutput>
			<li><a href="/today/archives/index.cfm?fuseaction=date&year=#myYear#&month=#sCurrentMonth-iCounter#"> #monthasstring(sCurrentMonth-iCounter)#</a></li></cfoutput>
		</cfif>
		<cfset iCounter = iCounter + 1>
	</cfloop>
	</ul>


<cfelseif isdefined("url.year") is false>
	<cfset sCurrentYear = year(now())>
	<!--- Change this to include more years from the DB --->
	<cfset sStartYear = 2005>
	<cfset iCounter = 0>
	<ul class="archive-date">
     <cfset myYear = year(now())>
	<cfloop from="#sStartYear#" to="#sCurrentYear#" index="idx" step="1">
		<cfoutput>
		<!---	<li><a id="archive-#sCurrentYear-iCounter#" href="index.cfm?fuseaction=date&year=#sCurrentYear-iCounter#">#sCurrentYear-iCounter#</a></li>--->


            <li><a id="archive-#sCurrentYear-iCounter#">#sCurrentYear-iCounter#</a>
            <ul id="archive-#sCurrentYear-iCounter#-month" class="archive-month">

				<cfset sCurrentMonth = month(now())+1>
                <cfset sStartMonth = 1>
				<cfset jCounter = 1>
                <!--- loop through the months --->
                <cfif year(now()) is myYear>
					<cfset sCurrentMonth = month(now())+1>
                <cfelse>
                    <cfset sCurrentMonth = 13>
                </cfif>
                <cfloop condition="jCounter less than 13">
                    <cfif (sCurrentMonth-jCounter) is 0>
                    	<cfbreak>
                    <cfelse>
						<cfoutput>
                        <li><a href="/today/archives/index.cfm?fuseaction=date&year=#myYear#&month=#sCurrentMonth-jCounter#"> #monthasstring(sCurrentMonth-jCounter)#</a></li></cfoutput>
                    <cfset jCounter = jCounter + 1>

                    </cfif>

                </cfloop>

            </ul></li>

		</cfoutput>
		<cfset iCounter = iCounter + 1>
          <cfset myYear = myYear - 1>
	</cfloop>

	<!--- Add static links to pages not included in the archive --->
	<!--- <li><a href="http://www.utsa.edu/today/archive/2005.cfm">2005</a></li> --->
	  <li><a href="http://www.utsa.edu/today/archives/2004.cfm">2004</a></li>
	  <li><a href="http://www.utsa.edu/today/archives/2003.cfm">2003</a></li>
	  <li><a href="http://www.utsa.edu/today/archives/2002/index.cfm">2002</a></li>
	  <li><a href="http://www.utsa.edu/today/archives/2001/index.cfm">2001</a></li>
	</ul>

</cfif>


	<!---
			<li><a href="/today/archive/index.cfm?#cfusion_encrypt(URLEncodedFormat("fuseaction=date&year=#sCurrentYear-iCounter#"), theKey)#">#sCurrentYear-iCounter#</a></li>
			--->
