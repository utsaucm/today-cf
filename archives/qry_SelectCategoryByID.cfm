<cftry>
	<cfif Application.ENV EQ "TEST">
	  <!--- TEST: dialect is postgresql --->
		<cfquery name="SelectCategoryByID" datasource="#Application.PrimaryDSN#" timeout="500">
			Select RSScategories.CategoryID, RSSCategories.CategoryName, RSSCategories.CategoryDescription, RSSCategories.CategoryStatus
			from RSSCategories
			<!---WHERE RSSCategories.CategoryID=#url.CategoryID# --->
			 WHERE RSSCategories.CategoryID = <cfqueryparam value="#url.CategoryID#" cfsqltype="cf_sql_integer" maxlength="15">
		</cfquery>
	<cfelse>
	  <!--- ELSE: assume MSSQL dialect --->
		<cfquery name="SelectCategoryByID" datasource="#Application.PrimaryDSN#" timeout="500">
			Select RSScategories.CategoryID, RSSCategories.CategoryName, RSSCategories.CategoryDescription, RSSCategories.CategoryStatus
			from RSSCategories
			<!---WHERE RSSCategories.CategoryID=#url.CategoryID# --->
			 WHERE RSSCategories.CategoryID = <cfqueryparam value="#url.CategoryID#" cfsqltype="cf_sql_integer" maxlength="15">
		</cfquery>
	</cfif>
<cfcatch type="any">
	<cfoutput><p>#errorMsg# </p></cfoutput>
    </cfcatch>
</cftry>
