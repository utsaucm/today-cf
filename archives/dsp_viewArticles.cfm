<cfinclude template="/today/archives/common/act/act_SetDatabasePaging.cfm">
<cfoutput>
	<div>#sQueryName.RecordCount# total records.</div>
</cfoutput>
<br />
<cfinclude template="/today/archives/common/dsp/dsp_DatabasePagingButtons.cfm">
<cfinclude template="/today/archives/common/act/act_WriteTableSortLinkFunction.cfm">
<table class="standard table table-responsive"  border="0">
	<tr>
		<th><cfoutput>#WriteTableSortLink("Title","Title")#</cfoutput></th>
		<th><cfoutput>#WriteTableSortLink("Date","DateStart")#</cfoutput></th>
	<cfoutput QUERY="sQueryName" StartRow="#iStartRow#" maxrows="#iMaxRow#">
	<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
		<td>#DateFormat(DateStart,"mmm d, yyyy")#</td>
		<td><a href="#sQueryName.URL[currentrow]#">#Title#</a></td>
	</tr>
	</cfoutput>
</table>
  <br />
        <cfinclude template="/today/archives/common/dsp/dsp_DatabasePagingButtons.cfm">
