<cfset feedUrl = url.rss>
  
<cfhttp url="http://#CGI.HTTP_HOST#/#feedURL#" method="get" />
<cfset rss = XMLParse(cfhttp.filecontent)>

<!--- Read items at channel level --->
<cfset RSSTitle = XMLSearch(rss, "/rss/channel/title")>
<cfset RootRSSElements = XMLSearch(rss,"/rss/channel")>
<!---<cfdump var="#RSSTitle#"> --->

<!--- Read RSS at item level --->
<cfset items = XMLSearch(rss, "/rss/channel/item")>
<!--- This line outputs everything in the array, very useful for figuring out how to query stuff out --->
<!--- <cfdump var="#items#">   --->
<cfset rssItems = QueryNew("title,description,link")>

<!--- loop through the array of items --->
<cfsilent>
<cfloop from="1" to="#ArrayLen(items)#" index="i">
  <cfset row = QueryAddRow(rssItems)>
  <cfset title = XMLSearch(rss, "/rss/channel/item[#i#]/title")>
  <cfif ArrayLen(title)>
    <cfset title = title[1].xmlText>
  <cfelse>
    <cfset title="">
  </cfif>
  <cfset description = XMLSearch(items[i], "/rss/channel/item[#i#]/description")>
  <cfif ArrayLen(description)>
    <cfset description = description[1].xmlText>
  <cfelse>
    <cfset description="">
  </cfif>
  <cfset link = XMLSearch(items[i], "/rss/channel/item[#i#]/link")>
  <cfif ArrayLen(link)>
    <cfset link = link[1].xmlText>
  <cfelse>
    <cfset link="">
  </cfif>
  <!--- add to query --->
  <cfset QuerySetCell(rssItems, "title", title, row)>
  <cfset QuerySetCell(rssItems, "description", description, row)>
  <cfset QuerySetCell(rssItems, "link", link, row)>
</cfloop>
</cfsilent>
<cfinclude template="/today/_inc/v1.1/cfm/today-header.cfm">

<title>UTSA Today RSS Preview | UTSA | The University of Texas at San Antonio</title>
<cfinclude template="/today/_inc/v1.1/cfm/today-header-after-title.cfm">
<!---
<cfinclude template="/inc-share/analytics/analytics.js">
--->
</head>
<body>
<cfinclude template="/_files/global-header.html" />
<cfinclude template="/today/_inc/v1.1/cfm/today-nav.cfm" />

<section id="utsatoday-top" style="margin-top: 2em;">
<div class="container"><div class="row">
  <!-- first col -->
  <div class="col-sm-12 table-responsive">

    <h1>RSS Preview - <cfoutput>#htmleditformat(RSSTITle[1].xmlText)#</cfoutput></h1>
  	<div class="col span_12_of_12" style="background-color: #eee;">
      <ul style="list-style:none;">
      <cfoutput query="rssItems">
          <li style="padding:2px 0 2px 0;"><a href="#htmleditformat(link)#">#htmleditformat(title)#</a> <!---#htmleditformat(description)#---></li>
      </cfoutput>
      </ul>
    </div>
		<p>&nbsp;</p>
		<p><a href="index.cfm">&laquo; Back to UTSA Today RSS home</a> </p>
  </div>
</div></div>
</section>



<cfinclude template="/today/_inc/v1.1/generated/today-footer.html" />
<cfinclude template="/_files/global-footer.html" />
<!-- activate typekit -->
<script src="/today/js/typekit.js"></script>
<!--GOBAL UTSA SCRIPT - DO NOT REMOVE-->
<!-- this includes JQuery, Bootstrap.js, tether.js, slick.js -->
<script type="text/javascript" src="/_files/js/global.min.js"></script>
<!-- LOCAL PROJECT BOWER INCLUDES -->
</body>
</html>
