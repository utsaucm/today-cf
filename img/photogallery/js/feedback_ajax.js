function getHTTPObject()
{
	var xhr = false;
	if(window.XMLHttpRequest)
	{
		xhr = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		try{
			xhr = new ActiveXObject("Msxml2.XMLHTTP");			
		}
		catch(e)
		{
			try{
				xhr = new ActiveXObject("Microsoft.XMLHTTP");				
			}
			catch(e)
			{
				xhr = false;
			}			
		}		
	}
	return xhr;
} // end of getHTTPObject()

function prepareForm()
{
	//object detection
	if(!document.getElementById || !document.getElementsByTagName)
	{
		return;
	}
	if(!document.getElementById("formWriteBack"))
	{
		return;		
	}
	
	document.getElementById("formWriteBack").onsubmit = function()
	{
		var data = "";
		data = "feedbackText=" + document.getElementById("sb_message").value
		return !sendData(data);
	}
}

function sendData(data){
	var request = getHTTPObject(); 
	//alert("Request is: " + request)
	if(request)
	{
		request.onreadystatechange = function()
		{
			parseResponse(request);
		};		
		request.open("POST", "/sombrilla/includes/process_writeback.cfm", true);
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		request.send(data);
		return true;
	}
	else
	{
		return false;
	}
}

function parseResponse(request)
{
	if(request.readyState == 4)
	{
		if(request.status == 200 || request.status == 304)
		{
			
			
			//feedback = document.getElementById("sb_message");
			test = document.getElementById("test");
			feedback = "Use this form to share your thoughts.";
			test.innerHTML = "<p> " + feedback + " </p><p style='color:#f47321; font-style:italic;'>Your feedback has been submitted, thank you!</p>";
			//setTimeout("feedback.value = ''", 5000);				
			
		}
		else
		{
			//feedback.value = "Your comments could not be submitted at this time. Try again later.";
		}
		prepareForm();
	}
}

//window.onload = prepareForm;
addLoadEvent(prepareForm);

