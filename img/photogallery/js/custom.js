// JavaScript Document
<!--
$(document).ready(function() {
	//setup, grab all our tab objects (all elements with class paneltabs)
	//and grab all our panels (all elements with class panel)
	//$(".SombrillaMast").pngFix();
	//$(document).pngFix(); 
	//@example $(function(){
	$('div#title').pngFix();
	$('div#topBarContainer').pngFix();
	
	$tabs = $(".tabs");
	$panels = $(".panel");
	var tabs = $(".tab");
	//var hoverTimeout = 300;
	var hoverTimeout = 150;
	var slideDelay = 6000;
	
	$(".tab").eq(0).toggleClass("selected");
	var cycleContainer = $(".panels").cycle({
	fx: 'fade',
	timeout: slideDelay,
	before: onAfter,
	pager: '.panelLink'
	});
	
	function onAfter(e) {
	//console.log(this);
	//alert($(this).position);
	var panelPos = $(this).attr("index");
	//alert(panelPos);
	$(".tab").removeClass("selected");
	$(".tab").find(".article_descript").css("color", "#cccccc");
	$(".tab").find(".jsOff_articleLink").css("color", "#ffffff");
	//$(".tab").find(".feature_number").css("color", "#aaaaaa");
	
	$(".tab").eq(panelPos).toggleClass("selected");
	$(".tab").eq(panelPos).find(".article_descript").css("color", "#ffffff");
	$(".tab").eq(panelPos).find(".jsOff_articleLink").css("color", "#f47321");
	//$(".tab").eq(panelPos).find(".feature_number").css("color", "#222222");
	}
	
	//hover magic goes here
	//our timer variable for setInterval
	var hoverIntent = null;
	//get all the anchor elements in our tabs
	$tabs.find(".tab").hover(function () {
	//get our currently selected hover
	var divel = this;
	var el = $(this).find(".panelLink").get(0);
	//if the user stays over this hover for 500ms, change the panel
	//to the appropriate panel image by hiding every panel but the
	//one selected (filter(el.hash))
	hoverIntent = setTimeout(function () {
	//cycleContainer.cycle('stop');
	$tabs.find(".tab").removeClass("selected").filter(divel.hash).addClass("selected");
	$tabs.find(".tab").find(".article_descript").css("color", "#cccccc");
	$tabs.find(".tab").find(".jsOff_articleLink").css("color", "#ffffff");
	//$tabs.find(".tab").find(".feature_number").css("color", "#aaaaaa");
	
	$(divel).toggleClass("selected");
	$(divel).find(".article_descript").css("color", "#ffffff");
	$(divel).find(".jsOff_articleLink").css("color", "#f47321");
	//$(divel).find(".feature_number").css("color", "#222222");
	
	var desiredSlide = parseInt($(divel).attr("index"));
	cycleContainer.cycle('pause');
	cycleContainer.cycle(desiredSlide);
	}, hoverTimeout);
	}, function () {
	//on hover out, stop counting to 500ms
	clearTimeout(hoverIntent);
	//restart our rotation
	$(".panels").cycle('resume');
	}).click(function () {
	return false;
	});
	
	//click magic goes here
	//$tabs.find(".tab").click(function () {
	
	// Added by Ruben on 9-26-2009
	$tabs.find(".tab").click(function (event) {
	
	//look for the embedded articleLink anchor tag
	//on click, set the window to the new url
	
	
	//var urlToFollow = $(this).find(".articleLink").attr("href");
	
	/* Added by Ruben on 9-26-2009 */
	var urlToFollow = $(this).find(".jsOff_articleLink").attr("href");
	
	window.location = urlToFollow;
	event.preventDefault();
	});

	// ARCHIVES 
	$("#archive-2009-month").hide();
	$("#archive-2008-month").hide();
	$("#archive-2007-month").hide();
	$("#archive-2006-month").hide();
	$("#archive-2005-month").hide();
	$("#archive-2004-month").hide();
	$("#archive-2003-month").hide();
	$("#archive-2002-month").hide();
	$("#archive-2001-month").hide();
	

	$("#archive-2009").click(
		function () { $(".archive-month").hide(); $("#archive-2009-month").show('blind', 1000); }
	);
	
	$("#archive-2008").click(
		function () { $(".archive-month").hide(); $("#archive-2008-month").show('blind', 1000); }
	);
	
	$("#archive-2007").click(
		function () { $(".archive-month").hide(); $("#archive-2007-month").show('blind', 1000); }
	);
	
	$("#archive-2006").click(
		function () { $(".archive-month").hide(); $("#archive-2006-month").show('blind', 1000); }
	);
	
	$("#archive-2005").click(
		function () { $(".archive-month").hide(); $("#archive-2005-month").show('blind', 1000); }
	);
	
	$("#archive-2004").click(
		function () { $(".archive-month").hide(); $("#archive-2004-month").show('blind', 1000); }
	);
	
	$("#archive-2003").click(
		function () { $(".archive-month").hide(); $("#archive-2003-month").show('blind', 1000); }
	);
	
	$("#archive-2002").click(
		function () { $(".archive-month").hide(); $("#archive-2002-month").show('blind', 1000); }
	);
	
	$("#archive-2001").click(
		function () { $(".archive-month").hide(); $("#archive-2001-month").show('blind', 1000); }
	);
		
	// added by Ruben on 9-23-09
	// UTSA poll
	
	$("#results").hide();
	
	$("#pollResultsLink").click(
		function() { $("#pollResults").toggle(1000); return false;
	});
	
	// event handler for the "Vote" button
	$("#voteSubmit2").click(function(){ 
		
		/* store the value of the Answer ID */
		var AnswerIDVal;
		AnswerIDVal = $("input:checked").val();
		
		/* store the value of the Question ID */
		var QIDVal 	= $("#QID").val();
		
		// form validation
		if(typeof AnswerIDVal == "undefined"){
				$("#results").show();
				$("#results").html("<p style='color:#900'>Sorry, you didn't select anything. Please select an answer before hitting the 'Vote' button.</p>");
		}
		
		else{
			/* ajax call to the UTSA Poll Results .cfm  template */
			$.ajax({
					type: "POST",
					data: {AnswerID: AnswerIDVal, QuestionID: QIDVal},
					dataType: "html",
					url: "http://www.utsa.edu/today/Poll/results.cfm",
					success: callback,
					error: err
				});			
			/* ajax call to the UTSA Poll publicresults.cfm template */
				$.ajax({
					type: "POST",
					data: {AnswerID: AnswerIDVal, QuestionID: QIDVal},
					dataType: "html",
					url: "http://www.utsa.edu/today/Poll/resultspublic.cfm",
					success: callback2,
					error: err
				});					
		}	
		/* prevent a full page reload */
		return false;	
	});	
});
	
/* Added by Ruben on 10-30-2009 */	
function callback(data, status){	
	var myStr = data;
	myStr = cleanString(myStr);
	$("#results").html(myStr);
	$("#results").show(1000);
}

function callback2(data, status){	
	var myStr = data;
	var index = myStr.search("<table>");	
	var index2;		
	myStr = myStr.substr(index - 1);	
	index2 = myStr.search("</table>");	
	myStr = myStr.substr(0, index2 + 8);	
	$("#pollDisplay").hide();
	$("#pollDisplay").html(myStr);
	$("#pollDisplay").show(1500);	
}

function cleanString(mystring){
	var index = mystring.search('<div id="contentWrapper">');	
	var index2;	
	mystring = mystring.substr(index - 1);	
	index2 = mystring.search('<!-- this clears float for col left and col right -->');	
	mystring = mystring.substr(0, index2 - 1);	
	index = mystring.indexOf('>');
	mystring = mystring.substr(index + 1); 
	return mystring;	
}
function err(xhr, reason, ex)
{
	$("#results").text(reason + " could not submit your vote, try again at a later time.");	
}
//-->
