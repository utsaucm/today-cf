<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<title>Adalberto Guzman Portfolio</title>
<link rel="stylesheet" type="text/css" href="includes/normalize.css" />
<link rel="stylesheet" type="text/css" href="includes/main.css" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300' rel='stylesheet' type='text/css' />
</head>

<body>

<!--start section 1-->
<div class="dark_out" id="section1" style="padding-bottom: 0px;">
    <!--start dark int-->
    <div class="dark_int">
        
        <!--start menu-->
        <div id="menu_cont">
            <ul class="nav">
                <li class="link_pad"><a href="#section2">About</a></li>
                <li class="link_pad"><a href="#section3">Experience</a></li>
                <li><a href="#section1"><img src="images/logo.jpg" width="158" height="139" alt="" title="" /></a></li>
                <li class="link_pad"><a href="#section4">Portfolio</a></li>
                <li class="link_pad"><a href="#section5" id="afterMail">Contact</a></li>
            </ul>
        </div>
        <!--end menu-->
        <!--Opening-->
        <h2 class="opening white">Hi There,</h2>
        <!--Cont Intro-->
        <div class="im_cont">
            <img src="images/intro_left_line.jpg" width="373" height="11" alt="" title="" />
            <span class="white">I am</span>
            <img src="images/intro_right_line.jpg" width="373" height="11" alt="" title="" />
        </div>
        <!--fix-->
        <div class="clearfix"></div>
        
    </div>
    <!--end dark int-->
</div>
<!--end section 1-->

<!--start section 1 cont-->
<div class="white_out">

    <div class="white_int">
        
        <!--Cont Intro-->
        <h2 class="opening_name grey">Beto Guzman</h2>
        <h2 class="opening_pro grey">I am a web developer &amp; designer</h2>
        <!--Slideshow-->
        <img src="images/banner.jpg" width="970" height="485" alt="" title="" />
        <!--fix-->
        <div class="clearfix"></div>  
 
    </div>

</div>
<!--end section 1 cont-->

<!--start section 2-->
<div class="dark_out" id="section2">

    <div class="dark_int">
        <!--about me-->
        <?php include('includes/menu4dark.html'); ?>
        <div class="about_me">
            <h2 class="heading white">About me<!--<span class="decor_line"><img src="images/grey_line.jpg" width="715" height="14" alt="" title="" /><img src="images/grey_arrow.jpg" width="12" height="14" alt="" title="" />--></span></h2>
            <p class="white">Hi, my name is Adalberto Guzman I am a Web developer and Designer located in San Antonio, Texas. I Graduated from the University of Texas at Brownsville with a Bachelor in Applied Technology.<br /><br /> I developed my passion for web development since I took a class of web design in 2003. After that I got more involved by reading any book I could get my hands on regarding web development and design and spent countless hours online learning on the subject. As of now I am expanding my knowledge even further by joining FreeCodeCamp to further my knowledge in Javascript and the MEAN stack. I am a very dependable and self-driven individual and would be honored to be given the opportunity to be part of your team.</p>
        </div>
        <!--skills-->
        <div class="skills">
            <h2 class="heading white">Skills<!--<span class="decor_line"><img src="images/grey_line.jpg" width="313" height="14"  alt="" title="" /><img src="images/grey_arrow.jpg" width="12" height="14" alt="" title="" /></span>--></h2>
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>Bilingual</td>
                    <td>Responsive Design</td>
                </tr>
                <tr>
                    <td>HTML</td>
                    <td>WordPress</td>
                </tr>
                <tr>
                    <td>XHTML &amp; HTML5</td>
                    <td>Sitefinity CMS</td>
                </tr>
                <tr>
                    <td>CSS</td>
                    <td>Adobe Acrobat</td>
                </tr>
                <tr>
                    <td>JavaScript</td>
                    <td>Adobe Photoshop</td>
                </tr>
                <tr>
                    <td>jQuery</td>
                    <td>Adobe Fireworks</td>
                </tr>
                <tr>
                    <td>Adobe Flash</td>
                    <td>PHP</td>
                </tr>
                <tr>
                    <td>Adobe Dreamweaver</td>
                    <td>MySQL</td>
                </tr>
                <tr>
                    <td>Microsoft Office</td>
                    <td>Mac OS X</td>
                </tr>
                <tr>
                    <td>SEO</td>
                    <td>Ubuntu</td>
                </tr>
                <tr>
                    <td>Windows</td>
                </tr>
            </table>
        </div>
        <!--fix-->
        <div class="clearfix"></div>    
    </div>

</div>
<!--end section 2-->


<!--start section 3-->
<div class="white_out" id="section3">

    <div class="white_int">
        <?php include('includes/menu4white.html'); ?>
        <div class="experience">
            <h2 class="heading black">Experience<!--<span class="decor_line"><img src="images/white_line.jpg" width="683" height="14" alt="" title="" /><img src="images/white_arrow.jpg" width="10" height="14" alt="" title="" /></span>--></h2>
             <p><span class="place">The University of Texas at San Antonio</span><span class="year">2014 - Present</span><br /><span class="position">Multimedia Designer II</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Redesigned the main page of Giving UTSA</li>
                <li><span class="blue"><strong>+</strong></span> Worked with the Sombrilla Online website on modifying existing pages and adding content.</li>
                <li><span class="blue"><strong>+</strong></span> Worked with Blackbaud Netcommunity CMS to redesign and create new pages and templates.</li>
                <li><span class="blue"><strong>+</strong></span> I was in charge of building from the ground up the main page, landing pages and departmental pages of the main UTSA website, the main objective was to make a responsive experience to implement it into a new CMS.</li>
                <li><span class="blue"><strong>+</strong></span> Created a pattern library for the new design.</li>
                <li><span class="blue"><strong>+</strong></span> Modified existing pages to be responsive whenever was needed..</li>
                <li><span class="blue"><strong>+</strong></span> Designed and coded HTML emails for UTSA campaigns.</li>
                <li><span class="blue"><strong>+</strong></span> Created cross-browser compatible websites.</li>
                <li><span class="blue"><strong>+</strong></span> In charge of debugging existing pages whenever needed.</li>
            </ul>
            
             <p><span class="place">Gray Digital Group</span><span class="year">2013 - 2014</span><br /><span class="position">Web Developer</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Modified existing pages for clients such as: Baptist Health System, Valley Baptist, CHRISTUS Hospital, St. Davids and many others.</li>
                <li><span class="blue"><strong>+</strong></span> Created custom builds in Sitefinity CMS and also utilizing XHTML, CSS, JavaScript and jQuery.</li>
                <li><span class="blue"><strong>+</strong></span> Designed page layouts and banners when needed.</li>
                <li><span class="blue"><strong>+</strong></span> Created a JavaScript calendar so it can fit the modifications of any client without spending too much time on it.</li>
                <li><span class="blue"><strong>+</strong></span> Worked closely with team leader to achieve maximum results.</li>
                <li><span class="blue"><strong>+</strong></span> Participated in website testing.</li>
                <li><span class="blue"><strong>+</strong></span> Created e-blasts for different clients.</li>
            </ul>

            <p><span class="place">Kforce - USAA</span><span class="year">2012 - 2013</span><br /><span class="position">UI Designer (Contractor)</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Using Adobe Photoshop to create marketing design for both the public and private sides of USAA.com, USAA mobile and ad creation for the USAA mobile app.</li>
                <li><span class="blue"><strong>+</strong></span> Adhere to standards compliance code when constructing valid HTML/CSS page models.</li>
                <li><span class="blue"><strong>+</strong></span> Followed web accessibility standards.</li>
                <li><span class="blue"><strong>+</strong></span> One of the contact point to help debug issues for cross-browser compatibility.</li>
                <li><span class="blue"><strong>+</strong></span> Schedule and attend meetings with co-workers and businesses to keep projects on track.</li>
                <li><span class="blue"><strong>+</strong></span> Collaborate with other teams when necessary.</li>
                <li><span class="blue"><strong>+</strong></span> Created JavaScript calculator for Auto Savings following USAA standards.</li>
                <li><span class="blue"><strong>+</strong></span> Created mobile flows for the iPhone, iPad and Android application.</li>
                <li><span class="blue"><strong>+</strong></span> Work closely with the back-end developer to take care of any problems before showing final product</li>
            </ul>
            <p><span class="place">Rio Bravo Pictures</span><span class="year">(7 month contract) 2012 - 2012</span><br /><span class="position">Contract Web Developer and Designer (Contractor)</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Created website for Lone Star National Bank utilizing HTML, CSS, JavaScript, jQuery and ASP.net</li>
                <li><span class="blue"><strong>+</strong></span> Created and modified websites using PHP, MySQL, WordPress and Joomla!.</li>
                <li><span class="blue"><strong>+</strong></span> Involved the client in all steps of web development to achieve maximum end results.</li>
            </ul>
            <p><span class="place">Celebrity Group/PXE Marketing</span><span class="year">2010 - 2012</span><br /><span class="position">Lead Web Developer and Designer</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Designed mock-ups and prototypes for websites and applications for the company’s businesses utilizing Adobe Photoshop and Fireworks.</li>
                <li><span class="blue"><strong>+</strong></span> Converted Photoshop layouts to fully functional and cross-browser compatible websites utilizing HTML, CSS, JavaScript, jQuery, PHP, MySQL, WordPress and Drupal.</li>
                <li><span class="blue"><strong>+</strong></span> Created a subscription application using PHP and MySQL.</li>
                <li><span class="blue"><strong>+</strong></span> Daily maintenance of host server, web pages and emails.</li>
                <li><span class="blue"><strong>+</strong></span> Designed multiple websites for the company’s magazines.</li>
                <li><span class="blue"><strong>+</strong></span> Created a schedulling web application to maximize availability to the sales person offering ad space in magazine utilizing JavaScript, PHP and MySQL.</li>
                <li><span class="blue"><strong>+</strong></span> Supervised one graphic designer and two web developers.</li>
            </ul>
            <p><span class="place">Sabal Technologies</span><span class="year">2007 - 2011</span><br /><span class="position">Contract Web Developer and Designer</span></p>
            <ul>
                <li><span class="blue"><strong>+</strong></span> Developed web sites and online applications for several clients utilizing HTML, CSS, JavaScript, PHP and MySQL.</li>
                <li><span class="blue"><strong>+</strong></span> Developed visual design and user interface websites utilizing Adobe Photoshop and Adobe Fireworks.</li>
                <li><span class="blue"><strong>+</strong></span> Communicated with customer at all levels of the web design process.</li>
            </ul>
        </div>
    <!--fix-->
    <div class="clearfix"></div>   
    </div>

</div>
<!--end section 3-->


<!--start section 4-->
<div class="dark_out" id="section4">
        <div class="dark_int">
        <?php include('includes/menu4dark.html'); ?>
            <div class="portfolio">
                <h2 class="heading white">Portfolio<!--<span class="decor_line"><img src="images/grey_line.jpg" width="731" height="14" alt="" title="" /><img src="images/grey_arrow.jpg" width="12" height="14" alt="" title="" /></span>--></h2>
                <div class="port_img"><img src="images/websites/comingsoon.jpg" width="300" height="200" alt="" title="" /><p class="white">Fun Creations<br />Web, Freelance, 2015</p></div>
                <div class="port_img"><img src="images/websites/comingsoon.jpg" width="300" height="200" alt="" title="" /><p class="white">Ocejos Kickboxing Gym<br />Web, Freelance, 2015</p></div>
                <div class="port_img"><a class="preview" rel="images/pop/canticle.jpg" title=""><img alt="gallery thumbnail" src="images/websites/canticle.jpg"></a><p class="white">Canticle Hot Glass Shop (Design Phase)<br>Web, Freelance, 2014</p></div>


                <div class="port_img"><a href="#" rel="images/pop/utsa-index.jpg" class="preview" target="_blank"><img src="images/websites/utsa-main.jpg" alt="gallery thumbnail" /></a><p class="white">UTSA Redesign (In Process)<br />Web, UTSA, 2015</p></div>
                <div class="port_img"><a href="#" rel="images/pop/utsa-landing.jpg" class="preview" target="_blank"><img src="images/websites/utsa-landing.jpg" alt="gallery thumbnail" /></a><p class="white">UTSA Redesign Landing (In Process)<br />Web, UTSA, 2015</p></div>
                <div class="port_img"><a href="http://www.utsa.edu/Sombrilla/summer2015/" rel="images/pop/utsa-sombrilla.jpg" class="preview" target="_blank"><img src="images/websites/utsa-sombrilla.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.utsa.edu/Sombrilla/summer2015/" target="_blank">Sombrilla Magazine</a><br />Web, UTSA, 2015</p></div>
                <div class="port_img"><a href="http://www.utsa.edu/2020blueprint/" rel="images/pop/utsa-blueprint.jpg" class="preview" target="_blank"><img src="images/websites/utsa-blueprint.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.utsa.edu/2020blueprint/" target="_blank">Blueprint 2020</a><br />Web, UTSA, 2015</p></div>
                <div class="port_img"><a href="http://giving.utsa.edu/" rel="images/pop/utsa-giving.jpg" class="preview" target="_blank"><img src="images/websites/utsa-giving.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://giving.utsa.edu/" target="_blank">Giving UTSA</a><br />Web, UTSA, 2014</p></div>


                
                <div class="port_img"><a href="http://www.kingstheatre.com/" rel="images/pop/kings.jpg" class="preview" target="_blank"><img src="images/websites/kings.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.kingstheatre.com/" target="_blank">Kings Theatre</a> (In Development)<br />Web, Gray Digital Group, 2014</p></div>
                <div class="port_img"><a href="http://www.graydigitalgroup.com/home" rel="images/pop/gray.jpg" class="preview" target="_blank"><img src="images/websites/gray.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.graydigitalgroup.com/home" target="_blank">Gray Digital Group Main</a> (In Development)<br />Web, Rio Bravo Pictures, 2012</p></div>
                <div class="port_img"><a href="http://christushospital.org/minorcare" rel="images/pop/christusMinor.jpg" class="preview" target="_blank"><img src="images/websites/christusMinor.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://christushospital.org/minorcare" target="_blank">Christus Minor Care</a><br />Web, Gray Digital Group, 2014</p></div>
                <div class="port_img"><a rel="images/pop/abrazo.jpg" class="preview" target="_blank"><img src="images/websites/abrazo.jpg" alt="gallery thumbnail" /></a><p class="white">Abrazo Health Surgical Weight Loss<br />Web, Gray Digital Group, 2014</p></div>
                <div class="port_img"><a href="http://christushospital.org/emergency" rel="images/pop/christusEmergency.jpg" class="preview" target="_blank"><img src="images/websites/christusEmergency.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://christushospital.org/emergency" target="_blank">Christus Emergency Services</a><br />Web, Gray Digital Group, 2014</p></div>
                <div class="port_img"><a href="http://www.valleybaptist.net/medical-services/women-and-children/services/rgv-baby-app" rel="images/pop/valleyBaptist.jpg" class="preview" target="_blank"><img src="images/websites/valleyBaptist.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.valleybaptist.net/medical-services/women-and-children/services/rgv-baby-app" target="_blank">Valley Baptist RGV Baby</a><br />Web, Gray Digital Group, 2014</p></div>
                <div class="port_img"><a href="http://www.lonestarnationalbank.com" rel="images/pop/lonestar.jpg" class="preview" target="_blank"><img src="images/websites/lonestar.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.lonestarnationalbank.com" target="_blank">Lone Star National Bank</a><br />Web, Rio Bravo Pictures, 2012</p></div>
                <div class="port_img"><a href="http://www.dosarmadillostequila.com" rel="images/pop/dosArmadillos.jpg" class="preview" target="_blank"><img src="images/websites/dosArmadillos.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.dosarmadillostequila.com" target="_blank">Dos Armadillos Tequila</a><br />Web, Rio Bravo Pictures, 2012</p></div>
                <div class="port_img"><a rel="images/pop/dhr.jpg" class="preview"><img src="images/websites/dhr.jpg" alt="gallery thumbnail" /></a><p class="white">Doctors Hospital at Renaissance<br />Web, Rio Bravo Pictures, 2012</p></div>
                <div class="port_img"><a rel="images/pop/postalstore.jpg" class="preview"><img src="images/websites/postalstore.jpg" alt="gallery thumbnail" /></a><p class="white">The Postal Store<br />Web, Freelance, 2012</p></div>
                <div class="port_img"><a href="http://www.thejobhire.com" rel="images/pop/jobhire.jpg" class="preview" target="_blank"><img src="images/websites/jobhire.jpg" alt="gallery thumbnail" /></a><p class="white"><a href="http://www.thejobhire.com">The Job Hire</a><br />Web, Freelance, 2012</p></div>
                <div class="port_img"><a rel="images/pop/nights2.jpg" class="preview"><img src="images/websites/nights2.jpg" alt="gallery thumbnail" /></a><p class="white">Celebrity Nights Magazine V2<br />Web, Celebrity Group, 2011</p></div>
                <div class="port_img"><a rel="images/pop/celebritygroup.jpg" class="preview"><img src="images/websites/celebritygroup.jpg" alt="gallery thumbnail" /></a><p class="white">Celebrity Group<br />Web, Celebrity Group, 2011</p></div>
                <div class="port_img"><a rel="images/pop/pxemarketing.jpg" class="preview"><img src="images/websites/pxemarketing.jpg" alt="gallery thumbnail" /></a><p class="white">PXE Marketing<br />Web, Celebrity Group, 2011</p></div>
                <div class="port_img"><a rel="images/pop/celebrityevents.jpg" class="preview"><img src="images/websites/celebrityevents.jpg" alt="gallery thumbnail" /></a><p class="white">Celebrity Events Magazine<br />Web, Celebrity Group, 2011</p></div>
                <div class="port_img"><a rel="images/pop/pxecredit.jpg" class="preview"><img src="images/websites/pxecredit.jpg" alt="gallery thumbnail" /></a><p class="white">PXE Credit<br />Web, Celebrity Group, 2010</p></div>
                <div class="port_img"><a rel="images/pop/nights1.jpg" class="preview"><img src="images/websites/nights1.jpg" alt="gallery thumbnail" /></a><p class="white">Celebrity Nights Magazine V1<br />Web, Celebrity Group, 2010</p></div>
                <div class="port_img"><a rel="images/pop/pxedev.jpg" class="preview"><img src="images/websites/pxedev.jpg" alt="gallery thumbnail" /></a><p class="white">PXE Development<br />Web, Celebrity Group, 2010</p></div>
            </div>  
    <!--fix-->
    <div class="clearfix"></div>      
    </div>

</div>
<!--end section 4-->


<!--start section 5-->
<div class="dark_contact" id="section5">

    <div class="dark_contact_int">
    <?php include('includes/menu4cont.html'); ?>
        <div class="contact">   
            <h2 class="heading white">Contact<!--<span class="decor_line"><img src="images/cont_line.jpg" width="750" height="14" alt="" title="" /><img src="images/cont_arrow.jpg" width="12" height="14" alt="" title=""  /></span>--></h2>
            <p class="white">Please feel free to contact me at <span class="blue">956-639-9221</span> or use the form below and I will get back to you as soon as possible.</p>
                <!--Email Form-->
                <?php
        
                    if($_SERVER['REQUEST_METHOD'] == 'POST') { //Handle the form
                
                        //Declare Variables
                        $name = trim($_POST['name']);
                        $email = trim($_POST['email']);
                        $subject = trim($_POST['subject']);
                        $website = trim($_POST['website']);
                        $message = trim($_POST['feedback']);
                        $error = FALSE;
                    
                        //Validation
                        if($name == "Name" || $email == "Email" || $subject == "Subject" || $message == "Message"){
                            $error = TRUE;
                            echo '<span class="error">You must fill out all the fields in the form.</span><br />';
                        }
                        
                        //HoneyPot spam input field is left blank
                        if($_POST["address"] != ""){
                            $error = TRUE;
                            echo '<span class="error">Your form submission has an error</span><br />';
                        }
                    
                        //Send Mail
                        $to = 'beto_guz@yahoo.com';
                        $subject = $subject . " " . "betoguzman.com";
                        $body = "From: $name \r\n Email: $email \r\n\r\n Subject: $subject \r\n\r\n $message";
                        $headers = 'From: BetoGuzman.com';
                        
                        if(!$error){
                            echo '<span class="success">Thank you, your message has been sent.</span>';
                            mail($to, $subject, $body, $headers);
                            //send to bottom of page
                        }else{
                            echo '<span class="error">Message could not been sent<br />';
                        } //End main submitt
                    
                    }//End POST
                
                    
                ?>
                
                
                <form action="index.php" method="post" enctype="multipart/form-data">
                <div class="text_fields">
                <input type="text" class="input_text" name="name" id="name" value="Name" onclick="this.value='';" 
                onfocus="this.select()" onblur="this.value=!this.value?'Name':this.value;" />

                <input type="text" class="input_text" name="email" id="email" value="Email"  onclick="this.value='';" 
                onfocus="this.select()" onblur="this.value=!this.value?'Email':this.value;" />

                <input type="text" class="input_text" name="subject" id="subject" value="Subject"  onclick="this.value='';" 
                onfocus="this.select()" onblur="this.value=!this.value?'Subject':this.value;"/>
                
                <input type="text" class="input_text" name="website" id="website" value="Website"  onclick="this.value='';" 
                onfocus="this.select()" onblur="this.value=!this.value?'Website':this.value;"/>
                <p class="resume"><a href="beto_guzmanWordResume.docx">Download Resume</a></p>
                </div>
                <div class="message_field">
                <textarea class="message" name="feedback" id="feedback" rows="10" cols="60" onclick="this.value='';" 
                onfocus="this.select()" onblur="this.value=!this.value?'Message':this.value;">Message</textarea>
                </div>
                <div style="display:none;">
                    <input type="text" name="address" id="address" />
                </div>
                <input type="submit" class="button" value=""/>

            </form>   
        </div>
    <!--fix-->
    <div class="clearfix"></div>      
    </div>

</div>
<!--end section 5-->


<!--Javascript-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>      
<script type="text/javascript" src="jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/slide.js"></script>
<script type="text/javascript" src="js/gallery.js"></script>
<!--end Javascript-->
</body>
</html>