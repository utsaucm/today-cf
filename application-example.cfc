<!--- Application: UTSA-TODAY-INTAKEFORMS-UCM --->
<!--- VERSION: 1.0: Ruben Ortiz <ruben.ortiz@utsa.edu> 2015/06/04 --->
<!--- VERSION: 1.0.1: John David Garza <john.garza@utsa.edu> 2016/02/17
        - attempting to clean up several html escaping issues and setting up a proper testing env on haldev
		    - adding a UDF component to URL scope
      VERSION: 1.0.2: John David Garza <john.garza@utsa.edu> 2016/03
        - cleaning up base code for final improvements before CMS (supposedly)
--->
<cfcomponent output="false">
  <cfscript>
		this.name				= "UTSATODAY-UCM";
    this.env        = "PROD";
		this.customtagpaths 	=  GetDirectoryFromPath(GetCurrentTemplatePath()) & "_inc\customtags\";
		this.applicationTimeout =  createTimeSpan(0,0,5,0);
		this.loginStorage 		= "session";
		this.sessionManagement 	= true;
		this.sessionTimeout		= createTimeSpan(0,0,5,0);
		this.setClientCookies 	= true;
		this.setDomainCookies 	= false;
		this.scriptProtect 		= true;
    if (this.ENV EQ "TEST") {
      this.datasource = "utsaweb-syntaxdev";
    }
    if (this.ENV EQ "PROD") {
      this.datasource = "utsaweb-syntaxprod";
    }
    this.javaSettings = {
        loadPaths: [
            "./lib/"
        ],
        loadColdFusionClassPath: true
    };
  </cfscript>
  <cferror type="exception" template="/today/error.cfm" />
  <cfset structAppend(URL, createObject( "component", "cfc/UDF" )) />

	<!--- Application starts up --->
  <cffunction name="onApplicationStart" returntype="boolean">
    <cfset log=todayLog("The UTSA Today Web Application has started.") />
    <cflock timeout="1" SCOPE="APPLICATION" TYPE="Exclusive">
    <cfscript>
      Application.ENV = "PROD";
      Application.path = "/today";
      if (Application.ENV EQ "TEST") {
        Application.SERVER = "http://localhost:8888";
        Application.PrimaryDSN = "utsaweb-syntaxdev";
        Application.webAdminEmail = "john.garza@utsa.edu";
      }
      if (Application.ENV EQ "DEV") {
        Application.SERVER = "http://haldev.utsa.edu";
        Application.PrimaryDSN = "utsaweb-syntaxdev";
        Application.webAdminEmail = "john.garza@utsa.edu";
      }
      if (Application.ENV EQ "PROD") {
        Application.SERVER = "http://haldev.utsa.edu";
        Application.PrimaryDSN = "utsaweb-syntaxprod";
        Application.webAdminEmail = "webteam@utsa.edu";
      }
    </cfscript>
		</cflock>
    <cfreturn true>
  </cffunction>

  <!--- Application shuts down --->
  <cffunction name="onApplicationEnd" returntype="void">
    <cfargument name="appScope" required="true">
    <cfscript>
      todayLog("The UTSA Today Web Application has shut down.");
    </cfscript>
    <cfreturn true>
  </cffunction>

	<!--- Application request --->
  <cffunction name="onApplicationRequest" returntype="void">
  </cffunction>

  <cffunction name="onRequestStart" output="false">
  	<cfargument name="thePage" type="string" required="true">
  	<cfscript>
  		var myReturn = true;
  		// Block cfm pages that start with underscore
  		if (left(listLast(arguments.thePage, "/"), 1) EQ "_"){
  			myReturn = false;
  		}
  	</cfscript>

		<!--- live code --->
		<cfif StructKeyExists(URL,"force_app_restart")>
      <cfscript>onApplicationStart();</cfscript>
		</cfif>
		<cfreturn true>
  </cffunction>

  <!--- Handles 404 events --->
	<cffunction name="onMissingTemplate" returntype="void">
    <cflocation url="#APPLICATION.path#/404.cfm">
  </cffunction>

	<!--- Any web errors that are not caught --->
</cfcomponent>
