<cfcomponent output="false">

	<!--- Application name, should be unique --->
	<cfset this.name = "UTSATodayWebAdmin">
	<cfset this.applicationTimeout = createTimeSpan(0,3,0,0)>
	<cfset this.clientManagement = true>
	<cfset this.clientStorage = "cookie">
	<cfset this.loginStorage = "session">
	<cfset this.sessionManagement = true>
	<cfset this.sessionTimeout = createTimeSpan(0,8,0,0)>
	<cfset this.setClientCookies = true>
	<cfset this.setDomainCookies = false>
	<cfset this.scriptProtect = "none">
	<cfset this.secureJSON = false>
	<cfset this.secureJSONPrefix = "">
	<cfset this.welcomeFileList = "">

	<!--- define custom coldfusion mappings. Keys are mapping names, values are full paths  --->
	<cfset this.mappings = structNew()>
	<!--- define a list of custom tag paths. --->
	<cfset this.customtagpaths = "">
	<cfset structAppend(URL, createObject( "component", "/today/cfc/UDF" )) />

	<!--- Run when application starts up --->
	<cffunction name="onApplicationStart" returnType="boolean" output="false">
		<cfset log=adminLog("TodayAdmin Application has started.") />
		<cfset Application.dateInitialized = now() />
		<cfset Application.ENV = "PROD" />
		<cfset Application.SECURE_PORT = "" />
		<cfset Application.AD_SERVER = "BUSH1604.UTSARR.NET" />
		<cfset Application.USER_SUFFIX = "@utsarr.net" />
		<cfset Application.START_DN = "OU=Administrative Computing Services,DC=UTSARR,DC=NET" />
		<cfset Application.GROUP_1 = "CN=WWW_TODAY,OU=HAL,OU=WEB SERVERS,OU=Security Groups,OU=Administrative Computing Services,DC=UTSARR,DC=NET" />
		<cfset Application.GROUP_2 = "CN=www_today_admin,OU=HAL,OU=WEB SERVERS,OU=Security Groups,OU=Administrative Computing Services,DC=UTSARR,DC=NET" />
		<cfif (Application.ENV EQ "TEST") OR (Application.ENV EQ "DEV")>
			<cfset Application.ADMIN_NOTIFY = "john.garza@utsa.edu" />
			<cfset Application.PrimaryDSN="utsaweb-syntaxdev" />
	    <cfset attributes.PrimaryDSN="webemp-syntaxdev" />
		</cfif>
		<cfif (Application.ENV EQ "PROD")>
			<cfset Application.ADMIN_NOTIFY = "john.garza@utsa.edu, shashi.pinheiro@utsa.edu" />
			<cfset Application.PrimaryDSN="utsaweb-syntaxprod" />
	    <cfset attributes.PrimaryDSN="webemp-syntaxprod" />
		</cfif>
		<cfreturn true>
	</cffunction>

	<!--- Run when application stops --->
	<cffunction name="onApplicationEnd" returnType="void" output="false">
		<cfargument name="appScope" required="true">
    <cfset test=adminLog("TodayAdmin Application has started shut down.") />
    <cfreturn true />
	</cffunction>

	<cffunction name="onRequestStart" access="public" returnType="boolean" output="true">
		<cfargument name="thePage" type="string" required="true" />
		<cf_FormURL2Attributes>
		<cfparam name="submit" default="no">
		<!--- for app refresh --->
		<cfif StructKeyExists(URL,"force_app_restart")>
      <cfscript>onApplicationStart();</cfscript>
		</cfif>

		<cfif NOT isSecure()>
			<cfset secureURL = "https://#CGI.server_name##APPLICATION.SECURE_PORT##CGI.script_name#" />
			<cfset test=adminLog("connection is NOT secure: redirecting to: #secureURL#") />
			<cflocation url="#secureURL#" addtoken="no" />
			<!--- "#" --->
		</cfif>


		<cfif IsDefined("Form.logout")>
			<cflogout>
		</cfif>

		<cfset errorMessage = "Your login information is not valid. Please try again." />
		<cflogin>
			<cfif NOT IsDefined("cflogin")>
      	<cfinclude template="login.cfm">
        <cfabort>
      <cfelse>
      	<cfif cflogin.name IS "" OR cflogin.password IS "">
        	<cfoutput>
          	<h2>You must enter text in both the User Name and Password fields.</h2>
					</cfoutput>
					<cfinclude template="login.cfm">
					<cfabort>
        <cfelse>
					<cftry>
						<cfset filter="(|(&(objectCategory=user)(sAMAccountName=#cflogin.name#)(memberOf=#Application.GROUP_1#))(&(objectCategory=user)(sAMAccountName=#cflogin.name#)(memberOf=#Application.GROUP_2#)))" />
					  <cfldap
					    server="#Application.AD_SERVER#"
					    action="query"
					    name="res"
					    scope="subtree"
					    attributes="sAMAccountName,givenName,mail"
							filter="#filter#"
					    start="#Application.START_DN#"
					    username="#cflogin.name##Application.USER_SUFFIX#"
					    password="#cflogin.password#"
					  />
						<cfif #res.recordcount# EQ 0>
							<cfset test=adminLog("recordcount: #res.recordcount#") />
		          <cfinclude template="login.cfm">
						  <cfabort />
						<cfelse>
						  <cfloginuser name="#cflogin.name#" password="#cflogin.password#" roles="user" />
						  <!--- Create Session Variables --->
						  <cflock scope="session" timeout="20" type="Exclusive">
						  	<cfset Session.Username = res.givenName>
						  </cflock>
						</cfif>
						<cfcatch type="any">
							<cfset test=adminLog("cfldap try catch") />
							<cfset test=adminLog(cfcatch.message) />
		          <cfinclude template="login.cfm" />
						  <cfabort />
					  </cfcatch>
					</cftry>
					<cfset test=adminLog("cfldap cflogin success") />
	      </cfif>
			</cfif>
		</cflogin>
		<cfif GetAuthUser() NEQ "">
			<cfset test=adminLog("authenticated as #GetAuthUser()#") />
		</cfif>
		<cfreturn true>
	</cffunction>
	<!--- Run before the request is processed --->
	<!--- Runs before request as well, after onRequestStart --->
	<!---
	WARNING!!!!! THE USE OF THIS METHOD WILL BREAK FLASH REMOTING, WEB SERVICES, AND AJAX CALLS.
	DO NOT USE THIS METHOD UNLESS YOU KNOW THIS AND KNOW HOW TO WORK AROUND IT!
	EXAMPLE: http://www.coldfusionjedi.com/index.cfm?mode=entry&entry=ED9D4058-E661-02E9-E70A41706CD89724
	<cffunction name="onRequest" returnType="void">
		<cfargument name="thePage" type="string" required="true">
		<cfinclude template="#arguments.thePage#">
    <cfset request.Protocol="http://">
			<cfscript>
			todayAdminLog("testing existance onRequest");
			</cfscript>
	</cffunction>
	--->

	<!--- Runs at end of request --->
	<cffunction name="onRequestEnd" returnType="void" output="false">
		<cfargument name="thePage" type="string" required="true">
	</cffunction>

	<!--- Runs when your session starts --->
	<cffunction name="onSessionStart" returnType="void" output="false">

	</cffunction>

	<!--- Runs when session ends --->
	<cffunction name="onSessionEnd" returnType="void" output="false">
		<cfargument name="sessionScope" type="struct" required="true">
		<cfargument name="appScope" type="struct" required="false">
	</cffunction>

	<cffunction name="adminLog">
		<cfargument name="textMessage" type="String" required="yes"/>
	      <cflog file="today-admin" text="TodayAdmin - #textMessage#" />
	</cffunction>
	<cffunction name="isSecure">
		<cfset protocolSecure = false />
		<cfif CGI.server_port_secure EQ 1>
			<cfset protocolSecure = true />
		</cfif>
		<cfreturn protocolSecure />
	</cffunction>

</cfcomponent>
