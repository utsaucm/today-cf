
<cfparam name="errorMessage" default="Please enter a valid username/password combination.">
<!---These set the session variables back to their defaults--->

<!---A quick check to see if the form username&ID were invalid and prints to the screen an error message
	based on session.LoginTest but is returned after the form is processed --->
<!DOCTYPE html>
<html>
<head>
      <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
      <meta charset="utf-8">

<title>UTSA Today | UTSA | The University of Texas at San Antonio</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="/_inc/css/global-style.css" />
<link rel="stylesheet" type="text/css" href="/_inc/css/internal-style.css" />
<!--[if lt IE 9]>
<script src="/_inc/js/selectivizr-min.js"></script>
<link rel="stylesheet" type="text/css" href="/_inc/css/ie8-fixes.css" />
<![endif]-->
<!--Glyphicons-->
<link rel="stylesheet" href="/_inc/css/glyphicons.css" />

<!--Fonts-->
<script src="//use.typekit.net/uzw2huo.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<!--scripts-->
<script src="/_files/js/jquery.min.js"></script>
<script src="/_inc/js/vendor/modernizr.custom.71422.js"></script>
<script src="/_inc/js/respond.min.js"></script>
<link rel="stylesheet" type="text/css" href="/today/css/today-new.css">

<!-- Google Analytics code includes -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2438226-1', 'utsa.edu');
  ga('send', 'pageview');

</script>
<!-- end Google Analytics code includes -->
<style type="text/css">
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
.errorMessage {
	color: #990000;
}

form {
  display: block;
}

.btn {
  display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 1.428571429;
text-align: center;
white-space: nowrap;
vertical-align: middle;
cursor: pointer;
border: 1px solid transparent;
border-radius: 4px;
-webkit-user-select: none;
}


.btn:focus {
    outline: thin dotted #333;
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px
}

.btn:hover, .btn:focus {
    color: #333;
    text-decoration: none
}

.btn:active, .btn.active {
    background-image: none;
    outline: 0;
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125)
}

.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
    pointer-events: none;
    cursor: not-allowed;
    opacity: .65;
    filter: alpha(opacity=65);
    -webkit-box-shadow: none;
    box-shadow: none
}

.btn-default {
    color: #333;
    background-color: #fff;
    border-color: #ccc
}
.control-group {
  display: block;
}

.well {
padding: 19px;
margin-bottom: 20px;
background-color: #f5f5f5;
border: 1px solid #e3e3e3;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);

}

label {
  display: inline-block;
margin-bottom: 5px;
font-weight: bold;
}

.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.428571429;
  color: #555;
  vertical-align: middle;
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

#form-login {
}
</style>
</head>
<body>
<!-- start global header -->
<!--start emergency notice-->
<div id="emergencyNotice">
	<p>&nbsp;</p>
</div>
<!--end emergency notice-->

<!--start search-->
<div class="section group headerSearchContainer">
	<div class="internalContainer clearfix">
		<form id="cse-search-box" action="/search/searchresults.html">
            <input type="hidden" name="cx" value="000238266656426684962:mzli4pte7ko">
            <input type="hidden" name="cof" value="FORID:11">
            <span style="display:none;"><label for="q">Search</label></span>
            <input class="searchfield" name="q" id="q" type="text" size="15" value="Search UTSA" onfocus="if (this.value == 'Search UTSA') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search UTSA';}">
        </form>
	</div>
</div>
<!--end search-->

<!--start header-->
<div id="globalHeaderContainer">
	<div class="internalContainer clearfix section group">
		<div class="col span_3_of_12">
			<div class="col span_3_of_12">
				<a href="/index.html"><img src="/_inc/img/logo4_sm.png" alt="The University of Texas at San Antonio" class="globalSmMobileLogo"></a>
			</div>
		</div>
		<div class="col span_9_of_12">
			<nav id="globalSmNav">
					<a href="http://my.utsa.edu">
                    <span class="glyphicons glyphicons-global"></span>
                    <span class="linkMobile">myUTSA</span></a>
					<a href="/today">
                    <span class="glyphicons glyphicons-newspaper"></span>
                    <span class="linkMobile">News</span></a>
					<a href="/visit">
                    <span class="glyphicons glyphicons-map"></span>
                    <span class="linkMobile">Visit</span></a>
					<a href="/directory">
                    <span class="glyphicons glyphicons-parents"></span>
                    <span class="linkMobile">Directory</span></a>
                    <a href="#" id="headerSearchCTA">
                    <span class="glyphicons glyphicons-search"></span>
                    <span class="linkMobile">Search</span></a>
			</nav>
		</div>
	</div>
</div>
<!--end header-->

<!--start mobile menu-->
<!--#include file="/_inc/mobile-menu.html"-->
<!--end mobile menu-->
<!-- end global header -->

<div class="whiteContainer">
  <div class="internalContainer clearfix section group">
    <div class="col span_12_of_12">
	<div class="errorMessage">
		<center><cfoutput>#errorMessage#</cfoutput></center>
	</div>
	<cfoutput>
	<form action="#CGI.script_name#?#CGI.query_string#" method="POST" name="form-login" id="form-login" class="" style="width: 80%">
    <div class="control-group">
      <div class="controls">
         <label for="j_username">User Name:</label>
         <input name="j_username" id="j_username" class="form-control" type="text" size="20" placeholder="Enter UTSA Network ID to Log in.">
         <p class="help-block"></p>
      </div>
    </div>

    <div class="control-group">
       <div class="controls">
          <label for="j_password">Password:</label>
          <input name="j_password" id="j_password" class="form-control" type="password" size="20" placeholder="Enter Password to Log in.">
       </div>
    </div>
    <br/><br/>
    <div class="control-group">
      <button id="submit" type="submit" class="btn btn-default">Log In</button>
    </div>
  </form>
</cfoutput>
    </div>
  </div>


</div><!--end whiteContainer-->

<!--start internal footer-->
<cfinclude template="/today/inc/internal-footer.html">
<!-- end internal footer -->
</div>

<!--start global footer-->
<cfinclude template="/_inc/global-footer.html">
<!--end global footer-->

</body>
</html>
