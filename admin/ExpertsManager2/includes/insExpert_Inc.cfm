<cfquery name="qryInsertExpert" datasource="weblinks-syntax">
	INSERT INTO Experts
      		(FirstName, MiddleName, LastName, email, WorkPhone, HomePhone, HomePage,	
            Degree, Title, Topics, Credentials, College, CollegeURL, Department, DepartmentURL)          
    VALUES(
    	'#FirstName#',
    	'#MiddleName#',
   		'#LastName#',
    	'#email#',
    	'#WorkPhone#',
        '#HomePhone#',
        '#HomePage#',
        '#Degree#',
        '#Title#',
        '#Topics#',
        '#Credentials#',
        '#College#',
        '#CollegeURL#',
        '#Department#',
        '#DepartmentURL#'  
        
    );    
        
</cfquery>