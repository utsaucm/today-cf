<cfquery name="qryUpdateExpert" datasource="weblinks-syntax">
	UPDATE Experts
    SET 
    		FirstName 		=  	'#FirstName#',
			MiddleName 		= 	'#MiddleName#',
            LastName 		=	'#LastName#',
            email     		=	'#email#',
            WorkPhone		=	'#WorkPhone#',
            HomePhone		=	'#HomePhone#',
            HomePage		=	'#HomePage#',
            Degree			=	'#Degree#',
            Title			=	'#Title#',
            Topics			=	'#Topics#',
            Credentials		=	'#Credentials#',
            College			=	'#College#',
            CollegeURL		=	'#CollegeURL#',
            Department		=	'#Department#',
            DepartmentURL	=	'#DepartmentURL#'            
    WHERE 	ExpertID 		= 	#EID#
        
        
</cfquery>