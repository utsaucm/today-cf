<!--- Start displaying with record 1 if not specified via url --->
<cfparam name="start" default="1" type="numeric">

<!--- Number of records to display on a page --->
<cfparam name="disp" default="25" type="numeric">

<!--- This query all Experts records from the db --->
<cfquery name="qryExperts" datasource="weblinks-syntax">
	SELECT ExpertID, FirstName, MiddleName, LastName, Topics
    FROM Experts;
</cfquery>

<cfset end=Start + disp>

<cfif start + disp GREATER THAN qryExperts.RecordCount>
	<cfset end = 999>
<cfelse>
	<cfset end = disp>
</cfif>
