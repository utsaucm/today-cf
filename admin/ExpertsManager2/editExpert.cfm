<cf_UTSATemplate site="today" columns="1" title="Today Administration Interface">
<cfinclude template="includes/editExpert_Inc.cfm">
<h1>Edit Expert</h1>
<form id="form1" method="post" action="updateExpert.cfm">
<cfoutput query="qryExpertDetail">
<fieldset>
  <legend>Expert Details</legend>
        <p>First Name:<br />
        <input type="text" value="#FirstName#" id="FirstName" name="FirstName"/></p>    	
        <p>Middle Name:<br />
        <input type="text" value="#MiddleName#" id="MiddleName" name="MiddleName"/></p>  	
        <p>Last Name: <br />
        <input type="text" value="#LastName#" id="LastName" name="LastName"/></p>  
</fieldset>	
<fieldset>
	<legend>Contact Information</legend>
	<p>Email:<br />
    	<input type="text" value="#email#" id="email" name="email"/></p>    	
    <p>Work Phone:<br />
        <input type="text" value="#WorkPhone#" id="WorkPhone" name="WorkPhone" />  </p>  	
    <p>Home Phone: <br />
        <input type="text" value="#HomePhone#" id="HomePhone" name="HomePhone" /></p>  
	<p>Home Page: <br />
    	<input type="text" value="#homePage#" size="60" id="homePage" name="homePage"/></p>  
</fieldset>
<fieldset>
	<legend>Other</legend>
	<p>Degree:<br />
    	<input type="text" value="#Degree#" id="Degree" name="Degree"/></p>    	
    <p>Title:<br />
        <input type="text" value="#Title#" size="60" id="Title" name="Title" />  </p>  	
    <p>Topics:  </p>
      	<textarea cols="50" rows="15" id="Topics" name="Topics">#Topics#</textarea>  
    <p>Credentials:</p>
    	<textarea cols="50" rows="15" name="Credentials" id="Credentials">#Credentials#</textarea>
</fieldset>
<fieldset>
	<legend>Organization</legend>
	<p>College:<br />
    	<input type="text" value="#College#" id="College" name="College"/></p>    	
    <p>College URL:<br />
        <input type="text" value="#CollegeURL#" size="60" id="CollegeURL" name="CollegeURL"/>  </p>  	
    <hr />    
    <p>Department: <br />
        <input type="text" value="#Department#" size="60" id="Department" name="Department"/></p>  
	<p>Department URL: <br />
    	<input type="text" value="#DepartmentURL#" size="60" id="DepartmentURL" name="DepartmentURL"/></p>
    <hr />
    <p>College ID: <br />
    	<select name="CollegeID">
        
        </select>
    </p>
    <p>Department ID: <br />
    	<select name="DepartmentID">
        
        </select>
    </p>   
</fieldset>
<input type="hidden" value="#URL.EID#" id="EID" name="EID"/>
</cfoutput>
<p><input type="submit" value="Save Expert" />
</p>
<p><cfoutput>
<div style="margin-left:635px;"><a href="deleteExpert.cfm?EID=#myEID#">Delete Expert</a></div>
</cfoutput></p>
</form>
<cf_UTSATemplate>