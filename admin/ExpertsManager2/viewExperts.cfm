<cfinclude template="includes/viewExperts_Inc.cfm">
<h1>View Experts</h1>
<p><a href="/today/admin/index.cfm">&lt;&lt; Back to Admin</a></p>
<cfinclude template="includes/linkCounter.cfm">

<table id="expertsTable" border="1" width="650px" align="center" cellspacing="0" cellpadding="2">
  <tr>
    <th>ID</th>
    <th>Expert Name</th>
    <th>Topics</th>
  </tr>
  <cfoutput query="qryExperts" startrow="#start#" maxrows="#end#">
    <tr>
      <td><a href="/today/admin/ExpertsManager2/editExpert.cfm?EID=#ExpertID#">#ExpertID#</a></td>
      <td>#LastName#, #FirstName# #MiddleName#</td>
      <td>#Topics#</td>
    </tr>
  </cfoutput>
</table>
<cfinclude template="includes/linkCounter.cfm">
