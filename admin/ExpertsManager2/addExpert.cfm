<cf_UTSATemplate site="today" columns="1" title="Today Administration Interface">
<h1>Add Expert</h1>
<form id="form1" method="post" action="insExpert.cfm">
<fieldset>
  <legend>Expert Details</legend>
        <p>First Name:<br />
        <input type="text" id="FirstName" name="FirstName"/></p>    	
        <p>Middle Name:<br />
        <input type="text" id="MiddleName" name="MiddleName"/></p>  	
        <p>Last Name: <br />
        <input type="text" id="LastName" name="LastName"/></p>  
</fieldset>	
<fieldset>
	<legend>Contact Information</legend>
	<p>Email:<br />
    	<input type="text"id="email" name="email"/></p>    	
    <p>Work Phone:<br />
        <input type="text" id="WorkPhone" name="WorkPhone" />  </p>  	
    <p>Home Phone: <br />
        <input type="text"  id="HomePhone" name="HomePhone" /></p>  
	<p>Home Page: <br />
    	<input type="text" size="60" id="homePage" name="homePage"/></p>  
</fieldset>
<fieldset>
	<legend>Other</legend>
	<p>Degree:<br />
    	<input type="text"  id="Degree" name="Degree"/></p>    	
    <p>Title:<br />
        <input type="text"  size="60" id="Title" name="Title" />  </p>  	
    <p>Topics:  </p>
      	<textarea cols="50" rows="15" id="Topics" name="Topics"></textarea>  
    <p>Credentials:</p>
    	<textarea cols="50" rows="15" name="Credentials" id="Credentials"></textarea>
</fieldset>
<fieldset>
	<legend>Organization</legend>
	<p>College:<br />
    	<input type="text" id="College" name="College"/></p>    	
    <p>College URL:<br />
        <input type="text" size="60" id="CollegeURL" name="CollegeURL"/>  </p>  	
    <hr />    
    <p>Department: <br />
        <input type="text" size="60" id="Department" name="Department"/></p>  
	<p>Department URL: <br />
    	<input type="text" size="60" id="DepartmentURL" name="DepartmentURL"/></p>
    <hr />
    <p>College ID: <br />
    	<select name="CollegeID">
        
        </select>
    </p>
    <p>Department ID: <br />
    	<select name="DepartmentID">
        
        </select>
    </p>   
</fieldset>
<p><input type="submit" value="Add Expert" /></p>
</form>
<cf_UTSATemplate>