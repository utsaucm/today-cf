<HTML>
<HEAD>
<TITLE>Hash Example</TITLE>
</HEAD>

<BODY>

<P>This example shows the resulting data from the Hash function and from 
the Encrypt function. Try it out by entering your own string and a key of 
your own choice and see the results.
<CFIF IsDefined("FORM.myString")>
    <CFSCRIPT>
       string = FORM.myString;
       key = FORM.myKey;
       encrypted = encrypt(string, key);
       decrypted = decrypt(encrypted, key);
    hashed = hash(string);
   <CFOUTPUT>
    <H4><B>The string:</B></H4> #string# <BR>
    <H4><B>The string's length:</B></H4> #len(string)# <BR>
    <H4><B>The key:</B></H4> #key#<BR>
    <H4><B>Encrypted:</B></H4> #encrypted#<BR>
    <H4><B>The encrypted string's length:</B></H4> #len(encrypted)# <BR>
    <H4><B>Hashed:</B></H4> #hashed#<BR>
    <H4><B>The hashed string's length:</B></H4> #len(hashed)# <BR>
    </CFOUTPUT>
</CFIF>
<FORM ACTION="hashmystring.cfm" METHOD="post">
<P>Input your key:
<P><INPUT TYPE="Text" NAME="myKey" VALUE="foobar">
<P>Input your string to be encrypted:
<P><textArea NAME="myString" cols="40" rows="5" WRAP="VIRTUAL">
This string will be encrypted (try typing some more) and then the same 
string will be hashed.
</textArea>
<INPUT TYPE="Submit" VALUE="Hash my String">
</FORM>
</BODY>
</HTML>       
