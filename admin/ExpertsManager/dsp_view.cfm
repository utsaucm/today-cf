		<h1>View Experts</h1>
		<!--- open query --->
		<cfinclude template="../common/sql/sql_SelectAllExperts.cfm">
		<cfset sQueryName=SelectAllExperts>
		<cfinclude template="../common/act/act_setDatabasePaging.cfm">
			
		<cfOutPut>
			<div>#SelectAllExperts.RecordCount# total records.</div>
		</cfoutput>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
		<cfinclude template="../common/act/act_WriteTableSortLinkFunction.cfm">
		<table class="standard"  border="0">
			<cfOutPut>
			<th>#WriteTableSortLink("ID","ExpertID")#</th>
			<th>#WriteTableSortLink("Expert Name","LastName")#</th>
			<th>#WriteTableSortLink("Topics","topics")#</th>
<!---			<th>#WriteTableSortLink("Custodian","PrimaryCustodianID")#</th>--->
			</cfoutput>
			<CFOUTPUT QUERY="SelectAllExperts" StartRow="#iStartRow#" maxrows="#iMaxRow#">
				<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
					<td><a href="/today/admin/index.cfm?fuseaction=expertsManager.edit&amp;expertID=#ExpertID#">#ExpertID#</a></td>
					<td>#lastName#, #FirstName# #middleName#</td>
					<td>#topics#</td>
<!---					<td>#PrimaryCustodianID#</td>--->
				</tr>
			</CFOUTPUT>
		</table>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">