	<h1>Edit Link</h1>
				<cfinclude template="../common/dsp/dsp_sMessage.cfm">
		<!--- open query --->
		<cfinclude template="../common/sql/sql_SelectOneExpert.cfm">
			<CFOUTPUT QUERY="SelectOneExpert">
			<form method="post" action="/today/admin/index.cfm">
			<input type="hidden" name="fuseaction" value="expertsManager.editProcess" />
			<input type="hidden" name="ExpertID" value="#ExpertID#" />
			
			<fieldset>
			<legend>Expert Details</legend>
				<div><strong>First Name</strong><br />
				<input type="text" name="FirstName" value="#FirstName#" maxlength="50" size="30" /></div>
				<div><strong>Middle Name</strong><br />
				<input type="text" name="MiddleName" value="#MiddleName#" maxlength="20" size="7" /></div>
				<div><strong>Last Name</strong><br />
				<input type="text" name="LastName" value="#LastName#" maxlength="50" size="30" /></div>
			</fieldset>	
			<fieldset>
			<legend>Contact Information</legend>
				<div><strong>E-mail</strong><br />
				<input type="text" name="" value="##" maxlength="50" size="30" /></div>
				<div><strong>Work Phone</strong><br />
				<input type="text" name="workPhone" value="#workPhone#" maxlength="20" size="7" /></div>
				<div><strong>Home Phone</strong><br />
				<input type="text" name="HomePhone" value="#HomePhone#" maxlength="50" size="30" /></div>
				<div><strong>Home Page</strong><br />
				<input type="text" name="homePage" value="#homePage#" maxlength="59" size="60" /></div>
			</fieldset>
			<fieldset>
			<legend>Other</legend>
				<div><strong>Degree</strong><br />
				<input type="text" name="degree" value="#degree#" maxlength="69" size="70" /></div>
				<div><strong>Title</strong><br />
				<input type="text" name="title" value="#title#" maxlength="69" size="70" /></div>

				<div><strong>Topics</strong><br />
				<textarea name="topics" cols="50" rows="10">#topics#</textarea></div>
				<div><strong>Credentials</strong><br />
				<textarea name="credentials" cols="50" rows="10">#credentials#</textarea></div>
			</fieldset>
			<fieldset>
			<legend>Organization</legend>
				<div><strong>College</strong><br />
				<input type="text" name="college" value="#college#" maxlength="69" size="70" /></div>
				<div><strong>College URL</strong><br />
				<input type="text" name="collegeURL" value="#collegeURL#" maxlength="59" size="60" /></div>			
				<hr />

				<div><strong>Department</strong><br />
				<input type="text" name="department" value="#department#" maxlength="69" size="70" /></div>
				<div><strong>Department URL</strong><br />
				<input type="text" name="departmentURL" value="#departmentURL#" maxlength="59" size="60" /></div>	
				<hr />
			</cfoutput>				
		<!--- BEGIN Select data for related table Offices, looking for colleges and Departments.--->
		<!--- First we grab the college and department IDs from the previous query --->
			<cfIF selectOneExpert.CollegeID is "">
				<cfset CollegeID=0>
			<cfELSE>
				<cfset CollegeID = selectOneExpert.CollegeID>
			</cfif>
			<cfIF selectOneExpert.DepartmentID is "">
				<cfset DepartmentID=0>
			<cfELSE>
				<cfset DepartmentID = selectOneExpert.DepartmentID>
			</cfif>
		<!--- Display College ID stuff --->	
				<div><strong>College ID</strong><br />
				<select name="CollegeID">
				<cfset OfficeIDs="0">
				<cfQuery name="SelectColleges" datasource="#Application.primaryDSN#">
				Select OfficeID, OfficeName
				FROM Offices
				WHERE OfficeID=(#CollegeID#)
				</cfquery>
				<!--- First show the selected college --->
				<CFOutPut query="SelectColleges">
					<option value="#OfficeID#" selected>#OfficeName#</option>
					<cfset OfficeIDs="#OfficeID#">
				</cfOutPut>
				<option value=""> ---</option>
				<!--- Then show the rest of the colleges --->
				<cfQuery name="SelectOfficesNotIn" datasource="#Application.primaryDSN#">
				Select OfficeID, OfficeName
				FROM Offices
				WHERE OfficeID not in (#OfficeIDs#) and isCollege=1
				Order by OfficeName
				</cfquery>
				<CFOutPut query="SelectOfficesNotIn"><option value="#OfficeID#">#OfficeName#</option></cfOutPut>
				</select>

		<!--- Display Department ID stuff --->	
				<div><strong>Department ID</strong><br />
				<select name="DepartmentID">
				<cfset OfficeIDs="0">
				<cfQuery name="SelectDepartments" datasource="#Application.primaryDSN#">
				Select OfficeID, OfficeName
				FROM Offices
				WHERE OfficeID=(#DepartmentID#)
				</cfquery>
				<!--- First show the selected department --->
				<CFOutPut query="SelectDepartments">
					<option value="#OfficeID#" selected>#OfficeName#</option>
					<cfset OfficeIDs="#OfficeID#">
				</cfOutPut>
				<!--- Then show the rest of the departments --->
				<option value=""> ---</option>
				<cfQuery name="SelectOfficesNotIn" datasource="#Application.primaryDSN#">
				Select OfficeID, OfficeName
				FROM Offices
				WHERE OfficeID not in (#OfficeIDs#) and isDepartment=1 and Officeparent=#collegeID#
				Order by OfficeName
				</cfquery>
				<CFOutPut query="SelectOfficesNotIn"><option value="#OfficeID#">#OfficeName#</option></cfOutPut>
				</select>
				
			</fieldset>
			
			
			
			<div class="buttons"><input type="submit" name="sFunction" value="save" /> <input type="submit" value="delete" name="sFunction" /></div>
			</form>