<!---

This script was seperated so that I can reuse it for a batch update.


--->

				<!--- Save our data and return to the edit page --->
<cfUpdate datasource="#Application.primaryDSN#" 
formfields="
expertID,
firstName,
middleName,
lastName,
workphone,
homephone,
homepage,
Degree,
title,
topics,
credentials,
college,
collegeURL,
department,
departmentURL,
collegeID,
departmentID
" tablename="Experts">

<!---			
<!--- Delete  and insert Category relationships--->
<cfinclude template="/internet/iom/common/sql/sql_DeleteLinkCategoryAssociations.cfm">
<cfinclude template="/internet/iom/common/sql/sql_InsertCategoryLinkRelations.cfm">

<!--- Delete  and insert subCategory relationships--->
<cfinclude template="/internet/iom/common/sql/sql_DeleteLinkSubCategoryAssociations.cfm">
<cfinclude template="/internet/iom/common/sql/sql_InsertSubCategoryLinkRelations.cfm">

<!--- Delete  and insert Audience relationships--->
<cfinclude template="/internet/iom/common/sql/sql_DeleteLinkAudienceAssociations.cfm">
<cfinclude template="/internet/iom/common/sql/sql_InsertAudienceLinkRelations.cfm">

<!--- Delete  and insert subAudience relationships--->
<cfinclude template="/internet/iom/common/sql/sql_DeleteLinkSubAudienceAssociations.cfm">
<cfinclude template="/internet/iom/common/sql/sql_InsertSubAudienceLinkRelations.cfm">
--->