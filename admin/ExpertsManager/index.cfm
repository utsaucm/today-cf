<!---
While I have yet to do it, this script is slowly being migrated for use under a better fusebox methodology.
Done mostly on a need basis.

--->

	
<cfSwitch expression="#attributes.fuseaction#">	
	<cfCase value="expertsManager.add">
		<cfinclude template="dsp_add.cfm">
	</cfcase>
	<cfCase value="expertsManager.addProcess">
		<cfinclude template="dsp_addProcess.cfm">
	</cfcase>

	
	
	<cfCase value="expertsManager.view">
		<cfinclude template="dsp_view.cfm">
	</cfcase>
	
	
	
	
	<cfCase value="expertsManager.edit">
		<cfinclude template="dsp_EditForm.cfm">		
	</cfcase>
	<cfCase value="expertsManager.editprocess">
			<!--- Process the edit and then redirect to categoryManager.edit --->
		<cfswitch expression="#form.sFunction#">
			<cfCase value="delete">
				<!--- Warn about deleting this data --->
				<h1>Delete expert</h1> 
				<p>Are you sure you want to delete this expert?</p>
				<cfOutPut><p><a href="/today/admin/index.cfm?fuseaction=expertsManager.delete&amp;expertID=#expertID#&amp;lastName=#lastName#">yes</a></p></cfoutput>
			</cfcase>
			<cfDefaultCase>
				<cfinclude template="act_UpdateExpert.cfm">	
				<cflocation url="/today/admin/index.cfm?fuseaction=expertsManager.edit&amp;sMessage=Expert%20updated.&amp;expertID=#expertID#">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	


	<cfCase value="expertsManager.delete">
		<cfInclude template="../common/sql/sql_DeleteExpert.cfm">
		<h1>experts Deleted</h1>
		<cfoutput><div>The expert <b>#url.lastName#</b> was successfully deleted.</div></cfoutput>
	</cfcase>



	<cfCase value="expertsManager.batchUpdate">
		<cfinclude template="dsp_BatchEditForm.cfm">
	</cfcase>
	
	
	
	<cfCase value="expertsManager.batchUpdateProcess">
		<cfinclude template="act_Updateexperts.cfm">		
	</cfcase>	
	
	<cfCase value="expertsManager.subCategories">
		<h1>View expertss Brouped by subCategories</h1>
		<!--- open query --->
		<cfinclude template="../common/sql/sql_SelectexpertssWithSubCategories.cfm">
		<cfset sQueryName=SelectexpertssWithSubCategories>
		<cfinclude template="../common/act/act_setDatabasePaging.cfm">
			
		<cfOutPut>
			<div>#SelectexpertssWithSubCategories.RecordCount# total records.</div>
		</cfoutput>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
		<cfinclude template="../common/act/act_WriteTableSortexpertsFunction.cfm">
		<table class="standard"  border="0">
			<cfOutPut>
			<th>#WriteTableSortexperts("ID","expertsID")#</th>
			<th>#WriteTableSortexperts("experts Name","experts_Name")#</th>
			<th>#WriteTableSortexperts("Sub-Category","subcategoryName")#</th>
			<th>#WriteTableSortexperts("Custodian","PrimaryCustodianID")#</th>
			</cfoutput>
			<CFOUTPUT QUERY="SelectexpertssWithSubCategories" StartRow="#iStartRow#" maxrows="#iMaxRow#">
				<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
					<td><a href="/today/admin/index.cfm?fuseaction=expertsManager.edit&amp;expertsID=#expertsID#">#expertsID#</a></td>
					<td>#experts_Name#</td>
					<td>#subCategoryName#</td>
					<td>#PrimaryCustodianID#</td>
				</tr>
			</CFOUTPUT>
		</table>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
	</cfcase>	
</cfSwitch>



<!--- 	<cfCase value="expertsManager.viewSingle">
		<cfinclude template="">
</cfcase> --->
