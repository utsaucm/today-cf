	<h1>Edit Link</h1>
				<cfinclude template="../common/dsp/dsp_sMessage.cfm">
		<!--- open query --->
			<form method="post" action="/today/admin/index.cfm">
			<input type="hidden" name="fuseaction" value="linkManager.editProcess" />
	
				
			<table class="standard" >
			<tr>
				<th>ID</th>
				<th>Link</th>
				<th>Categories</th>
				<th>Sub-Categories</th>
				<th>Audiences</th>
				<th>Sub-Audiences</th>
			</tr>
			<cfinclude template="../common/sql/sql_SelectAllLinks.cfm">
			<cfset iLooper="0">
			<CFLoop query="SelectAllLinks">

			<tr>
				<cfset iLooper=iLooper+1>
				<cfOutPut>
				<td>#SelectAllLinks.LinkID# (#iLooper#)</td>
				<td>
					#SelectAllLinks.Link_Name#
					<input type="hidden" name="linkID" value="#linkID#" />
				</td>
				</cfoutput>
				<td>
					<select name="CategoryID" size="1">
					<cfset categoryIDs="0"><!--- There is no CategoryID with 0, done so that I don't have to bother programming for the "," in the query below. --->
					<cfset iCategoriesParent = 0><!--- Counts the number of categories that our link belongs too. --->
					<cfinclude template="../common/sql/sql_SelectCategoryForOneLink.cfm">
						<!--- Output the Categories which our link belongs to first. --->
						<CFOutPut query="SelectCategoryForOneLink">
							<option value="#SelectCategoryForOneLink.CategoryID#" selected="selected">#SelectCategoryForOneLink.CategoryName#</option>
							<cfset categoryIDs="#categoryIDs#,#SelectCategoryForOneLink.CategoryID#">
							<cfset iCategoiresParent=iCategoriesParent&1>
						</cfOutPut>
					<cfinclude template="../common/sql/sql_SelectCategoriesForOneLink.cfm">
						<!--- Show the rest of the categories.--->
						<CFOutPut query="SelectCategoriesForOneLink">
							<option value="#SelectCategoryForOneLink.CategoryID#">#SelectCategoryForOneLink.CategoryName#</option>
						</cfOutPut>
					</select>
					<cfOutPUT><input type="hidden" name="iCategoriesParent" value="#iCategoriesParent#" /></cfoutput>
				</td>
				<td>
					<select name="subCategoryID">
					<cfset subCategoryIDs="0"><!--- There is no CategoryID with 0, done so that I don't have to bother programming for the "," in the query below. --->
					<cfset isubCategoriesParent = 0><!--- Counts the number of categories that our link belongs too. --->
					
					<cfinclude template="../common/sql/sql_SelectSubCategoryForOneLink.cfm">
						<!--- Output the Categories which our link belongs to first. --->
						<CFOutPut query="SelectSubCategoryForOneLink">
							<option value="#subCategoryID#" selected="selected">#subCategoryName#</option>
							<cfset SubcategoryIDs="#SubcategoryIDs#,#SubCategoryID#">
							<cfset iCategoiresParent=iCategoriesParent&1>
						</cfOutPut>
					<cfinclude template="../common/sql/sql_SelectSubCategoriesForOneLink.cfm">
						<!--- Show the rest of the categories.--->
						<option value="0"></option>
						<CFOutPut query="SelectSubCategoriesForOneLink">
							<option value="#subCategoryID#">#subCategoryName#</option>
						</cfOutPut>
					</select>
					<cfOutPUT><input type="hidden" name="iSubCategoriesParent" value="#isubCategoriesParent#" /></cfoutput>
				</td>
				<td>
					<select name="AudienceID" size="4" multiple="true">
					<cfset AudienceIDs="0"><!--- There is no CategoryID with 0, done so that I don't have to bother programming for the "," in the query below. --->
					<cfset iAudiencesParent = 0><!--- Counts the number of categories that our link belongs too. --->
					<cfinclude template="../common/sql/sql_SelectAudienceForOneLink.cfm">
						<!--- Output the Categories which our link belongs to first. --->
						<CFOutPut query="SelectAudienceForOneLink">
							<option value="#AudienceID#" selected="selected">#AudienceName#</option>
							<cfset categoryIDs="#AudienceIDs#,#AudienceID#">
							<cfset iAudiencesParent=iAudiencesParent&1>
						</cfOutPut>
					<cfinclude template="../common/sql/sql_SelectAudiencesForOneLink.cfm">
						<!--- Show the rest of the audiences.--->
						<CFOutPut query="SelectAudiencesForOneLink">
							<option value="#AudienceID#">#AudienceName#</option>
						</cfOutPut>
					</select>
				</td>
		
		
				<td>
					<select name="subAudienceID" size="4" multiple="true">
					<cfset subAudienceIDs="0"><!--- There is no CategoryID with 0, done so that I don't have to bother programming for the "," in the query below. --->
					<cfset isubAudiencesParent = 0><!--- Counts the number of categories that our link belongs too. --->
					<cfinclude template="../common/sql/sql_SelectSubAudienceForOneLink.cfm">
						<!--- Output the Audiences which our link belongs to first. --->
						<CFOutPut query="SelectSubAudienceForOneLink">
							<option value="#subAudienceID#" selected="selected">#subAudienceName#</option>
							<cfset SubAudienceIDs="#SubAudienceIDs#,#SubAudienceID#">
							<cfset iAudiencesParent=iAudiencesParent&1>
						</cfOutPut>
					<cfinclude template="../common/sql/sql_SelectSubAudiencesForOneLink.cfm">
						<!--- Show the rest of the Audiences.--->
						<CFOutPut query="SelectSubAudiencesForOneLink">
							<option value="#subAudienceID#">#subAudienceName#</option>
						</cfOutPut>
					</select>
				</td>

			</tr>			
			</cfLoop>
			</table>
			
			<div class="buttons"><input type="submit" name="sFunction" value="save" /> <input type="submit" name="sFunction" value="save &amp; return to links view" /> <input type="submit" value="delete" name="sFunction" /></div>
			</form>