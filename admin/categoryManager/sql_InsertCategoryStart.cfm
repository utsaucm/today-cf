<cfquery datasource="#Application.primaryDSN#" name="InsertCategoryStart">
SET NOCOUNT ON
INSERT INTO RSSCategories (categoryName)
VALUES ('#CategoryName#')
SELECT ThisID = @@Identity
SET NOCOUNT OFF
</cfquery>