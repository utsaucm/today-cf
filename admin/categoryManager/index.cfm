<cfswitch expression="#attributes.fuseaction#">
	<cfcase value="categoryManager.add">
		<h1>Add Category</h1>
		<cfinclude template="../common/dsp/dsp_sMessage.cfm">
			<form method="post" action="/today/admin/index.cfm">
				<input type="hidden" name="fuseaction" value="categoryManager.addProcess" />
				<fieldset>
					<legend></legend>
					<div>Category Name:<br />
					<input type="text" name="CategoryName" value="" maxlength="40" size="50" /></div>

				</fieldset>
				<div><input type="submit" value="save" /> <!--<input type="submit" value="cancel" />--></div>
			</form>
	</cfcase>

	<cfcase value="categoryManager.addProcess">
		<cfinclude template="sql_InsertCategoryStart.cfm">
		<cfset CategoryID = InsertCategoryStart.ThisID>
		<cflocation url="/today/admin/index.cfm?fuseaction=categoryManager.edit&sMessage=Please add detailed information about this category.&CategoryID=#CategoryID#">
	</cfcase>

	<cfcase value="categoryManager.view">
		<h1>View Categories</h1>
		<cfinclude template="../common/dsp/dsp_sMessage.cfm">
		<!--- open query --->
		<cfinclude template="sql_SelectAllCategories.cfm">
		<cfset sQueryName=SelectAllCategories>
		<cfinclude template="../common/act/act_SetDatabasePaging.cfm">

		<cfoutput>
			<div>#SelectAllCategories.RecordCount# total records.</div>
		</cfoutput>
		<table class="standard"  border="0">
			<cfinclude template="../common/act/act_WriteTableSortLinkFunction.cfm">
			<cfoutput>
				<th>#WriteTableSortLink("ID","CategoryID")#</th>
				<th>#WriteTableSortLink("Category Name","CategoryName")#</th>
				<th>#WriteTableSortLink("Description","CategoryDescription")#</th>
			</cfoutput>
			<cfoutput QUERY="SelectAllCategories" StartRow="#iStartRow#" maxrows="#iMaxRow#">
				<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
					<td><a href="/today/admin/index.cfm?fuseaction=categoryManager.edit&categoryID=#categoryID#">#categoryID#</a></td>
					<td>#CategoryName#</td>
					<td>#CategoryDescription#</td>
				</tr>
			</cfoutput>
		</table>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
	</cfcase>

	<cfcase value="categoryManager.edit">
		<h1>Edit Category</h1>
		<cfinclude template="../common/dsp/dsp_sMessage.cfm">
		<!--- open query --->
		<cfinclude template="sql_SelectOneCategory.cfm">
			<cfoutput query="SelectOneCategory">
			<form method="post" action="/today/admin/index.cfm">
			<input type="hidden" name="fuseaction" value="categoryManager.editProcess" />
			<input type="hidden" name="CategoryID" value="#CategoryID#" />
			<fieldset>
			<legend>Primary</legend>
				<div>Category Name:<br />
				<input type="text" name="CategoryName" value="#CategoryName#" maxlength="40" size="50" /></div>
				<!--- <div>URL:<br />
				<input type="text" name="url" value="#url#" maxlength="50" size="75" /></div> --->
				<div>Status:<br />
				<select name="CategoryStatus">
					<option value="0" <cfIf CategoryStatus is 0>Selected="selected"</cfif>>off</option>
					<option value="1" <cfIf CategoryStatus is 1>Selected="selected"</cfif>>on</option>
				</select></div>

				<div>Description:<br />
				<textarea name="CategoryDescription" cols="40" rows="8">#Categorydescription#</textarea></div>
			</fieldset>
			</cfoutput>


			<div class="buttons">
				<input type="submit" value="save" name="sFunction" />
				<input type="submit" value="delete" name="sFunction" />
			</div>
			</form>

	</cfcase>
	<cfcase value="categoryManager.editProcess">
		<cfswitch expression="#form.sFunction#">
			<cfcase value="save &amp; return to links view">
				<!--- Save our data and go to the main links listing --->
				<cfupdate datasource="#Application.primaryDSN#" formfields="LinkID,Link_Name,Link_Description,url" tablename="Links">
				<!--- <cflocation url="index.cfm?sMessage=Link updated.&LinkID=#LinkID#">			--->
			</cfcase>
			<cfcase value="delete">
				<!--- Warn about deleting this data --->
				<cfoutput>
				<h1>Delete Category</h1>
				<p>Are you sure yo want to delete this category?</p>
				<p>
					<form method="post" action="/today/admin/index.cfm">
					<input type="hidden" name="fuseaction" value="categoryManager.delete" />
					<input type="hidden" name="CategoryID" value="#CategoryID#" />
					<input type="hidden" name="CategoryName" value="#CategoryName#" />
					<input type="submit" value="delete" name="sFunction" />
				</p>
				</cfoutput>
			</cfcase>
			<cfdefaultcase>
				<!--- Process the edit and then redirect to categoryManager.edit --->
				<cfupdate datasource="#Application.primaryDSN#" formfields="CategoryID,CategoryName,CategoryDescription,CategoryStatus" tablename="rsscategories">
					<h1>Category Updated</h1>
					<cfoutput><div>The category entitled <b>#Form.CategoryName#</b> was successfully updated.</div></cfoutput>
			</cfdefaultcase>
		</cfswitch>
	</cfcase>

	<cfcase value="categoryManager.delete">
		<cfinclude template="sql_DeleteCategory.cfm">
		<h1>Category Deleted</h1>
		<cfoutput><div>The category entitled <b>#Form.CategoryName#</b> was successfully deleted.</div></cfoutput>
	</cfcase>
</cfSwitch>
