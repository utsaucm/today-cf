<cfapplication
	name="TodayApp"
	sessionmanagement="yes"
	setclientcookies="yes"
	sessiontimeout="#CreateTimeSpan(0,3,0,0)#">


<!---This chunk of code are the session variables all should use--->
<cflock scope="session" type="readonly" timeout="10">
	<!---Setup Application DSN--->
	<cfset session.dbDSN="joBob1">
	<!---Sets the Date to the current date on the users PC in the format "MM/DD/YY"--->
	<cfset session.defaultDate = #DateFormat(Now(), "MM/DD/YYYY")#>
	<cfset session.LoginTest = False />
</cflock>
