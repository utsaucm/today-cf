<cfif isdefined("url.sqlSort") is False>
	<cfset sqlSort = "ArticleID">
</cfif>
<cfIf isdefined("url.sAscDesc") is False>
	<cfset sAscDesc = "desc">
</cfif>
<cfQuery name="SelectAllArticles" datasource="#Application.primaryDSN#">
SELECT * FROM Articles
	 ORDER BY #sqlSort# #sAscDesc#
	<!--- Order by <cfqueryparam value="#sqlSort#" cfsqltype="cf_sql_char" maxlength="25">
	<cfqueryparam value="#sAscDesc#" cfsqltype="cf_sql_char" maxlength="25"> --->
</cfquery>
<h3>Current Database: <cfoutput>#Application.primaryDSN#</cfoutput></h3>
<!---
Listing of fields:

ArticleID
DateAdded
DateStart
DateExpire
DateEmphasis
DateLastModified
Title
Teaser
Content
URL
CategoryID
HistoricalRating
ArticleTypeID
MarketAudienceID
CustodianLastEditID
CustodianOwnerID
CustodianLastEditIP
--->
