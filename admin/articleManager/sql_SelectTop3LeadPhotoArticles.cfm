<!--- This T-SQL query does filter out future dates :) --->
<cfquery name="SelectTop3LeadPhotoArticles" datasource="#Application.primaryDSN#">
	Select top 3 * 
	FROM Articles
	WHERE leadphoto = 1 AND leadPhotoAddress IS NOT NULL AND (DateExpire IS NULL OR DateExpire > {fn Now()})
	Order by DateStart DESC
</cfquery>