<!--- dsp_viewArticles is the default view for ArticleManager --->
<cfif isdefined("url.rank") is "YES">
	<cfinclude template="sql_RankArticle.cfm" />
</cfif>

		<h1>View Articles</h1>
		<!--- open query --->
		<cfinclude template="sql_SelectAllArticles.cfm" />
		<cfset sQueryName=SelectAllArticles />
		<cfset currentID = 0 />
		<cfset prevID = 0 />
		<cfset currentTS = now() />
		<cfset previousTS = now() />
		<cfinclude template="../common/act/act_SetDatabasePaging.cfm" />

		<cfoutput>
			<div>#SelectAllArticles.RecordCount# total records.</div>
		</cfoutput>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm" />
		<cfinclude template="../common/act/act_WriteTableSortLinkFunction.cfm" />

		<table class="admin"  border="0">
			<cfoutput>
			<tr>
				<th>#WriteTableSortLink("ID","ArticleID")#</th>
				<th>#WriteTableSortLink("Title","Title")#</th>
				<th>#WriteTableSortLink("Start","DateStart")#</th>
				<th>#WriteTableSortLink("Expires","DateExpires")#</th>
        <th>#WriteTableSortLink("Date Added","DateAdded")#</th>
        <th>#WriteTableSortLink("Lead Photo","leadPhoto")#</th>
        <th>#WriteTableSortLink("More News Story","moreNewsStory")#</th>
        <th>#WriteTableSortLink("Campus Wire Story","internalStory")#</th>
        <th>#WriteTableSortLink("Announcement","aroundCampus")#</th>
				<th>#WriteTableSortLink("In The News","inTheNews")#</th>
				<cfif (ListFind(request.attributeslist, "sqlSort=DateStart", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<th>Order</th>
				</cfif>
			</tr>
      </cfoutput>
			<cfoutput query="SelectAllArticles" StartRow="#iStartRow#" maxrows="#iMaxRow#">
			<cfset currentTS = DateStart />
			<cfset rankLink = "/today/admin/index.cfm?fuseaction=ArticleManager.view&sqlSort=DateStart&sAscDesc=desc&" />
			<cfif currentrow NEQ 1>
				<cfif (ListFind(request.attributeslist, "sqlSort=DateStart", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<cfif DateCompare(previousTS, now()) EQ -1>
				<a href="#rankLink#rank=down&NextID=#ArticleID#&ArticleID=#prevID#">&darr;</a>
				</cfif>
				</cfif>
				</td>
			</tr>
			</cfif>
			<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
				<td><a href="/today/admin/index.cfm?fuseaction=ArticleManager.edit&ArticleID=#ArticleID#">#ArticleID#</a></td>
				<td>#Title#</td>
				<td>#(Len(DateStart) ? "#DateFormat(DateStart,"mm/dd/yy")# #TimeFormat(DateStart, "short")#" : "-----")#
				</td>
        <td>#(Len(DateExpire) ? "#DateFormat(DateExpire,"mm/dd/yy")# " : "-----")#</td>
        <td>#(Len(DateAdded) ? "#DateFormat(DateAdded,"mm/dd/yy")#" : "-----")#</td>
        <td><cfif #leadPhoto# EQ 1>Yes<cfelseif #leadPhoto# EQ 0>No<cfelse>-----</cfif></td>
        <td><cfif #moreNewsStory# EQ 1>Yes<cfelseif #moreNewsStory# EQ 0>No<cfelse>-----</cfif></td>
        <td><cfif #internalStory# EQ 1>Yes<cfelseif #internalStory# EQ 0>No<cfelse>-----</cfif></td>
        <td><cfif #aroundCampus# EQ 1>Yes<cfelseif #aroundCampus# EQ 0>No<cfelse>-----</cfif></td>
					<td><cfif #inTheNews# EQ 1>Yes<cfelseif #inTheNews# EQ 0>No<cfelse>-----</cfif></td>
				<td>
				<cfif (ListFind(request.attributeslist, "sqlSort=DateStart", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<cfif currentrow NEQ 1>
					<cfif (DateCompare(previousTS, now()) EQ -1) AND (DateCompare(currentTS, now()) EQ -1)>
					<a href="#rankLink#rank=up&previousID=#prevID#&ArticleID=#ArticleID#">&uarr;</a>
					</cfif>
				</cfif>
				</cfif>
				<cfset prevID = ArticleID />
				<cfset previousTS = DateStart />
			</cfoutput>
			<!--- always end the table --->
			</td></tr>
		</table>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
