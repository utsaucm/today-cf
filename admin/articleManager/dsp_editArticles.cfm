<!---<cfdump var="#url#" label="edit Articles URLs">--->
<script language="javascript">
/*
  This function allows the Use Custom Photo checkbox to be enabled/disabled
  based on whether or not the Show Photo checkbox is checked
*/

function publicCheck(){

	if(document.myform.leadPhoto.checked==true){
	    //setStyleById("leadPhotoAddressText", "display", "block");
		//setStyleById("leadPhotoThumbAddressText", "display", "block");
	}

	if(document.myform.leadPhoto.checked==false){
		//setStyleById("leadPhotoAddressText", "display", "none");
		//setStyleById("leadPhotoThumbAddressText", "display", "none");
	}

}
// setStyleById: given an element id, style property and
// value, apply the style.
// args:
//  i - element id
//  p - property
//  v - value
//
function setStyleById(i, p, v) {
        var n = document.getElementById(i);
        n.style[p] = v;
}
</script>


<h1>Edit Article</h1>
<cfinclude template="../common/dsp/dsp_sMessage.cfm">
	<!--- open query --->
	<cfinclude template="sql_SelectOneArticle.cfm"/>
	<cfoutput QUERY="SelectOneArticle">

		<!---<form method="post" action="/today/admin/index.cfm" id="myform" name="myform">--->
    <form method="post" action="/today/admin/index.cfm" id="myform" name="myform">
			<input type="hidden" name="fuseaction" value="ArticleManager.editProcess" />
			<input type="hidden" name="ArticleID" value="#ArticleID#" />
			<input type="hidden" name="CustodianLastEditIP" value="#cgi.Remote_Addr#" />
			<input type="hidden" name="CustodianLastEditID" value="#GetAuthUser()#" />
			<input type="hidden" name="DateLastModified" value="#now()#" />
      <!---  <input type="hidden" name="intStoryStatus" value="#form.internalStory#" />--->
			<!--- ARTICLE CONTENTS --->
			<table  border="0" cellpadding="10" width="90%">
        <tr>
          <td><div class="adminUnit">
              <h2>Article Information:</h2>
							<div>Title:<br />

								<!-- <input type="text" name="Title" value="#title#" maxlength="64" size="65" /></div> -->
								<!--- add by Ruben Ortiz on 6-25-2009 --->
                <input type="text" name="Title" value="#title#" maxlength="100" size="100" />
							</div>
              <div>Teaser:<br />
								<!-- <input type="text" name="Teaser" value="#Teaser#" maxlength="65" size="65" /></div> -->
								<!--- add by Ruben Ortiz on 6-25-2009 --->
                <input type="text" name="Teaser" value="#Teaser#" maxlength="100" size="100" />
							</div>

              <div>Author:<br />
								<input type="text" name="author" value="#author#" maxlength="55" size="75" />
							</div>

              <div>Submitter Dept/College/Office:<br />
								<input type="text" name="submitter" value="#submitter#" maxlength="55" size="75" />
							</div>

							<div>URL: (/today/2005/09/stafford.html)<br />
								<input type="text" name="url" value="#SelectOneArticle.URL[SelectOneArticle.currentRow]#" maxlength="150" size="150" />
							</div>

							<div>Content:<br />
								<textarea name="Content" cols="150" rows="20">#Content#</textarea>
							</div>

              <div id="leadPhotoAddressText">Image Address (required for Lead and More stories):<br />
                <input type="text" name="leadPhotoAddress" id="leadPhotoAddress" value="#leadPhotoAddress#" maxlength="55" size="65" />
                <br />
                Example - /today/images/events/actors/twelfth.jpg<br />
                <img src="#leadPhotoAddress#" alt="" name="buttonOne"  />
              </div>

              <div id="leadPhotoAddressText2">Image Address Landscape orientation for Home page(required for Lead and More stories):<br />
            	  <input type="text" name="leadPhotoAddress2" id="leadPhotoAddress2" value="#leadPhotoAddress2#" maxlength="55" size="65" />
                <br />
                Example - /today/images/events/actors/twelfth.jpg<br />
                <img src="#leadPhotoAddress2#" alt="" name="buttonOne"  />
              </div>

              <!---
              <div id="leadPhotoThumbAddressText">
                Thumb Image Address:<br />
                <input type="text" name="leadPhotoThumbAddress" id="leadPhotoThumbAddress" value="#leadPhotoThumbAddress#" maxlength="55" size="65" />
                <br />
                Example - /today/images/events/actors/twelfth-thumb.jpg<br />
                <img src="#leadPhotoThumbAddress#" alt="" name="buttonOne"  />
              </div>
    					--->
    				</div>

						<h2>Article Type:</h2>
    				<div class="adminUnit">
							<h3>
        			<!---<input onClick="publicCheck(this.form)" type="checkbox" name="leadPhoto" id="leadPhoto" value="1" <cfif leadPhoto is "1">checked="checked" </cfif> /> Lead Photo--->
	  						<input type="radio" id="leadPhoto" name="StoryType"  value="leadPhoto" <cfif SelectOneArticle.leadPhoto EQ 1>checked="checked"</cfif> /> Lead Photo
							</h3>
        			<h3>
								<input type="radio" id="moreNewsStory" name="StoryType" value="moreNewsStory" <cfif SelectOneArticle.moreNewsStory EQ 1>checked="checked"</cfif> /> More News Story
							</h3>
			        <h3>
		            <input type="radio" id="internalStory" name="StoryType" value="internalStory"<cfif SelectOneArticle.internalStory EQ 1>checked="checked"</cfif> /> Campus Newswire Story
			        </h3>
							<h3>
				        <input type="radio" id="aroundCampus" name="StoryType"  value="aroundCampus" <cfif SelectOneArticle.aroundCampus EQ 1>checked="checked"</cfif> /> Announcements Story
							</h3>
							<h3>
				        <input type="radio" id="inTheNews" name="StoryType"  value="inTheNews" <cfif SelectOneArticle.inTheNews EQ 1>checked="checked"</cfif> /> In The News
							</h3>
						</div>

<!--- HANDLE DATES FIRST --->
<div class="adminUnit">
		<cfset DateEmphasis = "#DateFormat(DateEmphasis,"m/d/yyyy")# #timeFormat(dateEmphasis,"HH:mm")#">
		<cfset DateExpire = "#DateFormat(DateExpire,"m/d/yyyy")# #timeFormat(dateExpire,"HH:mm")#">

    		<h2>Article Dates:</h2>
			  <div>
              		Start:<br />
              		<cfif DateStart is "">
                         <input type="text" name="DateStart" value="" maxlength="30" size="35" />
                    <cfelse>
                        <input type="text" name="DateStart" value="#DateFormat(DateStart,"m/d/yyyy")# #timeFormat(DateStart,"HH:mm")#" maxlength="30" size="35" />
                    </cfif>
                </div>
				<!---<input type="text" name="DateStart" value="#DateFormat(DateStart,"m/d/yyyy")# #timeFormat(dateStart,"HH:mm")#" maxlength="30" size="35" />--->
			    <div>
                	Expires: (Example - 9/12/2005 14:06) <br />
				<!---<div>Emphasis:<br />
				<input type="text" name="DateEmphasis" value="#dateEmphasis#" maxlength="30" size="35" /></div> --->

					<!--- There is a strange error associated with the space in between the date and time values below when there is no date expire --->
                    <cfif dateExpire is "">
                        <input type="text" name="DateExpire" value="" maxlength="30" size="35" />
                    <cfelse>
                        <input type="text" name="DateExpire" value="#DateFormat(DateExpire,"m/d/yyyy")# #timeFormat(dateExpire,"HH:mm")#" maxlength="30" size="35" /></div>
                    </cfif>
				</div>
                <div style="color:##666666">Article added #timeFormat(dateAdded,"HH:mm")#, #DateFormat(dateAdded,"m/d/yyyy")#. Last modified #DateLastModified# by #CustodianLastEditID#.</div>
			</cfOutPut>
</div>
<div class="adminUnit">

<h2>Categories</h2>
				<div>

				<cfset categoryIDs="0"><!--- There is no CategoryID with 0, done so that I don't have to bother programming for the "," in the query below. --->
				<cfset iCategoriesParent = 0><!--- Counts the number of categories that our Article belongs too. --->
				<cfinclude template="sql_SelectCategoryForOneArticle.cfm">
					<!--- Output the Categories which our Article belongs to first. --->
					<select name="CategoryID" multiple="true" size="30">
					<CFOutPut query="SelectCategoryForOneArticle">
						<option value="#CategoryID#" selected="selected">#CategoryName#</option>
						<cfset categoryIDs="#categoryIDs#,#CategoryID#">
						<cfset iCategoiresParent=iCategoriesParent&1>
					</cfOutPut>
				<cfinclude template="sql_SelectCategoriesForOneArticle.cfm">
					<!--- Show the rest of the categories.--->
					<CFOutPut query="SelectCategoriesForOneArticle">
						<option value="#CategoryID#">#CategoryName#</option>
					</cfOutPut>
				</select></div>
				<div>*Hold the &lt;ctrl&gt; key for multiple categories.</div>
</div>

				</td>
                <td><!--- CATEGORY INFORMATION --->
				</td>
              </tr>

            </table>
			<p>&nbsp;</p>

			<cfOutPUT><input type="hidden" name="iCategoriesParent" value="#iCategoriesParent#" /></cfoutput>
			<div class="buttons"><input type="submit" name="sFunction" value="save" /> <input type="submit" value="delete" name="sFunction" /></div>

			</form>

			<!--- for debugging only --->
		    <!---<cfoutput>Date Expire Var is: #dateexpire#</cfoutput>--->

<script type="text/javascript" language="javascript">
	if(document.myform.leadPhoto.checked==false){
		//document.leadPhotoAddressText=false;
		//setStyleById("leadPhotoAddressText", "display", "none");
		//setStyleById("leadPhotoThumbAddressText", "display", "none");
	}
</script>
