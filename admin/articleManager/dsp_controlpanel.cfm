<h1>Control Panel</h1>
<div style="text-align:center;">
  <strong>KEY: </strong>
  <span style="background-color:#900; color: #fff">&nbsp;L - Lead Photo </span> &nbsp;
  <span style="background-color:#00F; color: #fff">&nbsp;M - More Stories </span>&nbsp;
  <span style="background-color:#00cc00; color: #fff">&nbsp;W - UTSA NewsWire </span> &nbsp;
  <span style="background-color:#F00; color: #fff">&nbsp;A - Announcements </span> &nbsp;
  <!---<span style="background-color:#f15a22; color: #fff">&nbsp;R - Meet a Roadrunner </span> &nbsp;--->
  <span style="background-color:#f15a22; color: #fff">&nbsp;P - Photo of the Day </span> &nbsp;
  <span style="background-color:#000; color: #fff">&nbsp;X - Expired </span>&nbsp;
  <span style="background-color:#eee; color: #000">&nbsp;Arch - Archived </span>
</div>

<!---
  L  -  Lead Photo
  M  -  More Stories
  W -   UTSA NewsWire
  A  -  Announcements
  R  -  Meet A Roadrunner
  X  -  Expired
  P  -  Photo of the Day
--->

<table class="standard"  border="0" style="margin-left: auto; margin-right:auto">
	<tr>
		<th>Type</th>
    <th>Title</th>
    <th>Start</th>
    <th>Expires</th>
	</tr>
  <!--- Set counter to help us run through the 4 and six articles that need to display on the home page. --->
  <cfset articleElementSize = 12 /> <!-- 12 items per entry -->
  <cfset topStoryCount = 3 />
  <cfset marrCount = 1 />
  <cfset moreNewsCount = 10 />
  <cfset newsWireCount = 10 />
  <cfset announcementCount = 10 />
  <cfset itnCount = 1 />
  <cfset aStoriesMax = topStoryCount + marrCount + moreNewsCount + newsWireCount + announcementCount />
  <!--- This is our result array where all the magic is stored (why aren't we using seperate db queries?) --->
  <cfset aStories = ArrayNew(2)>
  <cfloop index="i" from="1" to="#aStoriesMax#">
  	<cfloop index="j" from="1" to="#articleElementSize#">
  		<cfset aStories[i][j] = "">
  	</cfloop>
  </cfloop>
  <!--- array idx pointers (offsets into aStories, why not use a struct?)--->
  <cfset topStory_i = 1 />
  <cfset marr_i = topStory_i + topStoryCount />
  <cfset moreNews_i = marr_i + marrCount />
  <cfset newsWire_i = moreNews_i + moreNewsCount />
  <cfset announcement_i = newsWire_i + announcementCount />
  <cfset itn_i = announcement_i + itnCount />

  <!--- starting indexes --->
  <cfset topStoryStart = topStory_i />
  <cfset marrStart = marr_i />
  <cfset moreNewsStart = moreNews_i />
  <cfset newsWireStart = newsWire_i />
  <cfset announcementStart = announcement_i />
  <cfset itnStart = itn_i />

  <!--- these are flags --->
  <cfset iLeadphoto = 0>
  <cfset iLeadphoto2 = 0>
  <cfset iLeadphoto3 = 0 />
  <cfset meetARoadrunner = 0 />
  <!--- accumulators, these are incremented by act_CategorizeStories
  	(I know, passing scope is BAD! Why aren't we using seperate queries again?)
  	--->
  <cfset futureStories = 0 />
  <cfset moreNewsStories = 0 />
  <cfset newswireStories = 0 />
  <cfset expiredStories = 0 />
  <cfset archivedStories = 0 />
  <cfset announcementStories = 0 />
  <cfset itnStories = 0 />
  <cfset totalStories = 0 />

  <!---<cfset iLeadphoto4 = 0>--->
  <!--- Story Status codes:
    Future Stories (unlimited) 			- 0
  	Lead Photo (1) 					- 1
  	Second Lead Photo 				- 20
  	Third Lead Photo  				- 30
    More News stories (6 count) 	- 2
  	UTSA Newswire  (6 count) 		- 3
   	Expired (unlimited) 			- 4
  	Archives (unlimited) 			- 5
  	Announcement Stories (6 count) 	- 6
  	Photo of the day 	(1 count)	- 7
  --->

  <!--- accumulators used for external and internal categories --->
  <!---
  <cfset extStories = 0 />
  <cfset intStories = 0 />
  <cfset intStories2 = 0 />
  --->
  <cfinclude template="sql_SelectControlPanelArticles.cfm" />
	<cfoutput query="SelectControlPanelArticles">
	  <!---<cfinclude template="act_CategorizeStories.cfm">--->
    <cfinclude template="../../_inc/admin/articleManager/act_CategorizeStories.cfm" />
	  <!--- Decide background colors and letter keys --->
	  <cfswitch expression="#storyStatus#">
  		<cfcase value = "0">
  			<cfset backgroundColor="##333">
              <cfset color = "##FFF">
  			<cfset menuKey = "">
  		</cfcase>
  		<cfcase value="1,20,30"> <!--- Lead Photo --->
  			<cfset backgroundColor="##900">
              <cfset color = "##FFF">
  			<cfset menuKey = "L">
  		</cfcase>
  		<cfcase value = "2"> <!--- More News Story --->
  			<cfset backgroundColor="##00F">
                <cfset color = "##FFF">
  			<cfset menuKey = "M">
  		</cfcase>
  		<cfcase value = "3"> <!--- UTSA NewsWire Story --->
  			<cfset backgroundColor="##00cc00">
  			<cfset menuKey = "W">
  		</cfcase>
      <!--- <cfcase value = "5">  Meet a Roadrunner --->
		    <!---	<cfset backgroundColor="##f15a22">
              <cfset color = "##FFF">
			        <cfset menuKey = "R">
		  </cfcase>--->
      <cfcase value = "4"> <!--- Expired --->
        <cfset backgroundColor="##000">
        <cfset color = "##fff">
        <cfset menuKey = "X">
      </cfcase>
      <cfcase value="5"> <!--- Archived --->
        <cfset backgroundColor="##eee">
        <cfset color = "##000">
        <cfset menuKey = "Arch">
      </cfcase>
      <cfcase value = "6"> <!--- Announcement  --->
        <cfset backgroundColor="##FF0000">
        <cfset color = "##FFF">
        <cfset menuKey = "A">
      </cfcase>
      <cfcase value = "7"> <!--- Photo of the Day  --->
        <cfset backgroundColor="##f15a22">
        <cfset color = "##FFF">
        <cfset menuKey = "P">
      </cfcase>
      <cfdefaultcase> <!--- Something went wrong --->
        <cfset menuKey = "?">
        <cfset backgroundColor="##808080">
        <cfset color = "##FFF">
      </cfdefaultcase>
    </cfswitch>
	<tr style="background-color:#backgroundColor#; color: #color#;">
	<!---	<td><a href="http://localhost:8500/today/admin/index.cfm?fuseaction=ArticleManager.edit&ArticleID=#ArticleID#">#ArticleID#</a></td> --->
		<td style="color: #color#; text-align:center"><strong>#menuKey#</strong></td>
		<td style="color: ##fff;"><a style="color: #color#;" href="/today/admin/index.cfm?fuseaction=ArticleManager.edit&ArticleID=#ArticleID#"><cfif leadphoto is 1><img src="iconImage.gif" alt="" /></cfif>#Title#</a></td>
		<td style="color: #color#;">#DateFormat(DateStart, 'MM/DD/YYYY')# #TimeFormat(DateStart, 'hh:mm:ss tt')#</td>
		<td style="color: #color#;">#DateFormat(DateExpire, "MM/DD/YYYY")# #TimeFormat(DateExpire,'hh:mm:ss tt')#</td>
	<!--- Check for other lead photos --->
	</tr>
</cfoutput>
</table>
