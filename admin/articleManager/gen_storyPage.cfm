<cfscript>
  tmpl = StructNew();
  tmpl['story'] = "/today/template/story.html";
  tmpl['story-noevents'] = "today/template/storynoevents.html";
  tmpl['pod'] = "today/template/pod.html";

  keywords = ArrayNew(1);
  keywords = ['DATE', 'TITLE', 'IMAGE_URL', 'CAPTION'];
  vals = StructNew();
</cfscript>

<cfparam name="tmplType" default="story" />
<cfset tmplLoc = tmpl['#tmplType#'] />

<cfset tmplFile = "#ExpandPath(tmplLoc)#" />

<cffile action="read" file="#tmplFile#"  variable="tmplFileContents" />

<cfinclude template="sql_SelectOneArticle.cfm"/>

<cfoutput query="SelectOneArticle">
  <cfscript>
  vals['DATE'] = "#SelectOneArticle.DateStart[currentrow]#";
  vals['TITLE'] = CleanHighAscii("#SelectOneArticle.Title[currentrow]#", true);
  vals['CAPTION'] = CleanHighAscii("#SelectOneArticle.Teaser[currentrow]#", true);
  vals['IMAGE_URL'] = "#SelectOneArticle.leadPhotoAddress[currentrow]#";
  </cfscript>
</cfoutput>

<cfdump var="#tmpl#" />
<cfdump var="#keywords#" />
<cfdump var="#vals#" />
<cfdump var="#tmplType#" />
<cfdump var="#tmplFile#" />

<cfabort/>


<cfdump var="#tmplFileContents#" />
