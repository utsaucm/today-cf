<cfinclude template="/CustomTags/security/safeText.cfm">
<!---
<cfquery datasource="#Application.primaryDSN#" name="InsertArticleStart">
SET NOCOUNT ON
INSERT INTO Articles (Title,DateStart)
VALUES ('#Title#',#now()#)
SELECT ThisID = @@Identity
SET NOCOUNT OFF
</cfquery>
--->

<cfquery datasource="#Application.primaryDSN#" name="InsertArticleStart">
	INSERT INTO Articles (Title,DateAdded)
	<!--- VALUES ('#Title#', #now()#) --->
	VALUES (<cfqueryparam value="#Title#" cfsqltype="cf_sql_char">, #now()#)

</cfquery>
<cfquery datasource="#Application.primaryDSN#" name="SelectRecent">
	SELECT top 1 ArticleID
	from Articles
	Order by ArticleID desc
</cfquery>

<cfoutput query="SelectRecent">
	<!--- escape output coming from database query using SafeText function --->
	<cfset ArticleID = SafeText(SelectRecent.articleID)>
</cfoutput>