<cfparam name="articleID" default="0" />
<cfparam name="direction" default="up" />
<cfparam name="previousID" default="0" />
<cfparam name="nextID" default="0" />

<cfif isDefined("url.rank") is "YES">
  <cfset direction = url.rank />
  <cfset articleID = url.ArticleID />
  <cfif isDefined("direction") is "YES">
    <cfif direction EQ "up">
      <cfset previousID = url.previousID />
      <cfif (previousID GT 0) AND (articleID GT 0)>
        <cfquery name="previousArticleDT" datasource="#Application.primaryDSN#">
        SELECT DATEADD(minute, 1, DateStart) AS newdt FROM Articles WHERE ArticleID = #previousID#
        </cfquery>
        <cfoutput query="previousArticleDT" startrow="1" maxrows="1">
          <cfquery name="rankUpUpdate" datasource="#Application.primaryDSN#">
            UPDATE Articles SET DateStart = '#newdt#' WHERE ArticleID = #articleID#
          </cfquery>
        </cfoutput>
      </cfif>
    </cfif>
    <cfif direction EQ "down">
      <cfset nextID = url.NextID />
      <cfif (nextID GT 0) AND (articleID GT 0)>
        <cfquery name="nextArticleDT" datasource="#Application.primaryDSN#">
        SELECT DATEADD(minute, -1, DateStart) AS newdt FROM Articles WHERE ArticleID = #nextID#
        </cfquery>
        <cfoutput query="nextArticleDT" startrow="1" maxrows="1">
          <cfquery name="rankDownUpdate" datasource="#Application.primaryDSN#">
          UPDATE Articles SET DateStart = '#newdt#' WHERE ArticleID = #articleID#
          </cfquery>
        </cfoutput>
      </cfif>
    </cfif>
  </cfif>
</cfif>
