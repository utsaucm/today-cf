<!--- Include a UDF used to help remove ascii non-printable chars from an MS Word source document --->
<cfinclude template="DeMoronize.cfm">
<!---
	These are the Database fields that I will be using.

	ArticleID
	DateAdded
	DateStart
	DateExpire
	DateEmphasis
	DateLastModified
	Title
	Teaser
	Content
	URL
	CategoryID
	HistoricalRating
	ArticleTypeID
	MarketAudienceID
	CustodianLastEditID
	CustodianOwnerID
	CustodianLastEditIP
--->
<cfswitch expression="#attributes.fuseaction#">
	<cfcase value="ArticleManager.add">
		<h1>Add Article</h1>
 		<cfinclude template="../common/dsp/dsp_sMessage.cfm" />
		<form method="post" action="/today/admin/index.cfm">
			<input type="hidden" name="fuseaction" value="ArticleManager.addProcess" />
			<fieldset>
				<legend></legend>
				<div>Title:<br />
          <input type="text" name="Title" value="" maxlength="75" size="100"  />
        </div>
			</fieldset>
			<div>
				<input type="submit" value="save" />
			</div>
		</form>
	</cfcase>

	<cfcase value="ArticleManager.addProcess">
		<cfinclude template="sql_InsertArticleStart.cfm" />
		<cflocation url="/today/admin/index.cfm?fuseaction=ArticleManager.edit&sMessage=Please enter detailed information about this Article.&ArticleID=#ArticleID#">
	</cfcase>

	<cfcase value="ArticleManager.edit">
		<cfinclude template="dsp_editArticles.cfm" />
	</cfcase>

	<cfcase value="ArticleManager.editprocess">
		<!--- Process the edit and then redirect to categoryManager.edit --->
		<cfswitch expression="#form.sFunction#">
			<cfcase value="delete">
				<!--- Warn about deleting this data --->
				<h1>Delete Article</h1>
				<p>Are you sure you want to delete this Article?</p>
				<cfoutput>
				<p>
					<form method="post" action="/today/admin/index.cfm">
						<input type="hidden" name="fuseaction" value="ArticleManager.delete" />
						<input type="hidden" name="ArticleID" value="#ArticleID#" />
						<input type="hidden" name="Title" value="#Title#" />
						<div><input type="submit" value="delete" /></div>
					</form>
				</p>
				</cfoutput>
			</cfcase>

			<cfdefaultcase>
				<!--- Save our data and return to the edit page --->
		    <!--- store Articles "Content" field text data into a var for further processing --->
			  <cfset encodedContent = #Content#>

				<!--- Escape Special Chars for XML --->
				<cfset encodedContent = XMLFormat(#Content#)>
				<!--- Remove MS Word  non-printable ascii chars --->
				<cfset encodedContent = DeMoronize(#Content#)>

        <cfif NOT isDefined("leadphoto")>
					<cfset leadphoto = 0>
				</cfif>

        <cfif NOT isDefined("intStory")>
          <cfset intStory = 0>
        </cfif>

				<cftry>
					<cfquery datasource="#Application.PrimaryDSN#" >
					UPDATE utsaweb.dbo.articles SET Title 				= '#Title#',
					 Teaser 				= '#Teaser#',
					  Content  			= '#encodedContent#',
					  DateStart 			= '#DateStart#',
						<cfif DateExpire EQ "">
						 DateExpire	    =   NULL,
						<cfelse>
						 DateExpire	    =   '#DateExpire#',
						</cfif>
						DateLastModified 	= #DateLastModified#,
						URL 				= '#form.url#',
						CustodianLastEditID = '#CustodianLastEditID#',
						CustodianLastEditIP = '#CustodianLastEditIP#',
						Author 				= '#author#',
						Submitter			= '#submitter#',
						<cfif isdefined("form.StoryType")>
							<cfif #form.StoryType# EQ "leadPhoto">
								leadPhoto  		=  1,
							<cfelse>
								leadPhoto 		= 0,
							</cfif>
							<cfif #form.StoryType# EQ "aroundCampus">
								aroundCampus  		= 1,
							<cfelse>
								aroundCampus 		= 0,
							</cfif>
							<cfif #form.StoryType# EQ "internalStory">
								internalStory  		= 1,
							<cfelse>
								internalStory 		= 0,
							</cfif>
							<cfif #form.StoryType# EQ "moreNewsStory">
								moreNewsStory = 1,
							<cfelse>
								moreNewsStory = 0,
							</cfif>
							<cfif #form.StoryType# EQ "inTheNews">
								inTheNews = 1,
							<cfelse>
								inTheNews = 0,
							</cfif>
						</cfif>
						leadPhotoAddress 		= '#leadphotoAddress#',
						leadPhotoAddress2       = '#leadphotoAddress2#'
						WHERE ArticleID 			= #ArticleID#;
					</cfquery>
					<!--- Delete  and insert Category relationships--->
					<cfinclude template="sql_DeleteArticleCategoryAssociations.cfm">
					<cfinclude template="sql_InsertCategoryArticleRelations.cfm">

					<!--- Delete  and insert Audience relationships--->
					<cfinclude template="sql_DeleteArticleAudienceAssociations.cfm">
					<cfinclude template="sql_InsertAudienceArticleRelations.cfm">
					<h1>Article Updated</h1>
					<cfoutput><div>The article entitled <b>#Form.Title#</b> was successfully updated.</div></cfoutput>
					<cfinclude template="notify_Admins.cfm" />

					<cfcatch type="Any">
						<h1>Error While Updating</h1>
						<cfoutput>
							<div>There was an error while attempting to save your article updates. Double check your start and expiration date and please try again.</div>
							<br/>
							<p>Ask your administrator about this error:</p>
							<div>
							#cfcatch.message# - #cfcatch.type#<br/>
							#cfcatch.detail#
							</div>
						</cfoutput>
					</cfcatch>
				</cftry>
			</cfdefaultcase>
		</cfswitch>
	</cfcase>

	<cfcase value="ArticleManager.delete">
		<cfinclude template="sql_DeleteArticle.cfm">
		<h1>Article Deleted</h1>
		<cfoutput><div>The Article entitled <strong>&quot;#URLDecode(Form.title)#&quot;</strong> was successfully deleted.</div></cfoutput>
	</cfcase>
	<cfcase value="ArticleManager.controlPanel">
		<cfinclude template="dsp_controlPanel.cfm">
	</cfcase>
	<cfcase value="ArticleManager.resetToday">
		<cfinclude template="/today/_inc/v1.1/sys/generate.cfm" />
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="dsp_viewArticles.cfm">
	</cfdefaultcase>
</cfswitch>

<!--- 	<cfcase value="ArticleManager.viewSingle">
		<cfinclude template="">
</cfcase> --->
