<cfinclude template="sql_SelectOneArticle.cfm" />
<!--- DEBUG:
<cfset defaultTo = "john.garza@utsa.edu" />
      LIVE:
<cfset defaultTo = "john.garza@utsa.edu, shashi.pinheiro@utsa.edu" />
--->
<cfset defaultTo = "john.garza@utsa.edu, shashi.pinheiro@utsa.edu" />
<cfset defaultFrom = "webteam@utsa.edu" />
<cfset defaultSubject = "UTSA Today Admin Update [#ArticleID#]" />
<cfsavecontent variable="mailContent">
  <cfoutput query="SelectOneArticle">
    <table>
      <tr><th>Last Edit IP:</th><td>#cgi.Remote_Addr#</td></tr>
      <tr><th>Article added:</th><td>#DateFormat(dateAdded,"m/d/yyyy")# #timeFormat(dateAdded,"HH:mm")#</td></tr>
      <tr><th>Last modified:</th><td>#DateFormat(DateLastModified,"m/d/yyyy")# #timeFormat(DateLastModified,"HH:mm")# by #CustodianLastEditID#</td></tr>
    </table>
    <table border="0" cellpadding="10" width="90%">
      <tr>
        <td>
        <div class="adminUnit">
          <h3>Article (#ArticleID#): #title#  (<a href="https://www.utsa.edu#url#">View</a> | <a href="https://www.utsa.edu/today/admin/index.cfm?fuseaction=ArticleManager.edit&ArticleID=#ArticleID#">Edit</a>)</h3>
          <h3>URL: #url#</h3>
          <h3>Story Type:
            <cfif SelectOneArticle.leadPhoto EQ 1>Lead Photo</cfif>
            <cfif SelectOneArticle.moreNewsStory EQ 1>More News Story</cfif>
            <cfif SelectOneArticle.internalStory EQ 1>Campus Newswire Story</cfif>
            <cfif SelectOneArticle.aroundCampus EQ 1>Announcements Story</cfif>
          </h3>
          <cfset DateEmphasis = "#DateFormat(DateEmphasis,"m/d/yyyy")# #timeFormat(dateEmphasis,"HH:mm")#" />
          <cfset DateExpire = "#DateFormat(DateExpire,"m/d/yyyy")# #timeFormat(dateExpire,"HH:mm")#" />
          <h3>Start Date: &nbsp;
            <cfif DateStart is "">None Set
            <cfelse>#DateFormat(DateStart,"m/d/yyyy")# #timeFormat(DateStart,"HH:mm")#
            </cfif>
          </h3>
          <h3>Expiration Date: &nbsp;
          <cfif dateExpire is "">None Set
          <cfelse>#DateFormat(DateExpire,"m/d/yyyy")# #timeFormat(dateExpire,"HH:mm")#
          </cfif>
          </h3>

          <div><em>Teaser:</em> #Teaser#</div>
          <div><em>Author:</em> #author#</div>
          <div><em>Submitter Dept/College/Office:</em> #submitter#</div>
          <div><em>Content:</em><br />
            <div>#Content#</div>
          </div>
          <div id="leadPhotoAddressText">Image Address (required for Lead and More stories):<br />
            #leadPhotoAddress#<br/>
            <img src="http://www.utsa.edu#leadPhotoAddress#" alt="" name="buttonOne"  />
          </div>

          <div id="leadPhotoAddressText2">Image Address Landscape orientation for Home page(required for Lead and More stories):<br />
            #leadPhotoAddress2#<br/>
            <img src="http://utsa.edu#leadPhotoAddress2#" alt="" name="buttonOne"  />
          </div>
        </div>

      </td></tr>
    </table>
  </cfoutput>
</cfsavecontent>

<cfmail to="#defaultTo#" from="#defaultFrom#" subject="#defaultSubject#" type="html">#mailContent#</cfmail>
