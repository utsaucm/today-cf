<cfSwitch expression="#attributes.fuseaction#">
	<cfCase value="RSSGenerator.all">
		<!--- Do the top stories --->
		<cfinclude template="/today/_inc/v1.1/sys/generateRSS.cfm" />
		<cfinclude template="/today/_inc/v1.1/sys/generateAcademicCalendarRSS.cfm" />
		<!--- events deprecated? --->
		<!---
			<cfinclude template="act_generateEventsCalendarRSS.cfm">
		--->
	</cfcase>
</cfswitch>
