<cfscript>
/**
 * Returns the index of the first item in an array that contains a specified substring.
 * Mods by RCamden
 * 
 * @param arrayToSearch  Array to search. (Required)
 * @param valueToFind  Value to look for. (Required)
 * @return Returns a number. 
 * @author Sudhir Duddella (skduddella@hotmail.com) 
 * @version 1, March 31, 2003 
 */
function ArrayContainsNoCase(arrayToSearch,valueToFind){
	var arrayList = "";
	arrayList = ArrayToList(arrayToSearch);
	return ListContainsNoCase(arrayList,valueToFind);
}
</cfscript>