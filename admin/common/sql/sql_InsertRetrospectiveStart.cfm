<cfquery datasource="#Application.primaryDSN#" name="InsertRetrospectiveStart">
SET NOCOUNT ON
INSERT INTO Retrospectives (RetrospectiveDate)
VALUES ('#RetrospectiveDate#')
SELECT ThisID = @@Identity
SET NOCOUNT OFF
</cfquery>