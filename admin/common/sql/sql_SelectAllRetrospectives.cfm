<cfIF isdefined("url.sqlSort") is False>
	<cfset sqlSort = "RetrospectiveID">
</cfif>
<cfIf isdefined("url.sAscDesc") is False>
	<cfset sAscDesc = "asc">
</cfif>
<cfQuery name="SelectAllRetrospectives" datasource="#Application.primaryDSN#">
Select * from Retrospectives
--left join custodians
--on custodianID=primaryCustodianID
Order by #sqlSort# #sAscDesc#

</cfquery>