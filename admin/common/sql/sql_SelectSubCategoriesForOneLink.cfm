<cfQuery name="SelectSubCategoriesForOneLink" datasource="#Application.primaryDSN#">
Select subCategories.subcategoryName, subCategories.subCategoryID
	FROM subCategories 
	WHERE subCategories.subCategoryID not in (#subCategoryIDs#)
	ORDER BY subCategories.subCategoryName
</cfquery>