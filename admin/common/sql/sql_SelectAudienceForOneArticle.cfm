<cfQuery name="SelectAudienceForOneArticle" datasource="#Application.primaryDSN#">
Select AudienceArticleRelations.ArticleID, Audiences.AudienceName, Audiences.AudienceID
	FROM AudienceArticleRelations, Audiences 
	WHERE Audiences.AudienceID=AudienceArticleRelations.AudienceID
		and AudienceArticleRelations.ArticleID in (#ArticleID#)
</cfquery>