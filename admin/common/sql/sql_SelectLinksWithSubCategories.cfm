<cfIF isdefined("url.sqlSort") is False>
	<cfset sqlSort = "LinkID">
</cfif>
<cfIf isdefined("url.sAscDesc") is False>
	<cfset sAscDesc = "asc">
</cfif>
<cfQuery name="SelectLinksWithSubCategories" datasource="#Application.primaryDSN#">
select links.*, subCategoryName, Link_Name 
	from links
	left join subCategoryLinkRelations on links.linkid = subCategoryLinkRelations.linkid
	left join subCategories on subCategories.subCategoryID = subCategoryLinkRelations.subCategoryID
	order by subCategoryName, links.linkid

</cfquery>