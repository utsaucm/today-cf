<cfIF isdefined("form.CategoryID")>
	<cfscript>
		//Prepare our update query
		iCounter = 1;
		sSQL = "";
		DO {
			sSQL = sSQL & "Insert into CategoryLinkRelations (CategoryID, LinkID) values(" & listGetAt(form.CategoryID,iCounter) & "," & form.LinkID & ")";	
			iCounter = iCounter + 1;
		}WHILE (iCounter LT ListLen(Form.CategoryID)+1);
		writeOutPut(sSQL & "<br />");
	</cfscript>
	<cfQuery name="UpdateCategoryLinkRelations" datasource="#Application.primaryDSN#">
		#sSQL#
	</cfquery>
<cfELSE>
	<!-- 	Category did not receive any categories.
			This means that some items can avoid having a category associated with them
	-->
</cfif>
