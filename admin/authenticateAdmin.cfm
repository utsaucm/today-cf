<cfif NOT isDefined(j_username) OR NOT isDefined(j_password)>
	<cfif Len(trim(j_username)) LT 1 AND Len(trim(j_password)) LT 1>
        <cflocation url="login.cfm?fail=yes" addtoken="yes">
    </cfif>
</cfif>


<!--- /////////////////////// SCRIPT Params /////////////////////////--->
<cfparam name="submit" default="no">
<cfparam name="DC1" default="UTSARR">
<cfparam name="DC2" default="NET">
<cfparam name="OU1" default="People">
<cfparam name="OU2" default="Administrative Computing Services">

<!--- Create Session Variables --->
<!---<cflock scope="session" timeout="20" type="Exclusive">
	<cfset Session.Username = form.expUserName>
	<cfset Session.Password = form.expPassword>
</cflock>

<cflock scope="session" timeout="20" type="readonly">
	<cfquery name="auth" datasource="experts">
		SELECT  usrname, passwd
		FROM [Experts].[dbo].[Admins]
		WHERE usrname = <cfqueryparam value="#Session.Username#" cfsqltype="cf_sql_char"> AND
		      passwd = <cfqueryparam value="#Session.Password#" cfsqltype="cf_sql_char">;
	</cfquery>
</cflock>--->

<!--- DEBUG ONLY
<cfoutput>
	#Session.Username#
	#Session.Password#
	#auth.RecordCount#
</cfoutput>
<cfabort>
--->

<cftry>
    <cfldap
            server="BUSH1604.UTSARR.NET"
            action="query"
            name="res"
            scope="onelevel"
            attributes="sAMAccountName,description,givenName"
						filter="(&(sAMAccountName=#form.j_username#)(department=UNIVERSITY COMM AND MKTG))"
            start="OU=#OU1#,OU=#OU2#,DC=#DC1#,DC=#DC2#"
            username="#form.j_username#@utsarr.net"
            password="#form.j_password#"
    />
	<cfcatch type="any">
        <cflocation url = "login.cfm?fail=yes" addtoken="no">
    </cfcatch>
</cftry>

<!--- Create Session Variables --->
<cflock scope="session" timeout="20" type="Exclusive">
	<cfset Session.Username = res.givenName>
</cflock>

<!--- Authenticate user here --->
<cfif #res.recordcount# EQ 0>
	<cflocation url = "login.cfm?fail=yes" addtoken="no">
<cfelse>
    <cflocation url="index.cfm" addtoken="no">
</cfif>
