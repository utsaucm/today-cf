<!---
Listing of fields:
	ID
	SUBMIT_DATE_TIME
	TYPE
	TITLE
	TEASER
	TEASER_URL
	RESOURCE_URL
	ACTIVE
	START_DATE_TIME
	EXPIRE_DATE_TIME
--->
<cfset creationDate = now() />
<cfquery datasource="#Application.primaryDSN#" name="InsertArticleStart">
	INSERT INTO Spotlight (TITLE,SUBMIT_DATE_TIME)
	VALUES (
		<cfqueryparam value="#TITLE#" cfsqltype="CF_SQL_CHAR" />,
		<cfqueryparam value="#creationDate#" cfsqltype="CF_SQL_TIMESTAMP"/>
	)
</cfquery>
