<cfinclude template="sql_SelectOneSpotlight.cfm" />
<cfset defaultTo = Application.ADMIN_NOTIFY />
<cfset defaultFrom = "web.team@utsa.edu" />
<cfset defaultSubject = "UTSA Today Admin Update [#ID#]" />
<cfsavecontent variable="mailContent">
  <cfoutput query="SelectOneSpotlight">
    <table>
      <tr><th>Last Edit IP:</th><td>#cgi.Remote_Addr#</td></tr>
      <tr><th>Article added:</th><td>#DateFormat(SUBMIT_DATE_TIME,"m/d/yyyy")# #timeFormat(SUBMIT_DATE_TIME,"HH:mm")#</td></tr>
    </table>
    <table border="0" cellpadding="10" width="90%">
      <tr>
        <td>
        <div class="adminUnit">
          <h3>Spotlight (#ID#) - #TYPE# : #TITLE#  (<a href="https://www.utsa.edu#TEASER_URL#">View</a> | <a href="https://www.utsa.edu/today/admin/index.cfm?fuseaction=SpotlightManager.edit&ID=#ID#">Edit</a>)</h3>
          <h3>Teaser: #TEASER#</h3>
          <h3>Teaser URL: #TEASER_URL#</h3>
          <h3>Resource URL: #RESOURCE_URL# <img src="https://www.utsa.edu#RESOURCE_URL#"/></h3>
          <h3>Start Date: &nbsp;
            <cfif START_DATE_TIME is "">None Set
            <cfelse>#DateFormat(START_DATE_TIME,"m/d/yyyy")# #timeFormat(START_DATE_TIME,"HH:mm")#
            </cfif>
          </h3>
          <h3>Expiration Date: &nbsp;
          <cfif EXPIRE_DATE_TIME is "">None Set
          <cfelse>#DateFormat(EXPIRE_DATE_TIME,"m/d/yyyy")# #timeFormat(EXPIRE_DATE_TIME,"HH:mm")#
          </cfif>
          </h3>
        </div>
        </td>
      </tr>
    </table>
  </cfoutput>
</cfsavecontent>

<cfmail to="#defaultTo#" from="#defaultFrom#" subject="#defaultSubject#" type="html">#mailContent#</cfmail>
