<!--- Include a UDF used to help remove ascii non-printable chars from an MS Word source document --->
<cfinclude template="DeMoronize.cfm">
<!---
	These are the Database fields that I will be using.
	ID
	SUBMIT_DATE_TIME
	TYPE
	TITLE
	TEASER
	TEASER_URL
	RESOURCE_URL  (this is an image url)
	ACTIVE
	START_DATE_TIME
	EXPIRE_DATE_TIME
--->
<cfswitch expression="#attributes.fuseaction#">
	<cfcase value="SpotlightManager.add">
		<h1>Add Spotlight</h1>
 		<cfinclude template="../common/dsp/dsp_sMessage.cfm" />
		<form method="post" action="/today/admin/index.cfm">
			<input type="hidden" name="fuseaction" value="SpotlightManager.addProcess" />
			<fieldset>
				<legend></legend>
				<div>Title:<br />
          <input type="text" name="Title" value="" maxlength="75" size="100"  />
        </div>
			</fieldset>
			<div>
				<input type="submit" value="save" />
			</div>
		</form>
	</cfcase>

	<cfcase value="SpotlightManager.addProcess">
		<cfinclude template="sql_InsertSpotlightStart.cfm" />
		<cflocation url="/today/admin/index.cfm?fuseaction=spotlightManager.view&sMessage=New Spotlight Created">
	</cfcase>

	<cfcase value="SpotlightManager.edit">
		<cfinclude template="dsp_editSpotlights.cfm" />
	</cfcase>

	<cfcase value="SpotlightManager.editprocess">
		<!--- Process the edit and then redirect to categoryManager.edit --->
		<cfswitch expression="#form.sFunction#">
			<cfcase value="delete">
				<div class="container">
				<!--- Warn about deleting this data --->
				<h1>Delete Spotlight</h1>
				<p>Are you sure you want to delete this Spotlight?</p>
				<cfoutput>
				<section>
					<form method="post" action="/today/admin/index.cfm">
						<div class="form-group">
							<input type="hidden" name="fuseaction" value="SpotlightManager.delete" />
							<input type="hidden" name="ID" value="#ID#" />
							<input type="hidden" name="Title" value="#Title#" />
							<input class="btn btn-primary" type="submit" value="delete"/>
						</div>
					</form>
				</section>
				</div>
				</cfoutput>
			</cfcase>

			<cfdefaultcase>
        <cfquery datasource="#Application.PrimaryDSN#" dbtype="oledb">
					UPDATE Spotlight SET
					TYPE					= '#Type#',
					TITLE					= '#Title#',
					TEASER 				= '#Teaser#',
					TEASER_URL		= '#TEASER_URL#',
					RESOURCE_URL	= '#RESOURCE_URL#',
					<cfif isDefined("ACTIVE")>
						<cfif ACTIVE EQ "">
							ACTIVE = NULL,
						<cfelse>
							ACTIVE = '#ACTIVE#',
						</cfif>
					</cfif>
					<cfif EXPIRE_DATE_TIME EQ "">
						 EXPIRE_DATE_TIME	    =   NULL,
					<cfelse>
						 EXPIRE_DATE_TIME	    =   '#EXPIRE_DATE_TIME#',
					</cfif>
					<cfif START_DATE_TIME EQ "">
						 START_DATE_TIME	    =   NULL
					<cfelse>
						 START_DATE_TIME	    =   '#START_DATE_TIME#'
					</cfif>
					WHERE ID = <cfqueryparam value="#ID#" cfsqltype="cf_sql_integer" maxlength="15">
				</cfquery>
				<h1>Spotlight Updated</h1>
				<cfoutput><div>The spotlight entitled <b>#Form.Title#</b> was successfully updated.</div></cfoutput>
				<cfinclude template="notify_Admins.cfm" />
			</cfdefaultcase>
		</cfswitch>
	</cfcase>

	<cfcase value="SpotlightManager.delete">
		<cfinclude template="sql_DeleteSpotlight.cfm">
		<h1>Spotlight Deleted</h1>
		<cfoutput><div>The Spotlight entitled <strong>&quot;#URLDecode(Form.title)#&quot;</strong> was successfully deleted.</div></cfoutput>
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="dsp_viewSpotlights.cfm">
	</cfdefaultcase>
</cfswitch>
