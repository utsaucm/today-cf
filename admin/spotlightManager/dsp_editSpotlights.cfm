<h1>Edit Spotlight</h1>
<cfinclude template="../common/dsp/dsp_sMessage.cfm">
	<cfinclude template="sql_SelectOneSpotlight.cfm"/>
	<cfoutput QUERY="SelectOneSpotlight">
		<h2>Preview</h2>

<div class="row">
	<div class="col-md-8">
		<div class="utsaToday-MoreNews">
			<a href="#TEASER_URL#">
			<div class="row">
				<div class="col-md-2">
					<img src="#RESOURCE_URL#"  alt="#CleanHighAscii(TITLE)#"/>
				</div>
				<div class="col-md-10">
					<span class="spotlightblue">#CleanHighAscii(TEASER)#</span>
					<p class="spotlightfrom">From #TYPE#</p>
				</div>
			</div>
			</a>
		</div>
	</div>
	<div class="col-md-4">&nbsp;</div>
</div>

<div class="row">
		<section>
    <form method="post" action="/today/admin/index.cfm" id="editSpotlightForm" name="editSpotlightForm">
			<fieldset class="form-group">
				<legend>Information</legend>
				<input type="hidden" name="fuseaction" value="SpotlightManager.editProcess" />
				<input type="hidden" name="ID" value="#ID#" />
				<input type="hidden" name="CustodianLastEditIP" value="#cgi.Remote_Addr#" />
				<input type="hidden" name="CustodianLastEditID" value="#GetAuthUser()#" />
				<input type="hidden" name="DateLastModified" value="#now()#" />
				<div class="form-group">
					<label for="TEASER">Teaser headline, displayed on the side of the image:</label>
					<input id="TEASER" class="form-control" type="text" maxlength="250" length="250" name="TEASER" value="#TEASER#" />
					<small id="emailHelp" class="form-text text-muted">
						Headline is displayed immediately to the right of the image, max: 250 characters
					</small>
				</div>
				<div class="form-group">
					<label for="TYPE">Source:</label>
          <input class="form-control" type="text" name="TYPE" value="#TYPE#" id="TYPE" />
					<small id="emailHelp" class="form-text text-muted">Source of spotlight, automatically prefixed with 'From': ("Sombrilla")</small>
				</div>
				<div class="form-group">
					<label for="TEASER_URL">Spotlight Permalink URL:</label>
					<input id="TEASER_URL" class="form-control" type="text"  maxlength="120" length="120" name="TEASER_URL" value="#SelectOneSpotlight.TEASER_URL[SelectOneSpotlight.currentRow]#" />
					<small id="emailHelp" class="form-text text-muted">
						Relative to www.utsa.edu: (/sombrilla/fall2016/story/feature-witte.html)
					</small>
				</div>
				<div class="form-group">
					<label for="RESOURCE_URL">Spotlight Image URL:</label>
					<input id="RESOURCE_URL" class="form-control" type="text"  maxlength="120" length="120" name="RESOURCE_URL" value="#SelectOneSpotlight.RESOURCE_URL[SelectOneSpotlight.currentRow]#" />
					<small id="emailHelp" class="form-text text-muted">
						Relative to www.utsa.edu: (/today/img/placeholder-secondary.png). Recommended size for image is 335px (width) x 280px (height).
					</small>
				</div>
				<div class="form-group">
					<label for="TITLE">Spotlight Image ALT text:</label>
					<input class="form-control" type="text" name="TITLE"  maxlength="120" length="120" value="#TITLE#" id="TITLE" />
					<small id="emailHelp" class="form-text text-muted">Required for screen readers and low bandwidth devices</small>
				</div>
			</fieldset>
			<fieldset class="form-group">
				<legend>Activation and Dates</legend>
				<div class="form-group">
					<cfset DateStart = "#DateFormat(START_DATE_TIME,"m/d/yyyy")# #timeFormat(START_DATE_TIME,"HH:mm")#">
					<label for="START_DATE_TIME">Start Date:</label>
					<cfif DateStart is " ">
						<input id="START_DATE_TIME" class="form-control" type="text" name="START_DATE_TIME" value=""/>
					<cfelse>
						<input id="START_DATE_TIME" class="form-control" type="text" name="START_DATE_TIME" value="#DateFormat(START_DATE_TIME,"m/d/yyyy")# #timeFormat(START_DATE_TIME,"HH:mm")#" />
					</cfif>
					<small id="emailHelp" class="form-text text-muted">
						Even if activation is set to 'YES', this spotlight will not appear until after this start date.  Also used for controlling order. (Example - 9/12/2005 14:06)
					</small>
				</div>

				<div class="form-group">
					<cfset DateExpire = "#DateFormat(EXPIRE_DATE_TIME,"m/d/yyyy")# #timeFormat(EXPIRE_DATE_TIME,"HH:mm")#">
					<label for="EXPIRE_DATE_TIME">Expiration Date:</label>
					<cfif DateExpire is " ">
						<input id="EXPIRE_DATE_TIME" class="form-control" type="text" name="EXPIRE_DATE_TIME" value=""/>
					<cfelse>
						<input id="EXPIRE_DATE_TIME" class="form-control" type="text" name="EXPIRE_DATE_TIME" value="#DateFormat(EXPIRE_DATE_TIME,"m/d/yyyy")# #timeFormat(EXPIRE_DATE_TIME,"HH:mm")#" />
					</cfif>
					<small id="emailHelp" class="form-text text-muted">
						After this date, this spotlight light will no longer appear, even if activation is set to 'YES'. (Example - 9/12/2005 14:06)
					</small>
				</div>

				<div class="form-group">
					<label for="ACTIVATION">Activation:</label>
					<label class="form-check-inline">
					  <input class="form-check-input" type="radio" name="ACTIVE" id="ACTIVE_YES" value="YES" <cfif ACTIVE EQ 'YES'>checked="checked"</cfif> /> YES
					</label>
					<label class="form-check-inline">
						<input class="form-check-input" type="radio" name="ACTIVE" id="ACTIVE_NO" value="NO" <cfif ACTIVE EQ 'NO'>checked="checked"</cfif> /> NO
					</label>
					<small id="activeHelp" class="form-text text-muted">
						Only spotlights with activation set to 'YES' will be displayed.
					</small>
				</div>
			</fieldset>

			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="sFunction" value="save" />
				<input type="submit" class="btn" value="delete" name="sFunction" />
			</div>
		</form>
	</section>
	<hr>
	<section>
		<div style="color:##666666">Spotlight added: #DateFormat(SUBMIT_DATE_TIME,"mm/dd/yyyy")#, #timeFormat(SUBMIT_DATE_TIME,"HH:mm")#</div>
	</section>
</div>
	</cfoutput>
