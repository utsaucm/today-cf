		<h1>View Spotlights</h1>
		<!--- open query --->
		<cfinclude template="sql_SelectAllSpotlights.cfm" />
		<cfset sQueryName=SelectAllSpotlights />
		<cfset currentID = 0 />
		<cfset prevID = 0 />
		<cfset currentTS = now() />
		<cfset previousTS = now() />
		<cfinclude template="../common/act/act_SetDatabasePaging.cfm" />

		<cfoutput>
			<div>#SelectAllSpotlights.RecordCount# total records.</div>
		</cfoutput>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm" />
		<cfinclude template="../common/act/act_WriteTableSortLinkFunction.cfm" />
		<div class="table-responsive">
		<table class="admin table"  border="0">
			<cfoutput>
			<tr>
				<th>#WriteTableSortLink("ID","id")#</th>
				<th>#WriteTableSortLink("Source","TYPE")#</th>
				<th>#WriteTableSortLink("Teaser Headline","TEASER")#</th>
				<th>#WriteTableSortLink("Teaser URL","TEASER_URL")#</th>
				<th>#WriteTableSortLink("IMG URL","RESOURCE_URL")#</th>
				<th>#WriteTableSortLink("IMG ALT","TITLE")#</th>
				<th>#WriteTableSortLink("Active","ACTIVE")#</th>
				<th>#WriteTableSortLink("Start","START_DATE_TIME")#</th>
				<th>#WriteTableSortLink("Expires","EXPIRE_DATE_TIME")#</th>
        <th>#WriteTableSortLink("Date Added","SUBMIT_DATE_TIME")#</th>
				<cfif (ListFind(request.attributeslist, "sqlSort=START_DATE_TIME", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<th>Order</th>
				</cfif>
			</tr>
      </cfoutput>
			<cfoutput query="SelectAllSpotlights" StartRow="#iStartRow#" maxrows="#iMaxRow#">
			<cfset currentTS = START_DATE_TIME />
			<cfset rankLink = "/today/admin/index.cfm?fuseaction=SpotlightManager.view&sqlSort=START_DATE_TIME&sAscDesc=desc&" />
			<cfif currentrow NEQ 1>
				<cfif (ListFind(request.attributeslist, "sqlSort=START_DATE_TIME", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<cfif DateCompare(previousTS, now()) EQ -1>
					<a href="#rankLink#rank=down&NextID=#id#&id=#prevID#">&darr;</a>
				</cfif>
				</cfif>
				</td>
			</tr>
			</cfif>
			<tr class="#iif(currentrow MOD 2,DE('row1'),DE('row2'))#">
				<td><a href="/today/admin/index.cfm?fuseaction=SpotlightManager.edit&id=#id#">#id#</a></td>
				<td>#TYPE#</td>
				<td>#TEASER#</td>
				<td><a href="#TEASER_URL#">view</a></td>
				<td><img src="#RESOURCE_URL#"/></td>
				<td>#TITLE#</td>
				<td>#ACTIVE#</td>
				<td>#(Len(START_DATE_TIME) ? "#DateFormat(START_DATE_TIME,"mm/dd/yy")# #TimeFormat(START_DATE_TIME, "short")#" : "-----")#</td>
				<td>#(Len(EXPIRE_DATE_TIME) ? "#DateFormat(EXPIRE_DATE_TIME,"mm/dd/yy")# #TimeFormat(EXPIRE_DATE_TIME, "short")#" : "-----")#</td>
				<td>#(Len(SUBMIT_DATE_TIME) ? "#DateFormat(SUBMIT_DATE_TIME,"mm/dd/yy")# #TimeFormat(SUBMIT_DATE_TIME, "short")#" : "-----")#</td>
				<td>
				<cfif (ListFind(request.attributeslist, "sqlSort=START_DATE_TIME", "&") GT 0) AND (ListFind(request.attributeslist, "sAscDesc=desc", "&") GT 0)>
				<cfif currentrow NEQ 1>
					<cfif (DateCompare(previousTS, now()) EQ -1) AND (DateCompare(currentTS, now()) EQ -1)>
					<a href="#rankLink#rank=up&previousID=#prevID#&id=#id#">&uarr;</a>
					</cfif>
				</cfif>
				</cfif>
				<cfset prevID = id />
				<cfset previousTS = START_DATE_TIME />
			</cfoutput>
			<!--- always end the table --->
			</td></tr>
		</table>
		</div>
		<cfinclude template="../common/dsp/dsp_DatabasePagingButtons.cfm">
