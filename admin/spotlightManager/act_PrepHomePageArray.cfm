<!--- DSN connection --->
<cfset request.PrimaryDSN="utsaweb-syntaxprod">

<!--- the main article loop counter ---> 
<cfset iArticleCounter = 1>

<!--- updated by Ruben O. on 9-9-09 --->
<!--- flag var, gets set to 1 when find the first lead photo article is found --->
<cfset iLeadphoto = 0>

<!--- updated by Ruben O. on 9-9-09 --->
<!--- flag var2, gets set to 1 when find the second lead photo article is found --->
<cfset iLeadphoto2 = 0>

<!--- updated by Ruben O. on 9-9-09 --->
<!--- flag var3, gets set to 1 when find the third lead photo article is found --->
<cfset iLeadphoto3 = 0>

<!--- accumulators used for categories --->
<cfset iMoreStories = 0>
<cfset iTopStories = 0>

<!--- updated by Ruben O. on 9-9-09 --->
<!--- accumulators used for external and internal categories --->
<cfset extStories = 0>
<cfset intStories = 0>

<!--- index pointers for each int/ext category --->
<cfset extIndex = 4>
<cfset intIndex = 20>

<!--- aStories loop index initialized to 2 b/c the lead photo article info. is stored in first array index position --->
<!--- <cfset iCounter = 2> --->

<!--- updated by Ruben O. on 9-9-09 --->
<!--- aStories loop index initialized to 4 b/c the lead photo articles info. is stored in first 3 array index positions --->
<cfset iCounter = 4>

<!--- not used? --->
<cfset iCount= 2>

<!--- article 2D array used to display articles on UTSA Today home page --->
<cfset aStories=ArrayNew(2)>

<!--- Added by Ruben O. on 10-29-2009 initialize the aStories 2D array with empty string values to avoid array bound exceptions --->
<cfloop index="i" from="1" to="29">
	<cfloop index="j" from="1" to="21">
		<cfset aStories[i][j] = "">
	</cfloop>
</cfloop>

<!--- include SQL query: SELECT Top 80 * FROM Articles table ORDER DESC --->
<cfinclude template="sql_SelectControlPanelArticles.cfm">

<!--- run the query --->
<cfoutput query="SelectControlPanelArticles">

<!--- include the category code --->
<cfinclude template="act_CategorizeStories.cfm">

<!--- updated by Ruben O. on 9-9-09
Story Status:
0 - Future Stories (unlimited)
1 - Lead Photo (1)
20 - Lead Photo2 (1) 
30 - Lead Photo3 (1)  
2 - Top Stories (4)
3 - More Stories (6)
4 - Expired (unlimited)
5 - Archives (unlimited) 
--->

	<!--- Make updates to the Array --->
	<!---<p>#articleid# - #title# - #teaser# - #datestart# - #url#</p>	 --->

	<cfif (storyStatus is 4) or (storystatus is 5) or (storystatus is 0)>
		<!--- skip: do not add to the aStories array for display) --->
	
	<cfelseif (storyStatus is 1)>		
		<!--- Set Lead photo to the first slot--->			
			<cfset aStories[1][1] = ArticleID>
			<cfset aStories[1][2] = title>
			<cfset aStories[1][3] = teaser>
			<cfset aStories[1][4] = datestart>
			<cfset aStories[1][5] = url>
			<cfset aStories[1][6] = leadphotoAddress>
			<cfset aStories[1][7] = leadPhoto>
			<cfset aStories[1][8] = internalStory>			
			<!--- Let the rest of the code know that the lead photo has run --->
			<cfset iLeadPhoto = 1>

    <cfelseif (storyStatus is 20)>		
		<!--- Set Lead photo to the second slot--->			
			<cfset aStories[2][1] = ArticleID>
			<cfset aStories[2][2] = title>
			<cfset aStories[2][3] = teaser>
			<cfset aStories[2][4] = datestart>
			<cfset aStories[2][5] = url>
			<cfset aStories[2][6] = leadphotoAddress>	
			<cfset aStories[2][7] = leadPhoto>
			<cfset aStories[2][8] = internalStory>		
			<!--- Let the rest of the code know that the lead photo has run --->
			<cfset iLeadPhoto2 = 1>			
			
	 <cfelseif (storyStatus is 30)>		
		<!--- Set Lead photo to the third slot--->			
			<cfset aStories[3][1] = ArticleID>
			<cfset aStories[3][2] = title>
			<cfset aStories[3][3] = teaser>
			<cfset aStories[3][4] = datestart>
			<cfset aStories[3][5] = url>
			<cfset aStories[3][6] = leadphotoAddress>
			<cfset aStories[3][7] = leadPhoto>
			<cfset aStories[3][8] = internalStory>			
			<!--- Let the rest of the code know that the lead photo has run --->
			<cfset iLeadPhoto3 = 1>	
			
	<!--- Top External Stories: storyStatus is 2 --->
<!---	<cfelseif (storyStatus is 2) and extStories less than 20 and extIndex less than 20>--->
	<cfelseif (storyStatus is 2) and extStories less than 17 and extIndex less than 20>
	        
	        <!--- <cfoutput>WriteOutput("StoryStatus is 2 <Br />");</cfoutput>	 --->
    		<cfset aStories[extIndex][1] = ArticleID>
			<cfset aStories[extIndex][2] = title>
			<cfset aStories[extIndex][3] = teaser>
			<cfset aStories[extIndex][4] = datestart>
			<cfset aStories[extIndex][5] = url>
			<cfset aStories[extIndex][6] = leadphotoAddress>
			<cfset aStories[extIndex][7] = leadPhoto>
			<cfset aStories[extIndex][8] = internalStory>	
			<cfset icounter = icounter + 1>
			<cfset extIndex = extIndex + 1>		
	
	<!--- Top Internal Stories: storyStatus is 3 --->
	<!--- <cfelseif (storyStatus is 3) and intStories less than 7 and intIndex less than 26> --->
<!---    <cfelseif (storyStatus is 3) and intStories less than 10 and intIndex less than 29>--->	
<cfelseif (storyStatus is 3) and intStories less than 11 and intIndex less than 30> 
    
    	 
    		<!--- <cfoutput>WriteOutput("StoryStatus is 3 <Br />");</cfoutput> --->
    		<cfset aStories[intIndex][1] = ArticleID>
			<cfset aStories[intIndex][2] = title>
			<cfset aStories[intIndex][3] = teaser>
			<cfset aStories[intIndex][4] = datestart>
			<cfset aStories[intIndex][5] = url>
			<cfset aStories[intIndex][6] = leadphotoAddress>
			<cfset aStories[intIndex][7] = leadPhoto>
			<cfset aStories[intIndex][8] = internalStory>	
			<cfset icounter = icounter + 1>
			<cfset intIndex = intIndex + 1>		
	</cfif>
</cfoutput>


<!--- ********************** OLD CODE ********************************** --->
<!---
	Top/More Stories: storyStatus is 2 or 3 
	<cfelse> 
    		<cfset aStories[icounter][1] = ArticleID>
			<cfset aStories[icounter][2] = title>
			<cfset aStories[icounter][3] = teaser>
			<cfset aStories[icounter][4] = datestart>
			<cfset aStories[icounter][5] = url>
			<cfset aStories[icounter][6] = leadphotoAddress>
			<cfset  aStories[icounter][7] = leadPhoto>
			<cfset icounter = icounter + 1>
	</cfif>
--->