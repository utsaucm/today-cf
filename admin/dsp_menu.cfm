<cfoutput>
  <div class="adminMenu">
    <div>
      <strong>Articles:</strong>
      <a href="/today/admin/index.cfm?fuseaction=ArticleManager.controlPanel">Control panel</a> |
      <a href="/today/admin/index.cfm?fuseaction=articleManager.view">View All</a> |
      <a href="/today/admin/index.cfm?fuseaction=articleManager.add">Add</a>
    </div>
    <div>
      <strong>Spotlights:</strong>
      <a href="/today/admin/index.cfm?fuseaction=spotlightManager.view">View All</a> |
      <a href="/today/admin/index.cfm?fuseaction=spotlightManager.add">Add</a>
    </div>
    <div>
      <strong>Categories:</strong>
      <a href="/today/admin/index.cfm?fuseaction=categoryManager.view">View</a> |
      <a href="/today/admin/index.cfm?fuseaction=categoryManager.add">Add</a>
    </div>
    <div>
      <strong>RSS:</strong>
      <a href="/today/admin/index.cfm?fuseaction=RSSGenerator.all">Generate All</a>
    </div>
    <div>
      <strong>UTSA Today Reset:</strong>
      <a href="/today/admin/index.cfm?fuseaction=articleManager.resetToday">Reset</a>
    </div>

    <div style="color:##999999">You are logged in as #GetAuthUser()#.</div>
  </div>
</cfoutput>
