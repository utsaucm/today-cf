<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

<body onLoad="form.uname.focus()">
<h3>Login</h3>

<!---These set the session variables back to their defaults--->
<cfset session.MM_Username="">
<cfset session.MM_UserAuthorization="">
<cfset session.EmailAddress="">
<cfset session.Name="">
<cfset session.LoggedIn="False">
<!---This will create the LoginTest variable in the session and set its default to True--->
<cfparam name="session.LoginTest" default="True">
<!---A quick check to see if the form username&ID were invalid and prints to the screen an error message
	based on session.LoginTest but is returned after the form is processed--->
<cfif session.LoginTest EQ "False">
		<font color="#990000"><center><cfoutput>Please enter a valid username/password combination.</cfoutput></center></font>
</cfif>
<!--- Do the following if the form is submitted. SELECT PasswordHash--->
<!-- /**/-->
<cfif IsDefined("FORM.uname")>
   <!--- query the data base. ---> 
   <cfquery name="MM_rsUser" datasource="#session.dbDSN#">
  	  SELECT UserID, Password, ProfileID, FirstName, LastName, EmailAddress 
  	
      
      FROM tblUserInfo
      
	  
	  WHERE UserID='#FORM.uname#' AND Password='#FORM.pass#' 
         cfsqltype = "CF_SQL_VARCHAR"> 
   </cfquery>
<!---Check to see if a Record is retrieved by the query--->
	<cfif MM_rsUser.RecordCount NEQ 0>
		<!---Check to see if the user is an administrator or developer--->
    	<cfif MM_rsUser.ProfileID EQ 1>
			<!---Sets the redirect variable to the Administrative Menu Page if the ProfileID is 1--->
  			<cfset MM_redirectLoginSuccess="/today/admin/index.cfm"><!--AdministratorMenu.cfm"-->
		<cfelse>
			<!---Sets the redirect variable to the Developer Menu Page if the ProfileID is not 1--->
			<cfset MM_redirectLoginSuccess="/today/admin/index.cfm"><!--DeveloperMenu.cfm"-->
  		</cfif>
		<!---Sets the Session variables MM_Username/MM_UserAuthorization/LoggedIn--->
      	<cflock scope="Session" timeout="10">
	        <cfset session.MM_Username=FORM.uname>
    	    <cfset session.MM_UserAuthorization=MM_rsUser.ProfileID>
			<cfset session.Name="#MM_rsUser.FirstName# #MM_rsUser.LastName#">
			<cfset session.emailAddress="#MM_rsUser.EmailAddress#">
			<cfset session.LoggedIn="True">
      	</cflock>
		<!---Redirects the user to the successful login page--->
      <cflocation url="#MM_redirectLoginSuccess#" addtoken="no">
  	</cfif>
	<!---Sets the LoginTest variable to "False" this will be used to print the error message
		and sends the user to the UserLogin Page--->
  	<cfset session.LoginTest="False">
	<cflocation url="h2.cfm" addtoken="no">
  	<cfelse>
	<!---Macromedia script used to run Login code--->
  	<cfset MM_LoginAction=CGI.SCRIPT_NAME>
  		<cfif CGI.QUERY_STRING NEQ "">
    		<cfset MM_LoginAction=MM_LoginAction & "?" & CGI.QUERY_STRING>
  		</cfif>
</cfif>

<!--- <cfif IsDefined("Form.UserID")>Form for entering ID WHERE UserID = <cfqueryparam value = '#UserID#'and password. --->
<form action="#MM_loginAction#" method="post">
   <b>User ID: </b>
   <input type = "text" name="UserID" ><br>
   <b>Password: </b>
   <input type = "text" name="password" ><br><br>
   <input type = "Submit" value = "Encrypt my String">
</form>
</body>
</html>
