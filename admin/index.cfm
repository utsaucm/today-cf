<cf_FormURL2Attributes />
<cfinclude template="/today/_inc/v1.1/cfm/today-header.cfm">

<title>UTSA Today ADMIN | UTSA | The University of Texas at San Antonio</title>
<cfinclude template="/today/_inc/v1.1/cfm/today-header-after-title.cfm">
  <link rel="stylesheet" type="text/css" href="today-admin.css" />
</head>
<body>
<cfinclude template="/_files/global-header.html" />

<section>
<div class="container">
	<div class="row">
    <br/><br/>

    <cfparam Name="attributes.fuseaction" Default="linkManager.view" />
    <!--- Start the Article Manager: --->
    <cfinclude template="dsp_menu.cfm">

    <cfif attributes.fuseaction contains "categoryManager">
      <cfinclude template="categoryManager/index.cfm">
    <cfelseif attributes.fuseaction contains "RSSGenerator.all">
      <cfinclude template="RSSGenerator/index.cfm">
    <cfelseif attributes.fuseaction contains "spotlightManager">
      <cfinclude template="spotlightManager/index.cfm">
    <cfelseif attributes.fuseaction contains "expertsManager">
      <cfinclude template="ExpertsManager2/viewExperts.cfm">
    <cfelse>
    	<cfinclude template="articleManager/index.cfm">
    </cfif>

  </div>
</div>
</section>
<br/><br/>

<cfinclude template="/_files/global-footer.html" />
<!-- activate typekit -->
<script src="/today/js/typekit.js"></script>
<!--GOBAL UTSA SCRIPT - DO NOT REMOVE-->
<!-- this includes JQuery, Bootstrap.js, tether.js, slick.js -->
<script type="text/javascript" src="/_files/js/global.min.js"></script>
<!-- LOCAL PROJECT BOWER INCLUDES -->
</body>
</html>
