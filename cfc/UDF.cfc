<cfcomponent output="false" hint="user defined functions">
	<cffunction name="todayLog" access="public" output="false">
		<cfargument name="logMessage" type="String" required="yes"/>
		<cflog file="#Application.applicationname#" text="#Application.applicationname# - #logMessage#" />
	</cffunction>

	<cffunction name="DeMoronize" access="public" output="false" returntype="string">
		<cfargument name="text" type="string" required="true" hint="The string that we are going to be cleaning."/>
		<cfscript>
			var i = 0;
			// map incompatible non-ISO characters into plausible
			// substitutes
			text = Replace(text, Chr(128), "&euro;", "All");
			text = Replace(text, Chr(130), ",", "All");
			text = Replace(text, Chr(131), "<em>f</em>", "All");
			text = Replace(text, Chr(132), ",,", "All");
			text = Replace(text, Chr(133), "...", "All");
			text = Replace(text, Chr(136), "^", "All");
			text = Replace(text, Chr(139), ")", "All");
			text = Replace(text, Chr(140), "Oe", "All");
			text = Replace(text, Chr(145), "`", "All");
			text = Replace(text, Chr(146), "'", "All");
			text = Replace(text, Chr(147), """", "All");
			text = Replace(text, Chr(148), """", "All");
			text = Replace(text, Chr(149), "*", "All");
			text = Replace(text, Chr(150), "-", "All");
			text = Replace(text, Chr(151), "--", "All");
			text = Replace(text, Chr(152), "~", "All");
			text = Replace(text, Chr(153), "&trade;", "All");
			text = Replace(text, Chr(155), ")", "All");
			text = Replace(text, Chr(156), "oe", "All");
			// remove any remaining ASCII 128-159 characters
			for (i = 128; i LTE 159; i = i + 1) {
				text = Replace(text, Chr(i), "", "All");
			}
			// map Latin-1 supplemental characters into
			// their &name; encoded substitutes
			text = Replace(text, Chr(160), "&nbsp;", "All");
			text = Replace(text, Chr(163), "&pound;", "All");
			text = Replace(text, Chr(169), "&copy;", "All");
			text = Replace(text, Chr(176), "&deg;", "All");
			// Modified by Ruben O. --> replace ASCII 0-32 (non-printing characters) with " " char
			for (i = 0; i LTE 31; i = i + 1) {
				text = REReplace(text, "(#Chr(i)#)", "", "All");
			}
			// encode ASCII 160-255 using ? format
			for (i = 160; i LTE 255; i = i + 1) {
				text = REReplace(text, "(#Chr(i)#)", "&###i#;", "All");
			}
			for (i = 8216; i LTE 8218; i = i + 1) {
				text = Replace(text, Chr(i), "'", "All");
			}
			// supply missing semicolon at end of numeric entities
			text = ReReplace(text, "&##([0-2][[:digit:]]{2})([^;])", "&##\1;\2", "All");
			// fix obscure numeric rendering of &lt; &gt; &amp;
			text = ReReplace(text, "&##038;", "&amp;", "All");
			text = ReReplace(text, "&##060;", "&lt;", "All");
			text = ReReplace(text, "&##062;", "&gt;", "All");
			// supply missing semicolon at the end of &amp; &quot;
			text = ReReplace(text, "&amp(^;)", "&amp;\1", "All");
			text = ReReplace(text, "&quot(^;)", "&quot;\1", "All");
		</cfscript>
		<cfreturn text />
	</cffunction>

	<cffunction name="CleanHighAscii" access="public" output="false" returntype="string">

		<cfargument name="Text" type="string" required="true" hint="The string that we are going to be cleaning."/>

		<!--- Set up local scope. --->
		<cfset var LOCAL = {} />
		<!---
		When cleaning the string, there are going to be ascii
		values that we want to target, but there are also going
		to be high ascii values that we don't expect. Therefore,
		we have to create a pattern that simply matches all non
		low-ASCII characters. This will find all characters that
		are NOT in the first 127 ascii values. To do this, we
		are using the 2-digit hex encoding of values.
		--->
		<cfset LOCAL.Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\x7F]" )) />

		<!---
		Create the pattern matcher for our target text. The
		matcher will be able to loop through all the high
		ascii values found in the target string.
		--->
		<cfset LOCAL.Matcher = LOCAL.Pattern.Matcher(JavaCast( "string", ARGUMENTS.Text )) />

		<!---
		As we clean the string, we are going to need to build
		a results string buffer into which the Matcher will
		be able to store the clean values.
		--->
		<cfset LOCAL.Buffer = CreateObject("java", "java.lang.StringBuffer").Init() />

		<!--- Keep looping over high ascii values. --->
		<cfloop condition="LOCAL.Matcher.Find()">

			<!--- Get the matched high ascii value. --->
			<cfset LOCAL.Value = LOCAL.Matcher.Group() />

			<!--- Get the ascii value of our character. --->
			<cfset LOCAL.AsciiValue = Asc( LOCAL.Value ) />

			<!---
			Now that we have the high ascii value, we need to
			figure out what to do with it. There are explicit
			tests we can perform for our replacements. However,
			if we don't have a match, we need a default
			strategy and that will be to just store it as an
			escaped value.
			--->

			<!--- Check for Microsoft double smart quotes. --->
			<cfif ((LOCAL.AsciiValue EQ 8220) OR (LOCAL.AsciiValue EQ 8221))>
				<!--- Use standard quote. --->
				<cfset LOCAL.Value = """" />
			<!--- Check for Microsoft single smart quotes. --->
			<cfelseif ((LOCAL.AsciiValue EQ 8216) OR (LOCAL.AsciiValue EQ 8217))>
				<!--- Use standard quote. --->
				<cfset LOCAL.Value = "'" />
			<!--- Check for Microsoft elipse. --->
			<cfelseif (LOCAL.AsciiValue EQ 8230)>
				<!--- Use several periods. --->
				<cfset LOCAL.Value = "..." />
			<cfelse>
				<!---
				We didn't get any explicit matches on our
				character, so just store the escaped value.
				--->
				<cfset LOCAL.Value = "&###LOCAL.AsciiValue#;" />
			</cfif>

			<!---
			Add the cleaned high ascii character into the
			results buffer. Since we know we will only be
			working with extended values, we know that we don't
			have to worry about escaping any special characters
			in our target string.
			--->
			<cfset LOCAL.Matcher.AppendReplacement(LOCAL.Buffer, JavaCast( "string", LOCAL.Value )) />
		</cfloop>

		<!---
		At this point there are no further high ascii values
		in the string. Add the rest of the target text to the
		results buffer.
		--->
		<cfset LOCAL.Matcher.AppendTail(LOCAL.Buffer) />

		<!--- Return the resultant string. --->
		<cfreturn LOCAL.Buffer.ToString() />
	</cffunction>
</cfcomponent>
