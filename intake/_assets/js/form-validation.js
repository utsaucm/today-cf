$(document).ready(function() {
  var formObj = $("form");
  initializeFormView("event", false);
  initializeFormView("around", false);
  /******************************* FORM - Event - Functions  *************************/
  // Confirm changes ...
  $("#eventNextBtn").click(function(){
    var isValidEvtSubmit = formObj.valid();
    //scrollToId("outer-layer-event");
    //isValidEvtSubmit = validateFields("utEvent", isValidEvtSubmit);
    //alert("is Event valid: " + isValidEvtSubmit);
    if (isValidEvtSubmit){
      hideFormView("intake-event");
      confirmFormView("event");
      hideFields("utEvent");
      showPara("event");
    }
    //isValidEvtSubmit = true; // reset flag
    return false; // don't submit the form!
  }); // end of #eventNextBtn function

  // Go back to make changes to form
	$("#eventGoBackBtn").click(function(){
		// UI configs
		initializeFormView("event", 0);
		goBackFormView("event");
		// Show form fields
		showFields('utEvent');
		// Hide confirmation paragraphs
		hidePara("event");
		// Go back to form view
		showFormView("intake-event");
	});	// end of #eventGoBackBtn

  $("#aroundNextBtn").click(function(){
    var isValidAroundSubmit = formObj.valid();
		if (isValidAroundSubmit){
      hideFormView("intake-around");
			confirmFormView("around");
			hideFields("utStory");
			showPara("around");
		} else{
			highlightRequireMessage("around");
		}
		return false; // don't submit the form!
	}); // end of #eventNextBtn function

	// Go back to make changes to form
	$("#aroundGoBackBtn").click(function(){
		//alert("I am going back!!!");
		// UI config
		initializeFormView("around", 0);
		goBackFormView("around");
		// Show input and textarea form fields
		showFields("utStory");
		// Hide Around Campus confirmation paragraphs
	 	hidePara("around");
		// Go back to form view
		showFormView("intake-around");
	 });	// end of #eventGoBackBtn

  function initializeFormView(formid, hidden){
    formObj.validate({
      debug: true,
      submitHandler: function(form) {
        //console.log('form submit go!');
        // do other things for a valid form
        form.submit();
      }
    });

    if (hidden){
      $("#outer-layer-" + formid).hide();
      //$("#outer-layer-" + formid +"-guidelines").hide();
    }
    $("#" + formid + "ReqConfirmMsg").hide();
    $("#" + formid + "GoBackBtn").hide();
    $("#" + formid + "Submit").hide();
  } // end of initializeFormView func

  function confirmFormView(formid){
    $("#" + formid + "ReqConfirmMsg").show();
    $("#" + formid + "ReqConfirmMsg").css('color', '#204d74');
    $("#" + formid + "ReqMsg").hide();
    $("#" + formid + "GoBackBtn").show();
    $("#" + formid + "Submit").show();
    $("#" + formid + "Btn").hide();
    $("#" + formid + "NextBtn").hide();
    $("#" + formid + "ResetBtn").hide();
  } // end of confirmFormView func

  function goBackFormView(formid){
    $("#" + formid + "NextBtn").show();
    $("#" + formid + "ResetBtn").show();
    $("#" + formid + "ReqMsg").show();
    $("#" + formid + "ReqConfirmMsg").hide();
  } // end of goBackFormView func

  function hideFields(id){
    // LOOP through all input-text, textarea controls - hide them
    console.log("fooo");
    $('input, textarea').each(function(){
      if ($(this).attr('name').indexOf(id) != -1){
        $(this).hide();
        // put it's content (val) into the p tag
        $("#" + $(this).attr('name') + "Confirm").html($(this).val());
      }
    });
  } // end of hideFields func

  function showFields(id){
    // LOOP through all input-text, textarea controls - show them
    $(':text, textarea').each(function(){
      if ($(this).attr('name').indexOf(id) != -1){
        $(this).show();
      }
    });
  } // end of showFields func

  function showFormView(id){
    $("#" + id).addClass("well");
  } // end of showFormView func

  function hideFormView(id){
    // Go back to form view
    $("#" + id).removeClass();
  } // end of hideFormView func

  function showPara(id){
    $("p." + id).show();
    $("p." + id).css('color', '#204d74');
  }  // end of showParaw func

  function hidePara(id){
    $("p." + id).hide();
  } // end of hidePara func
});
