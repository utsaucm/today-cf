$(function(){
    $("#createRRA a:contains('Create')").parent().addClass('active');
    $("#ViewAll a:contains('All')").parent().addClass('active');
    $("#ViewDrafts a:contains('Draft')").parent().addClass('active');
	$("#ViewPublished a:contains('Published')").parent().addClass('active');
	$("#ViewHidden a:contains('Hidden')").parent().addClass('active');
	$("#ViewArchive a:contains('Archived')").parent().addClass('active');
	$("#UpdateRR a:contains('Update')").parent().addClass('active');
  	$("#refreshRRWeb a:contains('Refresh')").parent().addClass('active');
	$("#logoutRR a:contains('Logout')").parent().addClass('active');
});
