$(document).ready(function(){
	var isValidEvtSubmit = true;
	var isValidAroundSubmit = true;
				
	/************** FORM - Event ***************/ 
	// Hide Event form 			
	initializeFormView("event", 1);
	
	$("#eventBtn").click(function(){
		$("#outer-layer-event").toggle();
	});
							
	/*********** FORM - Around Campus *************/
	// Hide Around Campus form 	
	initializeFormView("around" , 1);
					
	$("#aroundCampBtn").click(function(){
		$("#outer-layer-around").toggle();
	});
							
	 /************** FORM - Event - Functions  ***************/ 
	// Confirm changes ...
	$("#nextBtnEvt").click(function(){
	
		scrollToId("outer-layer-event");
		 
		validateFields("utEvent");				 
		
		if (isValidEvtSubmit){			
			hideFormView("intake-event");			
			confirmFormView("event");						
			hideFields("utEvent");				
			showPara("event");			
		}
		else{			
			//$("#eventReqMsg").css("color", "#900");
			requireMessage("event");
			if ($("#utEventName").val().length === 0 || $("#utEventURL").val().length === 0 || $("#utEventURL").val().length === 0){				
				scrollToId("eventReqMsg");
			}			
		}
		
		isValidEvtSubmit = true; // reset flag
		return false; // don't submit the form!						
	
	}); // end of #nextBtnEvt function
	   
	// Go back to make changes to form
	$("#goBackBtnEvt").click(function(){
		// UI configs 		
		initializeFormView("event", 0);
				
		goBackFormView("event");
			
		// Show form fields
		showFields('utEvent');
				
		// Hide confirmation paragraphs
		hidePara("event");
						
		// Go back to form view
		showFormView("intake-event");						        
	 	
	});	// end of #goBackBtnEvt
	
	
   /************** FORM - Event - Functions  ***************/
	// Confirm changes ...
	/*
	$("#submitEvt").click(function(){
			//alert("You submitted your form!");
			
			
			$.ajax("process-form.cfm", function(result){
				//$("#eventReqConfirmMsg").html(result);
				//$("#intake-event").hide();
				alert(result);
			});
		
		
		});
		
	*/
	
	/*
	$("#submitEvt").click(function(){
				 
			$.post( "process-form.cfm", $( "#intake-event" ).serialize(), 
				function(result){
					 alert(result.trim());
					 //$("#eventBtn").show();
				}
			);
			
			$("#eventBtn").show();			
			
			$("#eventReqConfirmMsg").hide();
			$("#outer-layer-event").toggle();
			
			// Go back to form view
			$("#intake-event").addClass("well");	
			
			// LOOP through all input-text, textarea controls - show them
			$(':text, textarea').each(function(){
				// show the control			
				//if ($(this).attr('name').contains("utEvent")){			
				if ($(this).attr('name').indexOf('utEvent') != -1){		
					$(this).show();
				}					    
			});
			
			// LOOP through all input-text, textarea controls - clear them
			$(':text, textarea').each(function(){			
				if ($(this).attr('name').indexOf('utEvent') != -1){									
									
					// Clear the fields										
					$(this).val("");			
				}				
			});
			
			// Hide confirmation paragraphs
			$("p.event").hide();					
			
			// Hide and Show buttons
			$("#goBackBtnEvt").hide();  
			$("#resetBtnEvt").show();
			$("#nextBtnEvt").show(); 
			$("#submitEvt").hide();
			
			return false;	
		});
	*/

	$("#nextBtnAroundCamp").click(function(){
		
		validateFields('utStory');		
				
		if (isValidAroundSubmit){					       		
			confirmFormView("around");	
			
			hideFormView("intake-aroundCamp");
							   
			hideFields("utStory");
			
			// Show paragraphs 
			showPara("around"); 		
		}
		else{			
			//$("#aroundCampReqMsg").css("color", "#900");
			requireMessage("around");			
		}		

		isValidAroundSubmit = true; // reset flag
		return false; // don't submit the form!			
			   
		// Show the Event paragraphs for confirmation	
			
	}); // end of #nextBtnEvt function
	
	// Go back to make changes to form
	$("#goBackBtnAroundCamp").click(function(){		
		//alert("I am going back!!!");		
		// UI config 		  
			
		initializeFormView("around", 0);		
		goBackFormView("around");
		
		// Show input and textarea form fields
		showFields("utStory");
	
		// Hide Around Campus confirmation paragraphs
	 	hidePara("around");
	 
		// Go back to form view
		showFormView("intake-aroundCamp");	 
	});	// end of #goBackBtnEvt 					   
  	
}); // end of Document ready...	


/*************************** Functions *********************************/

function initializeFormView(formid, hidden){
	if (hidden){
		$("#outer-layer-" + formid).hide();
	}
									
	/*$("#" + formid + "ReqConfirmMsg").hide();*/
	
	if (formid == "event"){
		$("#eventReqConfirmMsg").hide();
		$("#goBackBtnEvt").hide();  
		$("#submitEvt").hide();	
	}
	else {
		$("#aroundCampReqConfirmMsg").hide();
		$("#goBackBtnAroundCamp").hide();  
		$("#submitAroundCamp").hide();
	}
} // end of initializeFormView func

function confirmFormView(formid){
	if (formid == "around"){
		// Hide the around campus controls
		$("#aroundCampReqConfirmMsg").show();
		$("#aroundCampReqConfirmMsg").css('color', '#204d74');
		
		$("#aroundCampReqMsg").hide();
		
		$("#goBackBtnAroundCamp").show();
		$("#submitAroundCamp").show();
		
		$("#aroundCampBtn").hide();
		$("#nextBtnAroundCamp").hide();  
		$("#resetBtnAroundCamp").hide();
	}
	else {
		// Hide the EVENT controls
		$("#eventReqConfirmMsg").show();
		$("#eventReqConfirmMsg").css('color', '#204d74');
		
		$("#eventReqMsg").hide();
		
		$("#goBackBtnEvt").show();
		$("#submitEvt").show();
		
		$("#eventBtn").hide();
		$("#nextBtnEvt").hide();  
		$("#resetBtnEvt").hide();
	}
} // end of confirmFormView func

function goBackFormView(formid){
	if (formid == "around"){
		$("#nextBtnAroundCamp").show();
		$("#resetBtnAroundCamp").show();
		$("#aroundCampReqMsg").show();
		$("#aroundCampReqConfirmMsg").hide();	
	}
	else {
		$("#nextBtnEvt").show();
		$("#resetBtnEvt").show();
		$("#eventReqMsg").show();
		$("#eventReqConfirmMsg").hide();
	}
} // end of goBackFormView func

function requireMessage(formid){
	if (formid == "around"){
		$("#aroundCampReqMsg").css("color", "#900");
	}
	else {
		$("#eventReqMsg").css("color", "#900");
	}	

} // end of requireMessage func

function scrollToId(id){
	$('html, body').animate({
		scrollTop: $("#" +  id).offset().top
	}, 1000);	
} // end of scrollToId func


function showFields(id){
	// LOOP through all input-text, textarea controls - show them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){	
			$(this).show();					    
		}
	});
} // end of showFields func

function hideFields(id){
	// LOOP through all input-text, textarea controls - hide them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){
			$(this).hide();					       			
			// put it's content (val) into the p tag
			$("#" + $(this).attr('name') + "Confirm").html($(this).val());
		}
	});	    
} // end of hideFields func

function validateFields(id){
	$(':text, textarea').each(function(){			
		if ($(this).attr('name').indexOf(id) != -1){
			if ($(this).val().length === 0){
				isValidEvtSubmit  = false;
				$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
				$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");							    							    	
			}
			else {
				//isValidEvtSubmit = true;
				$("#" + $(this).attr('name') + "Help").html("");
			}
		}				
	});
} // end of validateFields func

function showFormView(id){
	$("#" + id).addClass("well");		
} // end of showFormView func

function hideFormView(id){
	// Go back to form view
	$("#" + id).removeClass();		
} // end of hideFormView func

function showPara(id){
	$("p." + id).show();
	$("p." + id).css('color', '#204d74'); 	
}  // end of showParaw func

function hidePara(id){
	$("p." + id).hide();	  
} // end of hidePara func					