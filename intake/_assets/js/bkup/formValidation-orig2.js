$(document).ready(function(){
	var isValidEvtSubmit = true;
	var isValidAroundSubmit = true;
				
	/************** FORM - Event ***************/ 
	// Hide Event form 			
	$("#outer-layer-event").hide();							
	$("#eventReqConfirmMsg").hide();
	$("#goBackBtnEvt").hide();  
	$("#submitEvt").hide();
	
	$("#eventBtn").click(function(){
		$("#outer-layer-event").toggle();
	});
							
	/*********** FORM - Around Campus *************/
	// Hide Around Campus form 	
	$("#outer-layer-around").hide(); 
	
	$("#aroundCampReqConfirmMsg").hide();
	$("#goBackBtnAroundCamp").hide();  
	$("#submitAroundCamp").hide();
							
	$("#aroundCampBtn").click(function(){
		$("#outer-layer-around").toggle();
	});
							
	 /************** FORM - Event - Functions  ***************/ 
	// Confirm changes ...
	$("#nextBtnEvt").click(function(){
			 
		$('html, body').animate({
			scrollTop: $("#outer-layer-event").offset().top
		}, 1000);
		 
		validateFields("utEvent");
				 
		/*$(':text, textarea').each(function(){			
			if ($(this).attr('name').indexOf('utEvent') != -1){
								
				if ($(this).val().length === 0){
					isValidEvtSubmit  = false;
					$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
					$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");							    							    	
				}
				else {
					//isValidEvtSubmit = true;
					$("#" + $(this).attr('name') + "Help").html("");
				}
			}				
		});*/
		
		if (isValidEvtSubmit){					       		
			// Hide the controls
			$("#intake-event").removeClass();					       	
			$("#eventReqConfirmMsg").show();
			$("#eventReqMsg").hide();
			$("#eventReqConfirmMsg").css('color', '#204d74');
			$("#goBackBtnEvt").show();
			$("#submitEvt").show();
			$("#eventBtn").hide();
			$("#nextBtnEvt").hide();  
			$("#resetBtnEvt").hide();
								   
			// LOOP through all input-text, textarea controls - hide them
			/*
			$(':text, textarea').each(function(){
				// Hide the controls
				
				if ($(this).attr('name').indexOf('utEvent') != -1){	
				
					$(this).hide();					       			
					// put it's content (val) into the p tag
					$("#" + $(this).attr('name') + "Confirm").html($(this).val());
				}
			});	
			*/
			hideFields("utEvent");
			    
			/*
			$("p.event").show();
			$("p.event").css('color', '#204d74');
			*/
			showPara("event");
			
		}
		else{			
			$("#eventReqMsg").css("color", "#900");
			if ($("#utEventName").val().length === 0 || $("#utEventURL").val().length === 0 || $("#utEventURL").val().length === 0){
				$('html, body').animate({
					scrollTop: $("#eventReqMsg").offset().top
				}, 1000);
			}			
		}
		
		isValidEvtSubmit = true; // reset flag
		return false; // don't submit the form!							
	
	}); // end of #nextBtnEvt function
	
   
	// Go back to make changes to form
	$("#goBackBtnEvt").click(function(){
		// UI config 
		$("#nextBtnEvt").show();
		$("#eventReqMsg").show();
		$("#eventReqConfirmMsg").hide();
		$("#goBackBtnEvt").hide();  
		$("#submitEvt").hide();
		$("#resetBtnEvt").show();
		
		
		/*
		// LOOP through all input-text, textarea controls - show them
		$(':text, textarea').each(function(){
			// show the control		
			if ($(this).attr('name').indexOf('utEvent') != -1){		
				$(this).show();
			}					    
		});
		*/
		
		// Show form fields
		showFields('utEvent');
				
		// Hide confirmation paragraphs
		//$("p.event").hide();		
		hidePara("event");
						
		// Go back to form view
		//$("#intake-event").addClass("well");
		showFormView("intake-event");						        
	 	
	});	// end of #goBackBtnEvt
	
	
   /************** FORM - Event - Functions  ***************/
	// Confirm changes ...
	/*
	$("#submitEvt").click(function(){
			//alert("You submitted your form!");
			
			
			$.ajax("process-form.cfm", function(result){
				//$("#eventReqConfirmMsg").html(result);
				//$("#intake-event").hide();
				alert(result);
			});
		
		
		});
		
	*/
	
	/*
	$("#submitEvt").click(function(){
				 
			$.post( "process-form.cfm", $( "#intake-event" ).serialize(), 
				function(result){
					 alert(result.trim());
					 //$("#eventBtn").show();
				}
			);
			
			$("#eventBtn").show();			
			
			$("#eventReqConfirmMsg").hide();
			$("#outer-layer-event").toggle();
			
			// Go back to form view
			$("#intake-event").addClass("well");	
			
			// LOOP through all input-text, textarea controls - show them
			$(':text, textarea').each(function(){
				// show the control			
				//if ($(this).attr('name').contains("utEvent")){			
				if ($(this).attr('name').indexOf('utEvent') != -1){		
					$(this).show();
				}					    
			});
			
			// LOOP through all input-text, textarea controls - clear them
			$(':text, textarea').each(function(){			
				if ($(this).attr('name').indexOf('utEvent') != -1){									
									
					// Clear the fields										
					$(this).val("");			
				}				
			});
			
			// Hide confirmation paragraphs
			$("p.event").hide();					
			
			// Hide and Show buttons
			$("#goBackBtnEvt").hide();  
			$("#resetBtnEvt").show();
			$("#nextBtnEvt").show(); 
			$("#submitEvt").hide();
			
			return false;	
		});
	*/

	$("#nextBtnAroundCamp").click(function(){
		
		validateFields('utStory');
		
		/*$(':text, textarea').each(function(){
						
			if ($(this).attr('name').indexOf('utStory') != -1){	
				if ($(this).val().length === 0){
					isValidAroundSubmit  = false;					
					$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
					$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");					
				}
				else{
					//isValidEvtSubmit = true;
					$("#" + $(this).attr('name') + "Help").html("");
				}
			}
		});*/
				
		if (isValidAroundSubmit){					       		
			// Hide the controls
			$("#aroundCampReqConfirmMsg").show();
			$("#aroundCampReqConfirmMsg").css('color', '#204d74');
			$("#aroundCampReqMsg").hide();
			$("#goBackBtnAroundCamp").show();
			$("#submitAroundCamp").show();
			$("#aroundCampBtn").hide();
			$("#nextBtnAroundCamp").hide();  
			$("#resetBtnAroundCamp").hide();		
			$("#intake-aroundCamp").removeClass();		
									   
			// LOOP through all input-text, textarea controls - hide them
			/*
			$(':text, textarea').each(function(){
				// Hide the controls		
								
				//if ($(this).attr('name').contains("utStory")){
				if ($(this).attr('name').indexOf('utStory') != -1){
					$(this).hide();					       			
					// put it's content (val) into the p tag
					$("#" + $(this).attr('name') + "Confirm").html($(this).val());
				}
			});	    
			*/
			hideFields("utStory");
						
			/*
			$("p.around").show();
			$("p.around").css('color', '#204d74');
			*/
			// Show paragraphs 
			showPara("around"); 	
		
		}
		else{			
			$("#aroundCampReqMsg").css("color", "#900");			
		}		

		isValidAroundSubmit = true; // reset flag
		return false; // don't submit the form!			
			   
		// Show the Event paragraphs for confirmation	
			
	}); // end of #nextBtnEvt function
	
	// Go back to make changes to form
	$("#goBackBtnAroundCamp").click(function(){		
		//alert("I am going back!!!");		
		// UI config 		  
		$("#nextBtnAroundCamp").show();
		$("#resetBtnAroundCamp").show();
		$("#aroundCampReqConfirmMsg").hide();
		$("#aroundCampReqMsg").show();
		$("#goBackBtnAroundCamp").hide();  
		$("#submitAroundCamp").hide();
		
		// LOOP through all input-text, textarea controls - show them
		/*
		$(':text, textarea').each(function(){
			// show the control
			
			//if ($(this).attr('name').contains("utStory")){
			if ($(this).attr('name').indexOf('utStory') != -1){	
				$(this).show();					    
			}
		});
		*/
		showFields("utStory");
	
		// Hide Around Campus confirmation paragraphs
		//$("p.around").hide();	  
	 	hidePara("around");
	 
		// Go back to form view
		//$("#intake-aroundCamp").addClass("well");
		showFormView("intake-aroundCamp");				        
	 
	});	// end of #goBackBtnEvt 					   
  	
}); // end of Document ready...	



/******************** Functions *********************************/

function showFields(id){
	// LOOP through all input-text, textarea controls - show them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){	
			$(this).show();					    
		}
	});
} // end of showFields func

function hideFields(id){
	// LOOP through all input-text, textarea controls - hide them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){
			$(this).hide();					       			
			// put it's content (val) into the p tag
			$("#" + $(this).attr('name') + "Confirm").html($(this).val());
		}
	});	    
} // end of hideFields func

function validateFields(id){
	$(':text, textarea').each(function(){			
		if ($(this).attr('name').indexOf(id) != -1){
			if ($(this).val().length === 0){
				isValidEvtSubmit  = false;
				$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
				$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");							    							    	
			}
			else {
				//isValidEvtSubmit = true;
				$("#" + $(this).attr('name') + "Help").html("");
			}
		}				
	});
} // end of validateFields func

function showFormView(id){
	$("#" + id).addClass("well");		
} // end of showFormView func

function hideFormView(id){
	// Go back to form view
	$("#" + id).removeClass();		
} // end of hideFormView func

function showPara(id){
	$("p." + id).show();
	$("p." + id).css('color', '#204d74'); 	
}  // end of showParaw func

function hidePara(id){
	$("p." + id).hide();	  
} // end of hidePara func					