$(document).ready(function(){
	var isValidEvtSubmit = true;
	var isValidAroundSubmit = true;
				
	/************** FORM - Event ***************/ 
	// Hide Event form 			
	$("#outer-layer-event").hide();							
	$("#eventReqConfirmMsg").hide();
	$("#goBackBtnEvt").hide();  
	$("#submitEvt").hide();
	
	$("#eventBtn").click(function(){
		$("#outer-layer-event").toggle();
	});
							
	/*********** FORM - Around Campus *************/
	// Hide Around Campus form 	
	$("#outer-layer-around").hide(); 
	
	$("#aroundCampReqConfirmMsg").hide();
	$("#goBackBtnAroundCamp").hide();  
	$("#submitAroundCamp").hide();
							
	$("#aroundCampBtn").click(function(){
		$("#outer-layer-around").toggle();
	});
							
	 /************** FORM - Event - Functions  ***************/ 
	// Confirm changes ...
	$("#nextBtnEvt").click(function(){
		
		//$("#intake-event").css("background-color", "#eee");
		//$("#intake-event").css("background-color", "#E6E6E6");				  
																
		//alert("before loop: " + isValidEvtSubmit);
		 
		$('html, body').animate({
			scrollTop: $("#outer-layer-event").offset().top
		}, 1000);
		 
		$(':text, textarea').each(function(){			
			//if ($(this).attr('name').contains("utEvent"))
			if ($(this).attr('name').indexOf('utEvent') != -1){
								
				//alert($(this).val());					
				//alert ("the len is: " + $(this).val().length);
				
				if ($(this).val().length === 0){
					isValidEvtSubmit  = false;
					$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
					$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");
					//return false; // break out of .each					    							    	
				}
				else {
					//isValidEvtSubmit = true;
					$("#" + $(this).attr('name') + "Help").html("");
				}
			}				
		});
		
		//alert("after loop: " + isValidEvtSubmit);
		
		if (isValidEvtSubmit){					       		
			// Hide the controls
			$("#intake-event").removeClass();					       	
			$("#eventReqConfirmMsg").show();
			$("#eventReqMsg").hide();
			$("#eventReqConfirmMsg").css('color', '#204d74');
			$("#goBackBtnEvt").show();
			$("#submitEvt").show();
			$("#eventBtn").hide();
			$("#nextBtnEvt").hide();  
			$("#resetBtnEvt").hide();
								   
			// LOOP through all input-text, textarea controls - hide them
			$(':text, textarea').each(function(){
				// Hide the controls
				//if ($(this).attr('name').contains("utEvent")){
				if ($(this).attr('name').indexOf('utEvent') != -1){	
				
					$(this).hide();					       			
					// put it's content (val) into the p tag
					$("#" + $(this).attr('name') + "Confirm").html($(this).val());
				}
			});	    
			
			$("p.event").show();
			$("p.event").css('color', '#204d74');
		}
		else{			
			$("#eventReqMsg").css("color", "#900");
			if ($("#utEventName").val().length === 0 || $("#utEventURL").val().length === 0 || $("#utEventURL").val().length === 0){
				$('html, body').animate({
					scrollTop: $("#eventReqMsg").offset().top
				}, 1000);
			}			
		}
		
	   
		// Show the Event paragraphs for confirmation						   
		/*
		$('p').each(function(){
			if ($('p').attr('id').contains("utEvent")){
				$(this).css('color', '#204d74');
				$(this).show();
				
			}
		});
		*/	
		isValidEvtSubmit = true; // reset flag
		return false; // don't submit the form!							
	
	}); // end of #nextBtnEvt function
	
   
	// Go back to make changes to form
	$("#goBackBtnEvt").click(function(){
		// UI config 
		$("#nextBtnEvt").show();
		$("#eventReqMsg").show();
		$("#resetAroundCamp").show();
		$("#eventReqConfirmMsg").hide();
		$("#goBackBtnEvt").hide();  
		$("#submitEvt").hide();
		$("#resetBtnEvt").show();
		
		// LOOP through all input-text, textarea controls - show them
		$(':text, textarea').each(function(){
			// show the control			
			//if ($(this).attr('name').contains("utEvent")){			
			if ($(this).attr('name').indexOf('utEvent') != -1){		
				$(this).show();
			}					    
		});
		
		// hide paragraphs
		/*
		$('p').each(function(){
			if ($('p').attr('id').contains("utEvent")){
				$(this).hide();
			}
		});
		*/		
		// Hide confirmation paragraphs
		$("p.event").hide();		
				
		// Go back to form view
		$("#intake-event").addClass("well");					        
	 	
	});	// end of #goBackBtnEvt
	
	
   /************** FORM - Event - Functions  ***************/
	// Confirm changes ...
	/*
	$("#submitEvt").click(function(){
			//alert("You submitted your form!");
			
			
			$.ajax("process-form.cfm", function(result){
				//$("#eventReqConfirmMsg").html(result);
				//$("#intake-event").hide();
				alert(result);
			});
		
		
		});
		
	*/
	
	/*
	$("#submitEvt").click(function(){
				 
			$.post( "process-form.cfm", $( "#intake-event" ).serialize(), 
				function(result){
					 alert(result.trim());
					 //$("#eventBtn").show();
				}
			);
			
			$("#eventBtn").show();			
			
			$("#eventReqConfirmMsg").hide();
			$("#outer-layer-event").toggle();
			
			// Go back to form view
			$("#intake-event").addClass("well");	
			
			// LOOP through all input-text, textarea controls - show them
			$(':text, textarea').each(function(){
				// show the control			
				//if ($(this).attr('name').contains("utEvent")){			
				if ($(this).attr('name').indexOf('utEvent') != -1){		
					$(this).show();
				}					    
			});
			
			// LOOP through all input-text, textarea controls - clear them
			$(':text, textarea').each(function(){			
				if ($(this).attr('name').indexOf('utEvent') != -1){									
									
					// Clear the fields										
					$(this).val("");			
				}				
			});
			
			// Hide confirmation paragraphs
			$("p.event").hide();					
			
			// Hide and Show buttons
			$("#goBackBtnEvt").hide();  
			$("#resetBtnEvt").show();
			$("#nextBtnEvt").show(); 
			$("#submitEvt").hide();
			
			return false;	
		});
	*/

	$("#nextBtnAroundCamp").click(function(){
		
		//$("#intake-aroundCamp").css("background-color", "#eee");
		//$("#intake-aroundCamp").css("background-color", "#E6E6E6");
							   
		// LOOP through all input-text, textarea controls - hide them
		$(':text, textarea').each(function(){
			// Hide the controls
			//if ($(this).attr('name').contains("utStory")){				
			if ($(this).attr('name').indexOf('utStory') != -1){	
				if ($(this).val().length === 0){
					isValidAroundSubmit  = false;					
					$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
					$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");
					
					//$(this).hide();					       			
					// put it's content (val) into the p tag
					//$("#" + $(this).attr('name') + "Confirm").html($(this).val());
				}
				else{
					//isValidEvtSubmit = true;
					$("#" + $(this).attr('name') + "Help").html("");
				}
			}
		});
		
		
		if (isValidAroundSubmit){					       		
			// Hide the controls
			$("#aroundCampReqConfirmMsg").show();
			$("#aroundCampReqConfirmMsg").css('color', '#204d74');
			$("#aroundCampReqMsg").hide();
			$("#goBackBtnAroundCamp").show();
			$("#submitAroundCamp").show();
			$("#aroundCampBtn").hide();
			$("#nextBtnAroundCamp").hide();  
			$("#resetBtnAroundCamp").hide();		
			$("#intake-aroundCamp").removeClass();		
									   
			// LOOP through all input-text, textarea controls - hide them
			$(':text, textarea').each(function(){
				// Hide the controls		
								
				//if ($(this).attr('name').contains("utStory")){
				if ($(this).attr('name').indexOf('utStory') != -1){
					$(this).hide();					       			
					// put it's content (val) into the p tag
					$("#" + $(this).attr('name') + "Confirm").html($(this).val());
				}
			});	    
			
			$("p.around").show();
			$("p.around").css('color', '#204d74'); 	
		
		}
		else{			
			$("#aroundCampReqMsg").css("color", "#900");
			/*
			if ($("#utStoryHeadline").val().length === 0 || $("#utStoryURL").val().length === 0 || $("#utStoryPublishDate").val().length === 0){
				$('html, body').animate({
					scrollTop: $("#aroundCampReqMsg").offset().top
				}, 1000);
			}
			*/			
		}		
	   
		// Show the Event paragraphs for confirmation						   
		/*
		$('p').each(function(){
			if ($('p').attr('id').contains("utEvent")){
				$(this).css('color', '#204d74');
				$(this).show();
				
			}
		});
		*/	
		isValidAroundSubmit = true; // reset flag
		return false; // don't submit the form!			
			   
		// Show the Event paragraphs for confirmation						   
		/*
		$('p').each(function(){
			if ($('p').attr('id').contains("utStory")){
				$(this).css('color', '#204d74');
				$(this).show();						    		
			}
		});
		*/	    		
	
	}); // end of #nextBtnEvt function
	
	// Go back to make changes to form
	$("#goBackBtnAroundCamp").click(function(){		
		//alert("I am going back!!!");		
		// UI config 		  
		$("#nextBtnAroundCamp").show();
		$("#resetBtnAroundCamp").show();
		$("#aroundCampReqConfirmMsg").hide();
		$("#aroundCampReqMsg").show();
		$("#goBackBtnAroundCamp").hide();  
		$("#submitAroundCamp").hide();
		
		// LOOP through all input-text, textarea controls - show them
		$(':text, textarea').each(function(){
			// show the control
			
			//if ($(this).attr('name').contains("utStory")){
			if ($(this).attr('name').indexOf('utStory') != -1){	
				$(this).show();					    
			}
		});
		
		// Hide paragraphs
		/*
		$('p').each(function(){
			
			alert($(this).attr('id'));
			if ($(this).attr('id').contains("utStory")){
				$(this).hide();
				//alert($(this).attr('id'));					    		
			}						    							    
		});
		*/
		
		// Hide Around Campus confirmation paragraphs
		$("p.around").hide();	  
	 
		// Go back to form view
		$("#intake-aroundCamp").addClass("well");					        
	 
	});	// end of #goBackBtnEvt 					   
  	
}); // end of Document ready...		
					