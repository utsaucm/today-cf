$(document).ready(function(){
	var isValidEvtSubmit = true;
	var isValidAroundSubmit = true;
				
	/************************** FORM - Event **********************************/ 
	// Hide Event form 			
	initializeFormView("event", 1);
	
	$("#eventBtn").click(function(){
		$("#outer-layer-event").toggle();
	});
							
	/************************** FORM - Around Campus *************************/
	// Hide Around Campus form 	
	initializeFormView("around" , 1);
					
	$("#aroundBtn").click(function(){
		$("#outer-layer-around").toggle();
	});
							
	 /******************************* FORM - Event - Functions  *************************/ 
	// Confirm changes ...
	$("#eventNextBtn").click(function(){
	
		scrollToId("outer-layer-event");
		 
		isValidEvtSubmit = validateFields("utEvent", isValidEvtSubmit);				 
		
		//alert("is Event valid: " + isValidEvtSubmit);
		
		if (isValidEvtSubmit){			
			hideFormView("intake-event");			
			confirmFormView("event");						
			hideFields("utEvent");				
			showPara("event");			
		}
		else{			
			highlightRequireMessage("event");
			if ($("#utEventName").val().length === 0 || $("#utEventURL").val().length === 0 || $("#utEventURL").val().length === 0){				
				scrollToId("eventReqMsg");
			}			
		}
		
		isValidEvtSubmit = true; // reset flag
		return false; // don't submit the form!						
	
	}); // end of #eventNextBtn function
	   
	// Go back to make changes to form
	$("#eventGoBackBtn").click(function(){
		// UI configs 		
		initializeFormView("event", 0);
				
		goBackFormView("event");
			
		// Show form fields
		showFields('utEvent');
				
		// Hide confirmation paragraphs
		hidePara("event");
						
		// Go back to form view
		showFormView("intake-event");						        
	 	
	});	// end of #eventGoBackBtn
	
	
   /************** FORM - Event - Functions  ***************/
	// Confirm changes via AJAX...
		
	/*
	$("#eventSubmit").click(function(){
				 
			$.post( "process-form.cfm", $( "#intake-event" ).serialize(), 
				function(result){
					 alert(result.trim());
					 //$("#eventBtn").show();
				}
			);
			
			$("#eventBtn").show();			
			
			$("#eventReqConfirmMsg").hide();
			$("#outer-layer-event").toggle();
			
			// Go back to form view
			$("#intake-event").addClass("well");	
			
			// LOOP through all input-text, textarea controls - show them
			$(':text, textarea').each(function(){
				// show the control			
				//if ($(this).attr('name').contains("utEvent")){			
				if ($(this).attr('name').indexOf('utEvent') != -1){		
					$(this).show();
				}					    
			});
			
			// LOOP through all input-text, textarea controls - clear them
			$(':text, textarea').each(function(){			
				if ($(this).attr('name').indexOf('utEvent') != -1){									
									
					// Clear the fields										
					$(this).val("");			
				}				
			});
			
			// Hide confirmation paragraphs
			$("p.event").hide();					
			
			// Hide and Show buttons
			$("#eventGoBackBtn").hide();  
			$("#eventResetBtn").show();
			$("#eventNextBtn").show(); 
			$("#eventSubmit").hide();
			
			return false;	
		});
	*/

	$("#aroundNextBtn").click(function(){

		isValidAroundSubmit = validateFields('utStory', isValidAroundSubmit);	
				
		if (isValidAroundSubmit){					       		
			confirmFormView("around");				
			hideFormView("intake-around");							   
			hideFields("utStory");			
			// Show paragraphs 
			showPara("around"); 		
		}
		else{			
			highlightRequireMessage("around");			
		}		

		isValidAroundSubmit = true; // reset flag
		return false; // don't submit the form!			
			   
		// Show the Event paragraphs for confirmation	
			
	}); // end of #eventNextBtn function
	
	// Go back to make changes to form
	$("#aroundGoBackBtn").click(function(){		
		//alert("I am going back!!!");		
		// UI config 		  
			
		initializeFormView("around", 0);		
		goBackFormView("around");
		
		// Show input and textarea form fields
		showFields("utStory");
	
		// Hide Around Campus confirmation paragraphs
	 	hidePara("around");
	 
		// Go back to form view
		showFormView("intake-around");	 
	});	// end of #eventGoBackBtn 					   
  	
}); // end of Document ready...	


/*************************** Functions *********************************/

function initializeFormView(formid, hidden){
	if (hidden){
		$("#outer-layer-" + formid).hide();
	}									
	$("#" + formid + "ReqConfirmMsg").hide();
	$("#" + formid + "GoBackBtn").hide();
	$("#" + formid + "Submit").hide();
	
} // end of initializeFormView func

function confirmFormView(formid){
	$("#" + formid + "ReqConfirmMsg").show();
	$("#" + formid + "ReqConfirmMsg").css('color', '#204d74');		
	$("#" + formid + "ReqMsg").hide();		
	$("#" + formid + "GoBackBtn").show();
	$("#" + formid + "Submit").show();		
	$("#" + formid + "Btn").hide();
	$("#" + formid + "NextBtn").hide();  
	$("#" + formid + "ResetBtn").hide();

} // end of confirmFormView func

function goBackFormView(formid){
	$("#" + formid + "NextBtn").show();
	$("#" + formid + "ResetBtn").show();
	$("#" + formid + "ReqMsg").show();
	$("#" + formid + "ReqConfirmMsg").hide();	
} // end of goBackFormView func

function highlightRequireMessage(formid){
	$("#" + formid + "ReqMsg").css("color", "#900");
} // end of highlightRequireMessage func

function scrollToId(id){
	$('html, body').animate({
		scrollTop: $("#" +  id).offset().top
	}, 1000);	
} // end of scrollToId func


function showFields(id){
	// LOOP through all input-text, textarea controls - show them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){	
			$(this).show();					    
		}
	});
} // end of showFields func

function hideFields(id){
	// LOOP through all input-text, textarea controls - hide them
	$(':text, textarea').each(function(){
		if ($(this).attr('name').indexOf(id) != -1){
			$(this).hide();					       			
			// put it's content (val) into the p tag
			$("#" + $(this).attr('name') + "Confirm").html($(this).val());
		}
	});	    
} // end of hideFields func

function validateFields(id, isValid){
	$(':text, textarea').each(function(){			
		if ($(this).attr('name').indexOf(id) != -1){
			if ($(this).val().length === 0){
				isValid  = false;
				$("#" + $(this).attr('name') + "Help").html(" (please complete this field)");
				$("#" + $(this).attr('name') + "Help").css("color", "#900", "fontWeight", "bold");							    							    	
			}
			else {
				//isValidEvtSubmit = true;
				$("#" + $(this).attr('name') + "Help").html("");
			}
		}				
	});	
	return isValid;	
} // end of validateFields func

function showFormView(id){
	$("#" + id).addClass("well");		
} // end of showFormView func

function hideFormView(id){
	// Go back to form view
	$("#" + id).removeClass();		
} // end of hideFormView func

function showPara(id){
	$("p." + id).show();
	$("p." + id).css('color', '#204d74'); 	
}  // end of showParaw func

function hidePara(id){
	$("p." + id).hide();	  
} // end of hidePara func					