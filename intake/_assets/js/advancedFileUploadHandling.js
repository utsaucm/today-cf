    var errorFiles = [];
    var Files = [];
	
	var fileComplete = function(result){
        if(result.STATUS != 200){
            errorFiles.push(result.FILENAME);
        }
		Files.push(result.FILENAME);
		//alert("end fileComplete!");
    }
    var uploadComplete = function(){       
		var filemessage = "";
		for(var i=0; i < Files.length; i++){
           filemessage += Files[i] + " ";
		}
		
		// save the filenames
		$("#uploadFile1").val(Files[0]);
		$("#uploadFile2").val(Files[1]);
		
		
		
		alert (filemessage);    
        Files = [];       
        
		
		
		
		
		var icon = 'info';
        var message = 'All files were successfully uploaded';
        if(errorFiles.length != 0){
            icon = 'error';
            message = 'The following files were not uploaded:<br /><br />';
            for(var i=0; i < errorFiles.length; i++){
                message += '&nbsp;&nbsp;&bull;&nbsp;' + errorFiles[i] + '<br />';
            }
            errorFiles = [];
        }	
        
		if(!ColdFusion.MessageBox.isMessageBoxDefined('uploadMessage')){
            ColdFusion.MessageBox.create('uploadMessage', 'alert', 'Upload Complete', message, empty, {icon: icon, modal: true});
        }else{
            ColdFusion.MessageBox.update('uploadMessage', {icon: icon, modal: true});
            ColdFusion.MessageBox.updateMessage('uploadMessage', message);
        }
        //ColdFusion.MessageBox.show('uploadMessage');
		
    }
    var empty = function(){}
