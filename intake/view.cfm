<!---<cfdump var="#url#">--->
<!--- Parameters --->
<cfparam name="Page" type="string" default="default">
<cfparam name="Body" type="string" default="ViewAll">

<!--- H1 heading value --->
<cfparam name="Heading" type="string" default="View All UTSA Today Stories">

<!--- Need to filter this input var --->
<cfif NOT isDefined("url.page")>
	<cfset Page = "default">	
<cfelse>
	<cfset Page = url.page>
</cfif>

<!---<cfdump var="#View#">--->

<cfif Page EQ "default">
	<cfset Heading = "All UTSA Today Articles">
	<cfset Body = "ViewAll">              	
<cfelseif Page EQ "intake-event">
	<cfset Heading = "Submit Events to UTSA Today">
	<cfset Body = "ViewAll">
<cfelseif Page EQ "intake-around">
	<cfset Heading = "Submit Campus Wire/Announcements to UTSA Today">
	<cfset Body = "ViewAll">
<cfelseif Page EQ "sorry">
	<cfset Heading = "Oop!">
<cfelseif Page EQ "oops">
	<cfset Heading = "Sorry!">	        	
</cfif>			
      
<!--- Route the Page View --->
<cf_header bodyID="#Body#" Title="#Heading#">        
	<div class="content container">    	
        <cf_content view="#Page#" heading="#Heading#">		
	</div><!-- content container -->              
<cf_footer JS="#Page#">