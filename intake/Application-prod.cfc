<!--- Application: UTSA-TODAY-UCM --->
<!--- WEB PROGRAMMER: Ruben Ortiz --->
<!--- VERSION: 1.0 --->
<!--- DATE: 06/04/2015 --->
<cfcomponent output="false">
  <cfscript>
		this.name				= "UTSATODAY-intake-UCM";
		this.datasource 		= "meetarr";
		this.customtagpaths 	=  GetDirectoryFromPath(GetCurrentTemplatePath()) & "_inc/customtags/";
		this.applicationTimeout = createTimeSpan(0,0,0,10);
		this.loginStorage 		= "session";
		this.sessionManagement 	= true;
		this.sessionTimeout		= createTimeSpan(0,0,30,0);
		this.setClientCookies 	= true;
		this.setDomainCookies 	= false;
		this.scriptProtect 		= true;

		if (StructKeyExists(URL, "showdebug")){
  			this.debuggingIPAddress = "0:0:0:0:0:0:0:1,127.0.0.1";
  			this.enableRobustException = true;
  		}
    if (StructKeyExists(URL,"force_app_restart")) {
        onApplicationStart();
    }
  </cfscript>

	<!--- Application starts up --->
    <cffunction name="onApplicationStart" returntype="boolean">
        <cflog text="The UTSA TODAY web application has started." type="information" file="#this.name#">
        <cflock timeout="1" SCOPE="APPLICATION" TYPE="Exclusive">
        <cfscript>
          APPLICATION.ENV = "DEV";
          APPLICATION.path =  "/today/intake";
          APPLICATION.webAdminEmail = "webteam@utsa.edu";
          APPLICATION.defaultSender = "do-not-reply@utsa.edu";
          APPLICATION.defaultSubject = "UTSA Today";
          APPLICATION.eventTemplate = "#APPLICATION.path#/_inc/html/email-event-table.html";
          APPLICATION.aroundTemplate = "#APPLICATION.path#/_inc/html/email-around-table.html";
        </cfscript>

        <cfif APPLICATION.ENV eq "DEV">
          <cfset APPLICATION.toAddress = "john.garza@utsa.edu" />
          <cfset APPLICATION.bccAddress = "" />
          <cfscript>
            APPLICATION.recaptchaPublic = "6Lc_ghsTAAAAAAhEdFpxSeGuUJNDQFeMGs2C1WXn";
            APPLICATION.recaptchaPrivate = "6Lc_ghsTAAAAAMJWyartVLawDEC1ctzbX4QRnmfB";
          </cfscript>
        </cfif>
        <cfif APPLICATION.ENV eq "PROD">
          <cfset APPLICATION.toAddress = "christi.fish@utsa.edu;courtney.clevenger@utsa.edu;kara.soria@utsa.edu;milady.nazir@utsa.edu;haylee.uptergrove@utsa.edu;pamela.lutrell@utsa.edu;news@utsa.edu;ingrid.wright@utsa.edu"  />
          <cfset APPLICATION.bccAddress = "shashi.pinheiro@utsa.edu;eric.ramirez3@utsa.edu" />
          <cfscript>
            APPLICATION.recaptchaPublic = "6Lc4chQTAAAAAFZN9U31I2tgs-cbBpz-O1Yv1oy-";
            APPLICATION.recaptchaPrivate = "6Lc4chQTAAAAAMUOE7g1k-zM4xewKLMzpDaUrFMF";
          </cfscript>
        </cfif>
		</cflock>
        <cfreturn true>
    </cffunction>

    <!--- Application shuts down --->
    <cffunction name="onApplicationEnd" returntype="void">
        <cfargument name="appScope" required="true">
        <cflog text="The UTSA Today web admin application has shut down." type="information" file="#this.name#">
        <cfreturn true>
    </cffunction>

  	<!--- Application request --->
    <cffunction name="onApplicationRequest" returntype="void">
    	<cfscript>
      if (StructKeyExists(URL, "hardReset")){
        onApplicationStart();
      }
			if (StructKeyExists(URL, "showdebugging")){
	  			this.debuggingIPAddress = "0:0:0:0:0:0:0:1,127.0.0.1";
	  			this.enableRobustException = true;
	  		}
  		</cfscript>
    </cffunction>

    <!--- Handles 404 events --->
    <cffunction name="onMissingTemplate" returntype="void">
        <cflocation url="#APPLICATION.path#/404.cfm">
    </cffunction>

  	<!--- Any web errors that are not caught --->
    <!---<cffunction name="onError" returntype="void">
        <!--- <cflocation url="#APPLICATION.path#/error.cfm"> --->
        <!--- add logging --->
    </cffunction>--->
</cfcomponent>
