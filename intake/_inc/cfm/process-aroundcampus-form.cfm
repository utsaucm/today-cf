<!--- reCaptcha COLDFUSION style --->
<cfparam name="toAddress" default="#APPLICATION.toAddress#" />
<cfparam name="bccAddress" default="#APPLICATION.bccAddress#" />

<!--- reCaptcha COLDFUSION style --->
<cf_recaptcha action="check"
	privateKey="#APPLICATION.recaptchaPrivate#"
	publicKey="#APPLICATION.recaptchaPublic#">

<cfset send = "no">
<cfif StructKeyExists(form, "aroundSubmit") AND StructKeyExists(form, "recaptcha") >
	<cfif form['recaptcha']>
		<cfset send = "yes">
	<cfelse>
		<cfset send = "no">
	</cfif>
</cfif>

<cfif send EQ "no">
	<!--- don't pass GO, give them back the guidelines and form --->
	<cfinclude template="#application.path#/_inc/html/around-guidelines.html">
	<cfinclude template="#application.path#/_inc/html/aroundcampus-intake-form.html">
<cfelse>
	<cfset date = DateFormat(now(), "yyyy-mm-dd")>
	<cfset time = TimeFormat(now(), "hh:mm:sstt")>
	<cfset lineBreak = Chr(13)&Chr(10)>
	<cfif APPLICATION.ENV EQ "DEV">
		<cfdump var="#form#" />
	</cfif>
	<cfscript>
		dataTxt = createObject( "java", "java.lang.StringBuilder" ).init();
		if (structKeyExists(FORM, "utStoryHeadline")){
			// Store the form vars
			data = { storyHeadline = FORM.utStoryHeadline,
				storyURL = FORM.utStoryURL,
				storyDescription = FORM.utStoryDescription,
				storyPublishDate = FORM.utStoryPublishDate,
				storySubmitter = FORM.utStorySubmitter,
				storySubmitterOrg = FORM.utStorySubmitterOrg,
				storyEmail = FORM.utStoryEmail
			};

			// email vars
			filename = "#FORM.utStorySubmitter#" & "StorySubmission";
			filename = ReReplace(filename, "[[:space:]]", "", "ALL");
			eSubject = "#APPLICATION.defaultSubject# - Headline Submission";

			// Build the string
			dataTxt.append("HEADLINE SUBMISSION DATA: #lineBreak#");
			dataTxt.append("Story Headline: #data['storyHeadline']##lineBreak#");
			dataTxt.append("Story URL: #data['storyURL']##lineBreak#");
			dataTxt.append("Story Description: #data['storyDescription']##lineBreak#");
			dataTxt.append("Ideal Publish Date: #data['storyPublishDate']##lineBreak#");
			dataTxt.append("Story Submitter: #data['storySubmitter']##lineBreak#");
			dataTxt.append("Story Submitter Org: #data['storySubmitterOrg']##lineBreak#");
			dataTxt.append("Story Submitter Email: #data['storyEmail']##lineBreak#");
		}
	</cfscript>
	<cftry>
		<cfif APPLICATION.ENV EQ "DEV">
			<cfdump var="#data#" />
		</cfif>

		<cfsavecontent variable="htmlContent">
			<cfinclude template="#Application.aroundTemplate#">
		</cfsavecontent>

		<!--- Send the email --->
		<cfmail to="#toAddress#" from="#APPLICATION.defaultSender#" bcc="#bccAddress#" subject="#eSubject#" type="html">
			<cfoutput>#htmlContent#</cfoutput>
		</cfmail>

			<cfset outputFile = GetDirectoryFromPath(GetBaseTemplatePath()) & "submits\" & "#filename#.txt">
      <cffile action="write" file="#outputFile#" output="#dataTxt#">
      <h2>Your request was submitted!</h2>
			<p><a href="/today/intake/view.cfm?page=intake-around">Submit another Campus Wire story request</a></p>
      <p><a href="/today/intake/view.cfm?page=intake-event">Submit an event request</a></p>
			<p><a href="http://www.utsa.edu/today/">UTSA Today</a></p>
    <cfcatch type="any">
			<h2>Error!</h2>
			<cfif APPLICATION.ENV EQ "DEV">
				<cfdump var="#cfcatch#" />
			</cfif>
			<p>Your event submission could not be completed at this time. Please try again later...</p>
		</cfcatch>
		</cftry>
	</cfif>
