<!--- reCaptcha COLDFUSION style --->
<cfparam name="toAddress" default="#APPLICATION.toAddress#" />
<cfparam name="bccAddress" default="#APPLICATION.bccAddress#" />

<!--- reCaptcha COLDFUSION style --->
<cf_recaptcha action="check"
	privateKey="#APPLICATION.recaptchaPrivate#"
	publicKey="#APPLICATION.recaptchaPublic#">

<cfset send = "no">
<cfif StructKeyExists(form, "eventSubmit") AND StructKeyExists(form, "recaptcha") >
	<cfif form['recaptcha']>
		<cfset send = "yes" />
	<cfelse>
		<cfset send = "no" />
	</cfif>
</cfif>

<cfif send EQ "no">
	<!--- don't pass GO, give them back the guidelines and form --->
  <cfinclude template="#application.path#/_inc/html/event-guidelines.html">
  <cfinclude template="#application.path#/_inc/html/event-intake-form.html">
<cfelse>
	<cfset date = DateFormat(now(), "yyyy-mm-dd")>
	<cfset time = TimeFormat(now(), "hh:mm:sstt")>
	<cfset lineBreak = Chr(13)&Chr(10)>
	<cfif APPLICATION.ENV EQ "DEV">
		<cfdump var="#form#" />
	</cfif>
	<cfscript>
		dataTxt = createObject( "java", "java.lang.StringBuilder" ).init();
		if (structKeyExists(FORM, "UTEVENTNAME")){
			// Store the form vars
			data = { evtName = FORM.UTEVENTNAME,
				evtDescription = FORM.UTEVENTDESCRIPTION,
				evtURL = FORM.UTEVENTURL,
				evtLOC = FORM.UTEVENTLOC,
				evtPUBLISHDATE = FORM.UTEVENTPUBLISHDATE,
				evtSubmitter = FORM.UTEVENTSUBMITTER,
				evtEmail = FORM.UTEVENTEMAIL,
				evtSUBMITTERORG = FORM.UTEVENTSUBMITTERORG
			};

			// email vars
			filename = "#FORM.UTEVENTSUBMITTER#" & "EventSubmission";
			filename = ReReplace(filename, "[[:space:]]", "", "ALL");

			eSubject = "#APPLICATION.defaultSubject# - Event Submission";

			// Build the string
			dataTxt.append("EVENT SUBMISSION DATA:#lineBreak#");
			dataTxt.append("Event Name: #data['evtNAME']##lineBreak#");
			dataTxt.append("Event Description: #data['evtDescription']##lineBreak#");
			dataTxt.append("Event URL: #data['evtURL']##lineBreak#");
			dataTxt.append("Event Location:  #data['evtLOC']##lineBreak#");
			dataTxt.append("Event Date/Time: #data['evtPUBLISHDATE']##lineBreak#");
			dataTxt.append("Event Submitter: #data['evtSubmitter']##lineBreak#");
			dataTxt.append("Event Submitter Email: #data['evtEmail']##lineBreak#");
			dataTxt.append("Event Submitter: #data['evtSubmitter']##lineBreak#");
			dataTxt.append("Event Submitter College/Office Name: #data['evtSUBMITTERORG']##lineBreak#");
		}
	</cfscript>
 	<cftry>
		<cfif APPLICATION.ENV EQ "DEV">
			<cfdump var="#data#" />
		</cfif>

		<cfsavecontent variable="htmlContent">
			<cfinclude template="#Application.eventTemplate#">
		</cfsavecontent>

		<!--- Send the email --->
		<cfmail to="#toAddress#" from="#APPLICATION.defaultSender#" bcc="#bccAddress#" subject="#eSubject#" type="html">
			<cfoutput>#htmlContent#</cfoutput>
		</cfmail>

		<cfset outputFile = GetDirectoryFromPath(GetBaseTemplatePath()) & "submits\" & "#filename#.txt" />
    <cffile action="write" file="#outputFile#" output="#dataTxt#">
    <h2>Your request was submitted!</h2>
		<p><a href="/today/intake/view.cfm?page=intake-event">Submit another Event request</a></p>
    <p><a href="/today/intake/view.cfm?page=intake-around">Submit an Around Campus request</a></p>
		<p><a href="http://www.utsa.edu/today/">UTSA Today</a></p>
  <cfcatch type="any">
		<h2>Error!</h2>
		<cfif APPLICATION.ENV EQ "DEV">
			<cfdump var="#cfcatch#" />
		</cfif>
		<p>Your event submission could not be completed at this time. Please try again later...</p>
	</cfcatch>
	</cftry>
</cfif>
