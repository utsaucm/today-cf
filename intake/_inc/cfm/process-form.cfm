<!--- reCaptcha COLDFUSION style --->
<cf_recaptcha action="check"
	privateKey="6LfZ4MQSAAAAAIirXjhZiyTXWJEZoIS-PBnjgW2z"
	publicKey="6LfZ4MQSAAAAAPyUhgukslXtBl52sH8DE-iW5haE">

<cfset send = "no">

<cfif (isDefined("form.eventSubmit") OR isDefined("form.aroundSubmit")) AND #form.recaptcha# EQ "true">
	<cfset send = "yes">
</cfif>

<cfif send EQ "no">
	<!--- Events Guidelines --->
    <cfinclude template="#application.path#/_inc/html/event-guidelines.html">
	<!--- Events Form --->
    <cfinclude template="#application.path#/_inc/html/event-intake-form.html">

    <hr />
	<!--- Around Guidelines --->
	<cfinclude template="#application.path#/_inc/html/around-guidelines.html">
	<!--- Around Campus Form--->
	<cfinclude template="#application.path#/_inc/html/aroundcampus-intake-form.html">
<cfelse>
	<cfset date = DateFormat(now(), "yyyy-mm-dd")>
	<cfset time = TimeFormat(now(), "hh:mm:sstt")>
	<cfset lineBreak = Chr(13)&Chr(10)>

		<!--- DEBUG: <cfdump var="#form#"> --->

		<cfscript>
			// FOR DEBUGGING: WriteDump(variables);

			// Create a JAVA StringBuilder object
			dataTxt = createObject( "java", "java.lang.StringBuilder" ).init();

			if (structKeyExists(FORM, "UTEVENTNAME")){
					// Store the form vars
					data = {evtName = FORM.UTEVENTNAME, evtDescription = FORM.UTEVENTDESCRIPTION, evtURL = FORM.UTEVENTURL,
						evtLOC = FORM.UTEVENTLOC, evtPUBLISHDATE = FORM.UTEVENTPUBLISHDATE, evtSubmitter = FORM.UTEVENTSUBMITTER,
						evtEmail = FORM.UTEVENTEMAIL, evtSUBMITTERORG = FORM.UTEVENTSUBMITTERORG
					};

					// email vars
					filename = "#FORM.UTEVENTSUBMITTER#" & "EventSubmission";
					filename = ReReplace(filename, "[[:space:]]", "", "ALL");
					eSubject = "UTSA Today Event Submission";

					// Build the string
					dataTxt.append("EVENT SUBMISSION DATA:");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Name: #data['evtNAME']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Description: #data['evtDescription']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event URL: #data['evtURL']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Location:  #data['evtLOC']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Date/Time: #data['evtPUBLISHDATE']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Submitter: #data['evtSubmitter']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Submitter Email: #data['evtEmail']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Submitter: #data['evtSubmitter']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Event Submitter College/Office Name: #data['evtSUBMITTERORG']#");
				}
				else{
					// Store the form vars
					data = {storyHeadline = FORM.utStoryHeadline, storyURL = FORM.utStoryURL, storyDescription = FORM.utStoryDescription, storyPublishDate = FORM.utStoryPublishDate,
						storySubmitter = FORM.utStorySubmitter, storySubmitterOrg = FORM.utStorySubmitterOrg, storyEmail = FORM.utStoryEmail
					};

					// email vars
					filename = "#FORM.utStorySubmitter#" & "StorySubmission";
					eSubject = "UTSA Today Headline Submission";
					eTemplate = "#application.path#/_inc/html/email-around-table.html";

					// Build the string
					dataTxt.append("HEADLINE SUBMISSION DATA:");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story Headline: #data['storyHeadline']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story URL: #data['storyURL']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story Description: #data['storyDescription']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Ideal Publish Date: #data['storyPublishDate']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story Submitter: #data['storySubmitter']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story Submitter Org: #data['storySubmitterOrg']#");
					dataTxt.append(#lineBreak#);
					dataTxt.append("Story Submitter Email: #data['storyEmail']#");
				}
		</cfscript>
	   <cftry>
			<!---DEBUG: <cfdump var="#data#">--->
			<!--- Send the email --->

  <!---          <cfmail to="Christi.Fish@utsa.edu;Mark.Langford@utsa.edu;Jesus.Chavez@utsa.edu;Joanna.Carver@utsa.edu;news@utsa.edu" from="no-reply@utsa.edu" bcc="#eReceiver#;Shashi.Pinheiro@utsa.edu" subject="#eSubject#" type="html">--->

            <cfmail to="ruben.ortiz@utsa.edu" from="ruben.ortiz@utsa.edu" subject="#eSubject#" type="html">
				<cfinclude template="#eTemplate#">
			</cfmail>

			<cfset outputFile = GetDirectoryFromPath(GetBaseTemplatePath()) & "submits\" & "#filename#.txt">

			<!--- Write form data to text file --->
            <cffile action="write" file="#outputFile#" output="#dataTxt#">

            <h2>Your request was submitted!</h2>
			<p>	<a href="/today/intake/view.cfm?page=intake">Submit another request</a> <br />
				<a href="http://www.utsa.edu/today/">UTSA Today</a>
			</p>

            <cfcatch type="any">
				<h2>Error!</h2>
				<p>Your event submission could not be completed at this time. Please try again later...</p>
			</cfcatch>
		</cftry>
	</cfif>
