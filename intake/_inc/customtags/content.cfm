<!--- Web Page View Parameters --->
<cfparam name="Attributes.view" 	default="default" 				type="string">
<cfparam name="Attributes.heading" 	default="Page heading" 			type="string">
<cfparam name="Attributes.query" 	default="selRR" 				type="string">
<cfparam name="isTable"  			default="0"         			type="numeric">
<cfparam name="eSubject"			default="UTSA Today"			type="string">
<cfparam name="eSender"				default="#APPLICATION.webAdminEmail#"	type="string">
<cfparam name="eReceiver"			default="#APPLICATION.webAdminEmail#"	type="string">
<cfparam name="eventLink"			default="/today/intake/view.cfm?page=intake-event"	type="string">
<cfparam name="aroundLink"			default="/today/intake/view.cfm?page=intake-around"	type="string">
<div class="row">
	<cfinclude template="/today/intake/_inc/html/side-nav.html" />
	<div class="col-xs-12 col-md-9 col-lg-9">
		<h1><cfoutput>#Attributes.heading#</cfoutput></h1>
  	<!--- Page View display logic --->
		<cfswitch expression="#Attributes.view#">
      <cfcase value="intake-event">
      	<cfinclude template="#application.path#/_inc/cfm/process-event-form.cfm" />
      	<br />
	    </cfcase>
      <cfcase value="intake-around">
    		<cfinclude template="#application.path#/_inc/cfm/process-aroundcampus-form.cfm" />
      	<br />
	    </cfcase>
			<!---  404  --->
			<cfcase value="oops">
				<p>the web page you are looking for does not exist!</p>
			</cfcase>
			<!--- Web page error --->
			<cfcase value="error">
				<p>You have encountered an error with this web application.</p>
				<p>Please contact the web admin for this issue. Thank you.</p>
			</cfcase>
			<!--- Default --->
			<cfdefaultcase>
      	<cfoutput>
        	<p>Submit a <a href="#eventLink#">new UTSA Today Event</a></p>
          <p>Submit a <a href="#aroundLink#"> new UTSA Newswire story or Announcement</a></p>
				</cfoutput>
      </cfdefaultcase>
		</cfswitch>
	</div><!-- class col-xs-12 -->
</div><!-- row -->
