 <!--- Web Page View Parameters --->
<cfparam name="Attributes.JS" default="default" type="string">
 		<footer id="footer">
            <div class="content container-fluid">
                <div id="footer-row" class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <nav id="footerLinks">
                      	<p class="info" style="display: inline;">&copy;2015 - UTSA University Communications and Marketing</p>
                      </nav>
                  </div><!-- col-sm-6 -->
                </div><!-- row -->
        	</div><!-- content container -->
		</footer>
		<!--- Javascripts --->
		<!--- JS include logic --->
		<cfswitch expression="#Attributes.JS#">
			<!--- Tabular views --->
	   		<cfcase value="default,drafts,archived,published,hidden" delimiters=",">
	   			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
          <script src="#application.path#/_assets/js/bootstrap.min.js" type="text/javascript"></script>
	   		</cfcase>

	   		<cfcase value="intake-event,intake-around" delimiters=",">
          <cfoutput>
	   			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 				  <script type="text/javascript" src="#application.path#/_assets/js/moment.min.js"></script>
 				  <script type="text/javascript" src="#application.path#/_assets/js/bootstrap.min.js"></script>
 				  <script type="text/javascript" src="#application.path#/_assets/js/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
          <script type="text/javascript" src="#application.path#/_assets/js/animatescroll.min.js"></script>
          <script type="text/javascript" src="#application.path#/_assets/js/jquery-validation-1.15.0/dist/jquery.validate.min.js"></script>
          <script type="text/javascript" src="#application.path#/_assets/js/jquery-validation-1.15.0/dist/additional-methods.min.js"></script>
          <script type="text/javascript" src="#application.path#/_assets/js/form-validation.js"></script>
          </cfoutput>
          <cfif #Attributes.JS# EQ "intake-event">
        	 <script type="text/javascript">
           //$("#outer-layer-event").animatescroll({scrollSpeed : 500, easing: 'easeInQuad'});
           </script>
          <cfelseif #Attributes.JS# EQ "intake-around">
        	 <script type="text/javascript">
           //$("##outer-layer-around").animatescroll({scrollSpeed : 500, easing: 'easeInQuad'});</script>
          </cfif>

          <cfif isDefined("form.recaptcha")>
            <cfif form.recaptcha is false>
              <cfif isDefined("form.eventSubmit")>
                <script type="text/javascript">
                $(document).ready(function(){
                  //alert("recaptcha is false.");
                  //$("#outer-layer-event").show();
                  $("#form-validation").animatescroll({scrollSpeed : 500, easing: 'easeInQuad'});
                });
                </script>
              <cfelseif isDefined("form.aroundSubmit")>
                <script type="text/javascript">
                  $(document).ready(function(){
                    //alert("recaptcha is false.");
                    //$("#outer-layer-around").show();
                    $("#form-validation").animatescroll({scrollSpeed : 500, easing: 'easeInQuad'});
                  });
                </script>
              </cfif>
            </cfif>
    			</cfif>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#utEventPublishDate').datetimepicker();
              $('#utStoryPublishDate').datetimepicker();
            });
				  </script>
	   		</cfcase>
	   		<!--- Default --->
			<cfdefaultcase>
				<!--- do nothing! --->
			</cfdefaultcase>
		</cfswitch>
	</body>
</html>
