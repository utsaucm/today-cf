<cfsetting enablecfoutputonly="true" />
<!---
	Use the reCAPTCHA API to verify human input.

	reCAPTCHA improves the process of digitizing books by sending words that
    cannot be read by computers to the Web in the form of CAPTCHAs for
    humans to decipher. More specifically, each word that cannot be read
    correctly by OCR is placed on an image and used as a CAPTCHA. This is
    possible because most OCR programs alert you when a word cannot be read
    correctly.

	You will need a key pair from http://recaptcha.net/api/getkey to use this tag.


	Sample 1 - Combined check/render
	--------------------------------

		<html>
		<body>

		<cfform>

			<cf_recaptcha
				privateKey="...your private key..."
				publicKey="...your public key...">

			<cfinput type="submit" name="submit">

		</cfform>

		<cfif isDefined("form.submit")>
			<cfoutput>recaptcha says #form.recaptcha#</cfoutput>
		</cfif>

		</body>
		</html>


	Sample 2 - Separate check/render
	--------------------------------

		<html>
		<body>

		<cf_recaptcha action="check"
			privateKey="...your private key..."
			publicKey="...your public key...">

		<cfif isDefined("form.submit")>
			<cfoutput>recaptcha says #form.recaptcha#</cfoutput>
		</cfif>

		<cfform>

			<cf_recaptcha
				privateKey="...your private key..."
				publicKey="...your public key...">

			<cfinput type="submit" name="submit">

		</cfform>

		</body>
		</html>


	@param publicKey 	Public key sent from browser with request for a challenge string.
						Note that if this is wrong you will not get a ColdFusion error and
						an error message will appear in place of the reCAPTCHA form controls.

	@param privateKey   Private key sent from ColdFusion server to reCAPTCHA's verification service.

	@param action 		render|check default render.
						"render" checks the submitted form and renders the reCAPTCHA form field.
						"check" checks the submitted form but does not render the form field.

	@param ssl 			set true if form on ssl page to use secured version of reCAPTCHA API and
						avoid browser complaints.

	@param theme 		red|white|blackgrass default red.  Changes look of reCAPTCHA form field.

	@param tabIndex		tabIndex of entry field on form.

	@return				sets form.recaptcha to true/false

	@throws				RECAPTCHA_ATTRIBUTE				Missing or invalid attribute
						RECAPTCHA_NO_SERVICE			Cannot contact verification service
						RECAPTCHA_VERIFICATION_FAILURE	Verification service responded with an error

	@see 				http://recaptcha.net/apidocs/captcha/

	(c) 2008 RocketBoots Pty Ltd
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	@version $Id$

--->
<cfif thistag.executionmode is "start">

<cfscript>
	CHALLENGE_URL = "http://api.recaptcha.net";
	SSL_CHALLENGE_URL = "https://api-secure.recaptcha.net";
	//VERIFY_URL = "http://api-verify.recaptcha.net/verify";
	VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
</cfscript>

<cfif not structKeyExists(attributes, "publicKey")>
	<cfthrow type="RECAPTCHA_ATTRIBUTE"
		message="recaptcha: required attribute 'publicKey' is missing" />
</cfif>

<cfif not structKeyExists(attributes, "privateKey")>
	<cfthrow type="RECAPTCHA_ATTRIBUTE"
		message="recaptcha: required attribute 'privateKey' is missing" />
</cfif>

<cftry>
	<cfparam name="attributes.action" default="render" />
	<cfif not listContains("render,check", attributes.action)>
		<cfset sInvalidAttr="action not render|check" />
		<cfthrow />
	</cfif>
	<cfcatch type="any">
		<cfthrow type="RECAPTCHA_ATTRIBUTE" message="recaptcha: attribute" />
	</cfcatch>
</cftry>

<cfif StructKeyExists(form, "g-recaptcha-response")>
	<cftry>
		<cfhttp url="#VERIFY_URL#" method="post" timeout="5" throwonerror="true">
			<cfhttpparam type="formfield" name="secret" value="#attributes.privateKey#" />
			<cfhttpparam type="formfield" name="response" value="#Form['g-recaptcha-response']#" />
		</cfhttp>
		<cfcatch>
			<cfthrow  type="RECAPTCHA_NO_SERVICE"
				message="recaptcha: unable to contact recaptcha verification service on url '#VERIFY_URL#'" />
		</cfcatch>
	</cftry>
	<cfscript>
		//localLog("parsing request result");
		responseString = CFHTTP.FileContent;
		responseJSON = deserializeJSON( CFHTTP.FileContent );
		captchaStatus = responseJSON['success'];
		if (captchaStatus NEQ 'true') {
			form.recaptcha = false;
			form['recaptcha-error'] = responseString;
			//attributes.action = "render";
		} else {
			form.recaptcha = true;
		}
	</cfscript>
	<cfset structDelete(form, "g-recaptcha-response") />
<cfelse>
	<cfset form.recaptcha = false />
</cfif>
<cfif (attributes.action EQ "render")>
	<cfoutput>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<div class="g-recaptcha" data-sitekey="#attributes.publickey#"></div>
	<cfif APPLICATION.ENV EQ "DEV">
		<div class="key">#attributes.publickey#</div>
	</cfif>
	</cfoutput>
</cfif>
</cfif>
<cfsetting enablecfoutputonly="false" />
