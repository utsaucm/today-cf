<cfparam name="Attributes.bodyID" default="ViewAll" type="string" />
<cfparam name="Attributes.RRHeading" default="Page 1" type="string" />
<!DOCTYPE HTML>
<html>
<cfoutput>
  <head>
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>#Attributes.Title# | UTSA Today</title>
  	<!-- Bootstrap 3.3.1 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="#application.path#/_assets/js/datetimepicker/css/bootstrap-datetimepicker.min.css"></script>
		<!-- custom style sheet -->
    <link rel="stylesheet" href="#application.path#/_assets/css/style.css">
  </head>
  <body id="#Attributes.bodyID#">
</cfoutput>
<cf_branding-nav>
<cf_site-header>
