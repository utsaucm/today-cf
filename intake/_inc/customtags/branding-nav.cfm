 <nav id="topBranding" class="navbar navbar-default" role="navigation">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a href="http://www.utsa.edu/" class="logo-link"><span id="logoSprite"></span></a>
                <a href="http://www.utsa.edu/" class="navbar-brand">The University of Texas at San Antonio</a>                  
              </div><!-- navbar-header -->
              <div class="collapse navbar-collapse" id="collapse">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://my.utsa.edu">myUTSA</a></li>
                    <li><a href="http://www.utsa.edu/today">News</a></li>
                    <li><a href="http://www.utsa.edu/maps">Maps</a></li>
                    <li><a href="#staff">Staff</a></li>
                    <li><a href="http://www.utsa.edu/directory">Directory</a></li>
                    <li><a href="#">Search</a></li>
                  </ul>        
              </div><!-- collapse navbar-collapse -->
            </div><!-- container -->
        </nav>         